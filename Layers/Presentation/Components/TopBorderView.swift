//
//  TopBorderView.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 28.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

@IBDesignable
class TopBorderView: UIView {
    @IBInspectable var thin: CGFloat = 0.5 {
        didSet
        {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var color: UIColor = UIColor.whiteColor() {
        didSet
        {
            setNeedsDisplay()
        }
    }
    
    override func drawRect(rect: CGRect)
    {
        super.drawRect(rect)
        let ctx = UIGraphicsGetCurrentContext()
        
        CGContextSetStrokeColorWithColor(ctx, color.CGColor)
        CGContextSetLineWidth(ctx, thin)
        CGContextMoveToPoint(ctx, CGRectGetMinX(rect), CGRectGetMinY(rect))
        CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMinY(rect))
        CGContextStrokePath(ctx)
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        setNeedsDisplay()
    }


}

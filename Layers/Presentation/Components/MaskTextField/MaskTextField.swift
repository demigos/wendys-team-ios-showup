//
// Created by Maksim Bazarov on 06.05.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import UIKit

class MaskTextField: UITextField, UITextFieldDelegate
{
    var formatter: CHRTextFieldFormatter?
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    
    private func setup()
    {
        self.delegate = self
        setPhone()
    }
    
    func setCreditCard() {
        formatter = CHRTextFieldFormatter(textField: self, mask: CHRCardNumberMask())
    }
    
    func setPhone() {
        formatter = CHRTextFieldFormatter(textField: self, mask: CHRPhoneNumberMask())
    }
    
    
    // MARK: Delegate
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        return formatter?.textField(textField, shouldChangeCharactersInRange: range, replacementString: string) ?? true
    }
    
}

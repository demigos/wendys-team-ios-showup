//
//  GenderSwitch.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 18.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

enum Person : Int
{
    case Me = 0
    case Loved = 1

    var name : String {
        switch self {
        case .Me:
            return "Me"

        case .Loved:
            return "A loved one"

        }
    }
}


@IBDesignable
class PersonSwitch:
    TwoOptonsSwitch
    ,TwoOptionsSwitchDatasource
{
    // MARK: Interface

    override func configure()
    {
        datasource = self
        person = .Me
    }

    var person : Person {
        get
        {
            let si = selectedIndex
            if  si >= 0 && si < data.count {
                return data[si]
            }

            return .Me
        }

        set
        {
            for (index,d) in data.enumerate() {
                if d == newValue {
                    selectCheckbox(index)
                }
            }
        }
    }


    // MARK: Private
    var data : [Person] = [.Me, .Loved]

    func labelForOption(index: Int) -> String {
        if index >= 0 && index < data.count {
            return data[index].name
        }
        return ""
    }

}

//
//  GenderSwitch.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 18.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

enum Gender : Int
{
    case Female = 2
    case Male = 1
    
    var name : String {
        switch self {
        case .Female:
            return "Female"
            
        case .Male:
            return "Male"
            
        }
    }
}


@IBDesignable
class GenderSwitch:
    TwoOptonsSwitch
    ,TwoOptionsSwitchDatasource
{
    // MARK: Interface
    
    override func configure()
    {
        datasource = self
    }
    
    var gender : Gender? {
        get
        {
            let si = selectedIndex
            if  si >= 0 && si < data.count {
                return data[si]
            }
            
            return nil
        }
        
        set
        {
            for (index,d) in data.enumerate() {
                if d == newValue {
                    selectCheckbox(index)
                }
            }
        }
    }

    
    // MARK: Private
    var data : [Gender] = [.Female, .Male]
    
    func labelForOption(index: Int) -> String {
        if index >= 0 && index < data.count {
            return data[index].name
        }
        return ""
    }
    
}

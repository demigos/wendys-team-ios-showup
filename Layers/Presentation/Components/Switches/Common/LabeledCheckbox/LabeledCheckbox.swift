//
//  GenderCheckbox.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 13.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit


@IBDesignable
class LabeledCheckbox: UIView
{
    let borderWidth = CGFloat(0)
    
    var defaultFGColor : UIColor? {
        didSet {
            image.defaultFGColor = defaultFGColor!
        }
    }
    
    var defaultBGColor : UIColor? {
        didSet {
            image.defaultBGColor = defaultBGColor!
        }
    }
    
    var defaultBorderColor : UIColor? {
        didSet {
            image.defaultBorderColor = defaultBorderColor!
        }
    }
    
    var selectedFGColor : UIColor? {
        didSet {
            image.selectedFGColor = selectedFGColor!
        }
    }
    
    var selectedBGColor : UIColor? {
        didSet {
            image.selectedBGColor = selectedBGColor!
        }
    }
    
    var selectedBorderColor : UIColor? {
        didSet {
            image.selectedBorderColor = selectedBorderColor!
        }
    }
        
    var selected : Bool = true {
        didSet {
            image.selected = selected
        }
    }

    var view: UIView!

    weak var delegate : LabeledCheckboxDelegate?
    
    // MARK: UI
    
    @IBOutlet weak var image: CheckBoxImage!
    @IBOutlet weak var label: UILabel!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
        
        if let check = UIImage(named: "check") {
            image.image = check
        }
        
        if let defaultBGColor = self.defaultBGColor {
            image.defaultBGColor = defaultBGColor
        }
        
        if let defaultFGColor = self.defaultFGColor {
            image.defaultFGColor = defaultFGColor
        }
        
        if let selectedBGColor = self.selectedBGColor {
            image.selectedBGColor = selectedBGColor
        }
        
        if let selectedFGColor = self.selectedFGColor {
            image.selectedFGColor = selectedFGColor
        }
        

        
    }
    
    func loadViewFromNib() -> UIView
    {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "LabeledCheckbox", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    // MARK : Control
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        notifyDelegatethatSelect()
    }
    
    override func intrinsicContentSize() -> CGSize
    {
        var size = label.intrinsicContentSize()
        size.width = size.height + size.width + 4 // image widht
        return size
    }
    
    // Delegate
    func notifyDelegatethatSelect() {
        delegate?.checkBoxDidSelect(self)
    }
    
   
}

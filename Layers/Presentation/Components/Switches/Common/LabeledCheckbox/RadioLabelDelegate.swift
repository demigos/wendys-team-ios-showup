//
//  GenderCheckboxDelegate.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 18.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

protocol LabeledCheckboxDelegate: class
{
    func checkBoxDidSelect(checkbox: LabeledCheckbox)
}

//
//  CheckBoxImage.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 18.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class CheckBoxImage: UIView
{
    
    // MARK: Public
    var defaultFGColor: UIColor = UIColor.clearColor()
    var defaultBGColor: UIColor = UIColor.clearColor()
    var defaultBorderColor: UIColor = UIColor.clearColor()
    
    var selectedFGColor: UIColor = UIColor.whiteColor()
    var selectedBGColor: UIColor = UIColor.blackColor()
    var selectedBorderColor: UIColor = UIColor.clearColor()
    
    var selected : Bool = true {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var borderWidth : CGFloat = 1
    
    var image : UIImage? {
        didSet {
            imageView.image = image
            setNeedsDisplay()
        }
    }
    
    // MARK: Private
    
    private var imageView: UIImageView!
    
    // MARK: Init
    func setup() {
        imageView = UIImageView(frame: bounds)
        addSubview(self.imageView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Layout
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        imageView.frame = bounds
        setNeedsDisplay()
    }
    
    // MARK: Drawings
    override func drawRect(rect: CGRect)
    {
        let imageRect = imageView.frame
        let size = bounds.height
        
        let fillColor = selected ? selectedBGColor : defaultBGColor
        let strokeColor = selected ? selectedBorderColor : defaultBorderColor
        
        let y = (CGRectGetMidY(imageRect) + borderWidth) - (size / 2)
        let x = (CGRectGetMidX(imageRect) + borderWidth) - (size / 2)
        
        let ovalPath = UIBezierPath(ovalInRect: CGRect(x: x , y: y, width: size - borderWidth - 1, height: size - borderWidth - 1))
        fillColor.setFill()
        ovalPath.fill()
        strokeColor.setStroke()
        ovalPath.lineWidth = borderWidth
        ovalPath.stroke()
        tintImage(imageView.image)
    }
    
    func tintImage(image: UIImage?)
    {
        if let image = image {
            imageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
            imageView.tintColor = selected ? selectedFGColor : defaultFGColor
        }
    }
}

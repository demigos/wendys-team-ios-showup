//
//  GenderSwitch.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 18.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

@IBDesignable

class TwoOptonsSwitch: UIView, LabeledCheckboxDelegate
{
    
    // MARK: Interface
    
    weak var datasource: TwoOptionsSwitchDatasource?
    weak var delegate: TwoOptionsSwitchDelegate?
    
    
    var view: UIView!
    var selectedIndex : Int {
        get {
            for (index, cb) in checkboxes.enumerate() {
                if cb.selected {
                    return index
                }
            }
            return -1
        }
    }
    
    func selectCheckbox(index: Int)
    {
        if (0...checkboxes.count-1).contains(index) {
            checkBoxDidSelect(checkboxes[index])
        }
    }
    
    func configure()
    {
      // configure component, need to override
    }
    
    // MARK: UI
    
    @IBOutlet var checkboxes: [LabeledCheckbox]!
    
    @IBInspectable var defaultFGColor: UIColor? {
        didSet {
            for cb in checkboxes {
                cb.defaultFGColor = defaultFGColor!
            }
        }
    }
    
    @IBInspectable var defaultBGColor: UIColor? {
        didSet {
            for cb in checkboxes {
                cb.defaultBGColor = defaultBGColor!
            }
        }
    }
    
    @IBInspectable var defaultBorderColor: UIColor? {
        didSet {
            for cb in checkboxes {
                cb.defaultBorderColor = defaultBorderColor!
            }
        }
    }
    
    @IBInspectable var selectedFGColor: UIColor? {
        didSet {
            for cb in checkboxes {
                cb.selectedFGColor = selectedFGColor!
            }
        }
    }
    
    @IBInspectable var selectedBGColor: UIColor? {
        didSet {
            for cb in checkboxes {
                cb.selectedBGColor = selectedBGColor!
            }
        }
    }
    
    @IBInspectable var selectedBorderColor: UIColor? {
        didSet {
            for cb in checkboxes {
                cb.selectedBorderColor = selectedBorderColor!
            }
        }
    }
    
    // MARK: Private
    
    // MARK : Init
    func xibSetup()
    {
        view = R.nib.twoOptonsSwitch.firstView(owner: self)
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        view.backgroundColor = UIColor.clearColor()
        // Crashes if count less then 2 thats because IB don't supports enums
        addSubview(view)
        configure()
        for (index, cb) in checkboxes.enumerate() {
            
            cb.label.text = datasource?.labelForOption(index) ?? ""
            cb.selected = false
            cb.delegate = self
            
            if let defaultBGColor = self.defaultBGColor {
                cb.defaultBGColor = defaultBGColor
            }
            
            if let defaultFGColor = self.defaultFGColor {
                cb.defaultFGColor = defaultFGColor
            }
            
            if let selectedBGColor = self.selectedBGColor {
                cb.selectedBGColor = selectedBGColor
            }
            
            if let selectedFGColor = self.selectedFGColor {
                cb.selectedFGColor = selectedFGColor
            }
        }
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    // MARK: Checkbox delegate
    internal func checkBoxDidSelect(checkbox: LabeledCheckbox)
    {
            var selectedIndex = 0
            for (index, cb) in checkboxes.enumerate() {
                if (cb == checkbox) {
                    cb.selected = true
                    selectedIndex = index
                } else {
                    cb.selected = false
                }
                
            }
            delegate?.optionDidSelect(selectedIndex, optionsSwitch: self)
        
    }
    
}

//
//  TwoOptionsSwitchDatasource.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 12/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

protocol TwoOptionsSwitchDatasource: class
{
    func labelForOption(index: Int) -> String
}


protocol TwoOptionsSwitchDelegate: class
{
    func optionDidSelect(index: Int, optionsSwitch: TwoOptonsSwitch)
}

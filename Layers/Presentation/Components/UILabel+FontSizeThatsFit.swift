//
//  UILabel+FontSizeThatsFit.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 07.06.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

extension UILabel
{
    
    
    func fontSizeThatFit(width width: CGFloat) -> CGFloat
    {
        let minFontSize = CGFloat(6)
        let originSize = CGFloat(20) // TODO: this cheat shouldn't be static
        let step = CGFloat(0.1)
        
        guard let labelFont = self.font , let labelText = self.text else {
            return minFontSize
        }
    
        if originSize <= minFontSize {
            return originSize
        }
        
        var size = originSize
        while size > minFontSize {
            size -= step
            let font = UIFont(name: labelFont.fontName, size: size)!
            let possibleWidth = widthForFont(font, text: labelText, height: CGRectGetHeight(self.bounds))
            if possibleWidth <= width {
                return size
            }
            
        }
    
        return minFontSize
    }
}

func widthForFont(font: UIFont, text: String, height: CGFloat) -> CGFloat
{
    let constraintRect = CGSize(width: CGFloat.max, height: height)
    let boundingBox = text.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
    return boundingBox.width
}

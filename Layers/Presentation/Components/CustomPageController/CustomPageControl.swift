//
//  PageViewSelector.swift
//  WendysTeamIOS
//
//  Created by Alexey on 20/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

protocol CustomPageControlDelegate {
    func vcSelected(vc: UIViewController)
}

class CustomPageControl: UIControl
{
    var view: UIView!
    @IBOutlet var buttons: [CustomPageButton]!
    
    private var controllers: [UIViewController]?
    private var delegate: CustomPageControlDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func setup(controllers: [UIViewController], defaultVC: UIViewController, delegate: CustomPageControlDelegate)
    {
        guard buttons.count >= self.controllers?.count else { return }
        
        self.controllers = controllers
        self.delegate = delegate
        
        if let index = controllers.indexOf(defaultVC) {
            buttons[index].selected = true
        }
        
        for vc in controllers {
            guard let index = controllers.indexOf(vc) else { continue }
            
            let button = buttons[index]
            button.vc = vc
        }
    }
    
    func selectVC(vc: UIViewController)
    {
        guard let index = controllers?.indexOf(vc) else { return }
        
        unselectAll()
        buttons[index].selected = true
    }
    
    // MARK: Private
    private func xibSetup()
    {
        view = R.nib.customPageControl.firstView(owner: self)
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }
    
    private func unselectAll()
    {
        for button in buttons {
            button.selected = false
        }
    }
    
    private func handleTouch(forButton button: CustomPageButton)
    {
        guard let index = buttons.indexOf(button) else { return }
        
        unselectAll()
        buttons[index].selected = true
        
        if let vc = controllers?[index] {
            delegate?.vcSelected(vc)
        }
    }
    
    // MARK: Actions
    @IBAction func aboutTouched(sender: CustomPageButton)
    {
        handleTouch(forButton: sender)
    }
    
    @IBAction func homeTouched(sender: CustomPageButton)
    {
        handleTouch(forButton: sender)
    }
    
    @IBAction func payManageTouched(sender: CustomPageButton)
    {
        handleTouch(forButton: sender)
    }
}

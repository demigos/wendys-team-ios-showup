//
//  CustomPageButton.swift
//  WendysTeamIOS
//
//  Created by Alexey on 20/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class CustomPageButton: UIButton
{
    weak var vc: UIViewController? {
        didSet
        {
            setTitle(vc?.title, forState: .Normal)
        }
    }
}

//
//  PageViewController.swift
//  WendysTeamIOS
//
//  Created by Alexey on 20/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

protocol CustomPageControllerDelegate {
    func didMoveTo(vc: UIViewController)
}

class CustomPageController: UIPageViewController
    , UIPageViewControllerDelegate
    , UIPageViewControllerDataSource
    , CustomPageControlDelegate
    , CustomPageControllerDelegate
{
    private var controllers: [UIViewController]?
    private var pendingVC: UIViewController?
    var pageControl: CustomPageControl?
    
    private var customDelegate: CustomPageControllerDelegate?
    
    
    init()
    {
        super.init(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(controllers: [UIViewController], defaultVC: UIViewController, delegate: CustomPageControllerDelegate)
    {
        self.controllers = controllers
        self.customDelegate = delegate
        
        if let _ = controllers.indexOf(defaultVC) {
            setViewControllers([defaultVC], direction: .Forward, animated: false, completion: nil)
        } else if let vc = controllers.first {
            setViewControllers([vc], direction: .Forward, animated: false, completion: nil)
        }
        
        pageControl?.setup(controllers, defaultVC: defaultVC, delegate: self)
    }
    
    func moveTo(vc: UIViewController)
    {
        if let _ = controllers?.indexOf(vc) {
            setViewControllers([vc], direction: .Forward, animated: true, completion: nil)
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    // MARK: UIPageViewControllerDelegate
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {        
        if let vc = pendingVC where completed {
            customDelegate?.didMoveTo(vc)
            pendingVC = nil
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController])
    {
        pendingVC = pendingViewControllers.first
    }
    
    // MARK: UIPageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        if let controllers = controllers, let index = controllers.indexOf(viewController) where index - 1 >= 0
        {
            return controllers[index - 1]
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        if let controllers = controllers, let index = controllers.indexOf(viewController) where index + 1 < controllers.count
        {
            return controllers[index + 1]
        }
        
        return nil
    }
    
    // MARK: CustomPageControlDelegate
    func vcSelected(vc: UIViewController)
    {
        moveTo(vc)
    }
    
    // MARK: CustomPageControllerDelegate
    func didMoveTo(vc: UIViewController)
    {
        pageControl?.selectVC(vc)
    }
}

//
//  TintedImageView.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 25.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class TintedImageView: UIImageView {

    override var image: UIImage? {
        didSet {
            if image?.renderingMode != .AlwaysTemplate {
                tintImage()
            }
        }
    }
    
    func tintImage()
    {
        if let image = image {
            self.image = image.imageWithRenderingMode(.AlwaysTemplate)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        tintImage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tintImage()
    }

}

//
// Created by Maksim Bazarov on 15.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import UIKit

class RoundedImage : UIImageView {

   
    func setupRoundCorners() {
        layer.cornerRadius = CGRectGetHeight(frame) / 2
        clipsToBounds = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundCorners()
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundCorners()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}

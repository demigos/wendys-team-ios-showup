//
//  RoundedView.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 27.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView {
  
    @IBInspectable var cornerRadius : CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = CGFloat(cornerRadius)
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.grayColor() {
        didSet {
            self.layer.borderColor = borderColor.CGColor
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 1 {
        didSet {
            self.layer.borderWidth = CGFloat(borderWidth)
            self.setNeedsLayout()
        }
    }
    
    
    func setupRoundCorners() {
        self.layer.cornerRadius = CGRectGetHeight(frame) / 2
        self.clipsToBounds = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundCorners()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundCorners()
    }
}

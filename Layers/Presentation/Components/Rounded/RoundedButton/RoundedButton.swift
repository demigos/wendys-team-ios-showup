//
// Created by Maksim Bazarov on 14.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

@IBDesignable
class RoundedButton : UIButton {
    
    @IBInspectable var bgColor : UIColor = UIColor.grayColor() {
        didSet {
            self.backgroundColor = bgColor
            self.setNeedsLayout()
        }
    }
    
    override var enabled: Bool {
        didSet{
            alpha = enabled ? 1.0 : 0.56
        }
    }
    
    @IBInspectable var cornerRadius : CGFloat = 0 {
      didSet {
            self.layer.cornerRadius = CGFloat(cornerRadius)
                    self.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.grayColor() {
        didSet {
            self.layer.borderColor = borderColor.CGColor
            self.setNeedsLayout()
        }
    }

    @IBInspectable var borderWidth : CGFloat = 1 {
        didSet {
            self.layer.borderWidth = CGFloat(borderWidth)
                        self.setNeedsLayout()
        }
    }

    func setupRoundCorners() {
        self.layer.cornerRadius = CGRectGetHeight(frame) / 2
        self.clipsToBounds = true

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundCorners()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

//            frame = CGRectMake(frame.origin.x+8, frame.origin.y, frame.size.width+16, frame.size.height)

        
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundCorners()
    }
}

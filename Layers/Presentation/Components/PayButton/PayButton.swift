//
//  PayButton.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 30.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

@IBDesignable
class PayButton: UIControl {

    var view: UIView!
    
    @IBInspectable var title : String? {
        didSet {
            buttonTitle.text = title
        }
    }
    
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var buttonTitle: UILabel!
    
    
    @IBAction func payForAllButtonAction(sender: AnyObject)
    {
        sendActionsForControlEvents(.AllEvents)
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "PayButton", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }


}

//
//  DateTextField.swift
//  WendysTeamIOS
//
//  Created by Alexey on 23/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

protocol DateTextFieldOutput: class
{
    func didSelectDate(date: NSDate)
}

class DateTextField: UITextField
    , YearMonthPickerDelegate
{
    weak var output: DateTextFieldOutput?
    var datePicker: YearMonthPicker!
    var toolbar: UIToolbar!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup()
    {
        datePicker = YearMonthPicker()
        datePicker.output = self
        
        toolbar = UIToolbar()
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(doneTouched))
        ]
        toolbar.sizeToFit()
        
        self.inputView = datePicker
        self.inputAccessoryView = toolbar
    }
    
    private func setDate(date: NSDate)
    {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "y / MM"
        
        self.text = formatter.stringFromDate(date)

        output?.didSelectDate(date)
    }
    
    func doneTouched(sender: AnyObject)
    {
        if let date = datePicker.date {
            setDate(date)
        }
        
        resignFirstResponder()
    }
    
    // MARK: YearMonthPickerDelegate
    func didSelectDate(date: NSDate)
    {
        setDate(date)
    }
}

//
//  YearMonthPicker.swift
//  WendysTeamIOS
//
//  Created by Alexey on 23/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

protocol YearMonthPickerDelegate: class
{
    func didSelectDate(date: NSDate)
}

class YearMonthPicker: UIPickerView
    , UIPickerViewDelegate
    , UIPickerViewDataSource
{
    var date: NSDate? {
        return getCurrentDate()
    }
    
    private let yearOffset = 15
    private var years: [Int]!
    private var months: [String]!
    
    weak var output: YearMonthPickerDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        let calendar = NSCalendar.currentCalendar()
        let formatter = NSDateFormatter()
        let today = NSDate()
        
        let year = calendar.components(.Year, fromDate: today).year
        let month = calendar.components(.Month, fromDate: today).month
    
        
        self.years = Array(year...year + yearOffset)
        
        var months = [String]()
        
        for month in formatter.monthSymbols {
            months.append(month)
        }
        
        self.months = months
        
        self.delegate = self
        self.dataSource = self
        
        self.selectRow(month - 1, inComponent: 0, animated: false)
        self.selectRow(0, inComponent: 1, animated: false)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getCurrentDate() -> NSDate?
    {
        let month = months[self.selectedRowInComponent(0)]
        let year = years[self.selectedRowInComponent(1)]
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMMM y"
        
        return formatter.dateFromString("\(month) \(year)")
    }
    
    // MARK: UIPickerViewDataSource
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        switch component {
        case 0: return months.count
        case 1: return years.count
        default: return 0
        }
    }
    
    // MARK: UIPickerViewDelegate
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        switch component {
        case 0: return months[row]
        case 1: return "\(years[row])"
        default: return nil
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if let date = getCurrentDate() {
            self.output?.didSelectDate(date)
        }
    }
}
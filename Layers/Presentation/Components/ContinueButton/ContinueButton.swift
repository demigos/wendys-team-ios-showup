//
// Created by Maksim Bazarov on 10.04.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

class ContinueButton : UIButton
{
    
    @IBInspectable var enabledColor = UIColor(red:0.45, green:0.71, blue:0.27, alpha:1.0)
    @IBInspectable var disabledColor = UIColor(red:0.67, green:0.82, blue:0.58, alpha:1.0)
    
    func setupColorForCurrentState()
    {
        self.backgroundColor = enabled ? enabledColor  : disabledColor
    }
    
    override var enabled : Bool {
        didSet {
            setupColorForCurrentState()
        }
    }

    func setup()
    {
        self.layer.cornerRadius = 4.0
        self.clipsToBounds = true
        setupColorForCurrentState()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

}

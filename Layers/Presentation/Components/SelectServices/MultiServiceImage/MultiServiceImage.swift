//
//  ServiceImage.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 04.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class MultiServiceImage: UIView
{    
    @IBInspectable var count: UInt = 1
    {
        didSet
        {
            setNeedsDisplay()
        }
    }


    @IBInspectable var service : CareServiceDomainModel = .CheckIn {
        didSet
        {
            if let image = UIImage(named: service.imageName) {
                imageView.image = image
                tintImage()
            }
        }
    }
    
    func tintImage()
    {
        if let image = imageView.image {
            imageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
            imageView.tintColor = mainColor
        }
    }
    
    var borderWidth = CGFloat(1)

    var step = CGFloat(2.5)

    @IBInspectable var size = CGFloat(42)

    @IBInspectable var mainColor: UIColor = UIColor.whiteColor() {
        didSet
        {
            tintImage()
        }
    }

    @IBInspectable var fillColor: UIColor = UIColor.blackColor() {
        didSet
        {
            setNeedsDisplay()
        }
    }

    lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: nil)
        self.addSubview(imageView)
        return imageView
    }()

    override func drawRect(rect: CGRect)
    {
        if count < 1 {
            count = 1
        }

        let center = CGPoint(x: CGRectGetWidth(rect) / 2, y: CGRectGetHeight(rect) / 2)
        let componentWidth = (CGFloat(count) * step) + size
        let componentHeight = size
        let y = center.y - componentHeight / 2
        var x = CGFloat(0)
        for i in 1 ... count {
            x = (center.x - componentWidth / 2) + (CGFloat(count - i) * step)
            let ovalPath = UIBezierPath(ovalInRect: CGRect(x: x, y: y, width: size, height: size))
            fillColor.setFill()
            ovalPath.fill()
            mainColor.setStroke()
            ovalPath.lineWidth = borderWidth
            ovalPath.stroke()
        }

        imageView.frame = CGRectMake(x, y, size, size)
        tintImage()
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
                setNeedsDisplay()
    }
}

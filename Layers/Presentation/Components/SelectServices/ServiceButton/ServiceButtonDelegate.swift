//
// Created by Maksim Bazarov on 10.04.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

@objc protocol ServiceButtonDelegate : class
{
    func selectionOfButton(button: ServiceButton, didChangeTo selected: Bool)
}

//
//  ServiceButton.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 10.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

@IBDesignable
class ServiceButton: UIView
{
    // MARK : Inspectable properties
    @IBInspectable weak var delegate: ServiceButtonDelegate?
    @IBInspectable var service : Int = 0 {
        didSet
        {
            if let serviceModel  = CareServiceDomainModel(rawValue: service) {
                if let image = UIImage(named: serviceModel.bigImageName){
                    tintImage(image)
                }
                label.text = serviceModel.description
            }

        }
    }

    @IBInspectable var defaultTintColor: UIColor = UIColor(red:0.11, green:0.57, blue:1.00, alpha:1.0)
    @IBInspectable var defaultBGColor: UIColor = UIColor.whiteColor()

    @IBInspectable var selectedTintColor: UIColor = UIColor.whiteColor()
    @IBInspectable var selectedBGColor: UIColor = UIColor(red:0.11, green:0.57, blue:1.00, alpha:1.0)

    @IBInspectable var borderWidth = CGFloat(1)

    var selected : Bool = false {
        didSet {
             setNeedsDisplay()
             delegate?.selectionOfButton(self, didChangeTo: selected)
        }
    }
    // MARK : Private properties

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    var view : UIView!

    var serviceModel : CareServiceDomainModel? {
        get
        {
            if let serviceModel  = CareServiceDomainModel(rawValue: service) {
                return serviceModel
            }

            return nil
        }
    }


    // MARK: Initialization
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }




    func loadViewFromNib() -> UIView {

        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ServiceButton", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView

        return view
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        selected = selected ? false : true
    }

    // MARK: Layout helpers
    var size = CGFloat(0)
    var imageCenter = CGPointZero

    override func drawRect(rect: CGRect)
    {
        let imageRect = imageView.frame
        size = CGRectGetHeight(imageRect) < CGRectGetWidth(imageRect) ? CGRectGetHeight(imageRect) : CGRectGetWidth(imageRect)
        imageCenter = CGPoint(x: CGRectGetWidth(imageRect) / 2, y: CGRectGetHeight(imageRect) / 2)

        let fillColor = selected ? selectedBGColor : defaultBGColor
        let strokeColor = selected ? selectedTintColor : defaultTintColor

        let y = CGRectGetMinY(imageRect) + borderWidth
        let x = CGRectGetMinX(imageRect) + borderWidth

        let ovalPath = UIBezierPath(ovalInRect: CGRect(x: x , y: y, width: size - borderWidth - 1, height: size - borderWidth - 1))
        fillColor.setFill()
        ovalPath.fill()
        strokeColor.setStroke()
        ovalPath.lineWidth = borderWidth
        ovalPath.stroke()
        tintImage(imageView.image)
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
        setNeedsDisplay()
    }


    func tintImage(image: UIImage?)
    {
        if let image = image {
            imageView.image = image.imageWithRenderingMode(.AlwaysTemplate)
            imageView.tintColor = selected ? selectedTintColor : defaultTintColor
        }
    }



}

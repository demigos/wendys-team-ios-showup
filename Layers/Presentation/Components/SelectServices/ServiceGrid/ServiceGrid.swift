//
//  ServiceGrid.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 10.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class ServiceGrid: UIView , ServiceButtonDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    weak var delegate : ServiceGridDelegate?
    var view : UIView!
    var additionalServicesChosen = true
    var min : CGFloat?
    var chosenServices = [false, false,false, false,false, false,false, false,false, false,false, false]

    //    @IBOutlet var serviceButtons: [ServiceButton]!

    // MARK: Initialization
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("serviceCell", forIndexPath: indexPath) as? ServiceCollectionViewCell {
            cell.layer.shouldRasterize = true;
            cell.layer.rasterizationScale = UIScreen.mainScreen().scale;
            //            cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: 20, height: 20)
            cell.serviceImageView.layoutIfNeeded()
            cell.serviceImageView.layer.borderWidth = 1

//            cell.serviceNameLabel.hidden = false
            if self.chosenServices[indexPath.row] {
                cell.serviceImageView.backgroundColor = UIColor(red: 27 / 255, green: 145 / 255, blue: 255 / 255, alpha: 1)
                cell.serviceImageView.tintColor = UIColor.whiteColor()
                //                cell.serviceImageView
                cell.serviceImageView.layer.borderColor = UIColor.whiteColor().CGColor
            }
            else {
                cell.serviceImageView.backgroundColor = UIColor.clearColor()
                //                cell.serviceImageView.tintColor = UIColor.redColor()
                cell.serviceImageView.tintColor = UIColor(red: 27 / 255, green: 145 / 255, blue: 255 / 255, alpha: 1)
                cell.serviceImageView.layer.borderColor = UIColor(red: 27 / 255, green: 145 / 255, blue: 255 / 255, alpha: 1).CGColor
            }
            if indexPath.row == 8 && !self.additionalServicesChosen {
                cell.bindWithService(0)
            }
            else {
                cell.bindWithService(indexPath.row + 1)
            }
            cell.serviceImageView.layer.cornerRadius = (self.view.frame.width - 64) / 6
            let width = (self.view.frame.width - 64) / 3
            if self.min == nil {
                min = cell.helperLabel.fontSizeThatFit(width: width)
                cell.serviceNameLabel.font = UIFont(name: cell.serviceNameLabel.font.fontName, size: min!)
            }
            else {
                cell.serviceNameLabel.font = UIFont(name: cell.serviceNameLabel.font.fontName, size: min!)
            }
            cell.serviceNameLabel.hidden = false
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("serviceCell", forIndexPath: indexPath)
            return cell
        }
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let baseSize = (self.view.frame.width - 64) / 3
        return CGSizeMake(baseSize , baseSize * 12 / 9)
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.additionalServicesChosen {
            return 12
        }
        else {
            return 9
        }
    }

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 8 && !self.additionalServicesChosen {
            self.additionalServicesChosen = true
            self.collectionView.scrollEnabled = true
            delegate?.servicesSetDidChanged(selectedServices() )
            collectionView.reloadData()
            collectionView.scrollToItemAtIndexPath(NSIndexPath(forRow: 11, inSection: 0), atScrollPosition: .Bottom, animated: true)
        }
        else {
            self.chosenServices[indexPath.row] = !self.chosenServices[indexPath.row]
            delegate?.servicesSetDidChanged(selectedServices() )
            collectionView.reloadData()
        }
        //        var someServicesSelected = false
        //        for item in self.chosenServices {
        //            if item {
        //                someServicesSelected = true
        //            }
        //        }
        //        if someServicesSelected {
        //        }
    }

    func setupButtonsDelegate()
    {
        //        for b in serviceButtons {
        //            b.delegate = self
        //        }
    }

    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ServiceGrid", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setupButtonsDelegate()
        collectionView.registerClass(ServiceCollectionViewCell.self, forCellWithReuseIdentifier: "serviceCell")
        var nibName = UINib(nibName: "ServiceCollectionViewCell", bundle:nil)
        collectionView.registerNib(nibName, forCellWithReuseIdentifier: "serviceCell")

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setupButtonsDelegate()
        collectionView.registerClass(ServiceCollectionViewCell.self, forCellWithReuseIdentifier: "serviceCell")
        var nibName = UINib(nibName: "ServiceCollectionViewCell", bundle:nil)
        collectionView.registerNib(nibName, forCellWithReuseIdentifier: "serviceCell")

    }

    func prepareView() {
//        self.collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: 11, inSection: 0), atScrollPosition: .Bottom, animated: false)
//        self.collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: .Top, animated: false)
//        self.collectionView.hidden = false
//        self.collectionView.reloadItemsAtIndexPaths([NSIndexPath(forItem:11, inSection: 0),NSIndexPath(forItem:10, inSection: 0),NSIndexPath(forItem:9, inSection: 0)])
    }

    func isServices(services: [CareServiceDomainModel], contains: Int) -> Bool {
        for s in services {
            if s.rawValue == contains {
                return true
            }
        }
        return false
    }

    func setupServices(services: [CareServiceDomainModel])
    {
        var haveAdditionalServices = false
        for service in services {
            if service.rawValue <= self.chosenServices.count {
                self.chosenServices[service.rawValue - 1] = true
                if service.rawValue > 8 {
                    haveAdditionalServices = true
                }
            }
        }
        if haveAdditionalServices {
            self.additionalServicesChosen = true
            self.collectionView.scrollEnabled = true
        }
        //        for b in serviceButtons {
        //            if isServices(services, contains: b.service) {
        //                b.selected = true
        //            } else {
        //                b.selected = false
        //            }
        //        }
        delegate?.servicesSetDidChanged(selectedServices() )
    }

    func selectedServices() -> [CareServiceDomainModel]
    {
        var services = [CareServiceDomainModel]()
        for index in 0 ..< self.chosenServices.count {
            if self.chosenServices[index] {
                if let serviceModel  = CareServiceDomainModel(rawValue: index + 1) {
                    services.append(serviceModel)
                }
            }
        }
        //        for b in serviceButtons {
        //            if let model = b.serviceModel where b.selected {
        //                services.append(model)
        //            }
        //        }
        return services
    }

    func updateLabelSizes() {
        self.collectionView.reloadData()
        var min = CGFloat.max
        //        for b in serviceButtons {
        //            let width = CGRectGetWidth(b.bounds)
        //            min = b.label.fontSizeThatFit(width: width) < min ? b.label.fontSizeThatFit(width: width) : min
        //        }
        //
        //        for b in serviceButtons {
        //            b.label.font = UIFont(name: b.label.font.fontName, size: min)
        //
        //        }
    }
    
    func selectionOfButton(button: ServiceButton, didChangeTo selected: Bool)
    {
        delegate?.servicesSetDidChanged(selectedServices() )
    }
    
}

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var helperLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func bindWithService(service : Int) {
        helperLabel.text = "Personal Assistance"
        if let serviceModel  = CareServiceDomainModel(rawValue: service) {
            if let image = UIImage(named: serviceModel.bigImageName) {
                serviceImageView.image = image
            }
            serviceNameLabel.text = serviceModel.description
        }
    }
    
}

//
// Created by Maksim Bazarov on 10.04.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

protocol ServiceGridDelegate : class
{
    func servicesSetDidChanged(services : [CareServiceDomainModel])
}

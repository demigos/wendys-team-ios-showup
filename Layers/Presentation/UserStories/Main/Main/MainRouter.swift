//
//  MainMainRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class MainRouter: VIPERRouter, UnpaidFlowDelegate
{
    weak var view: MainViewController!
    weak var presenter: MainPresenter!
    var unpaidVisits: [VisitDomainModel]?
    var homeControllerOutput : HomeViewOutput?

    func setupPageController()
    {
        view.addChildViewController(view.pageController)
        view.pageController.view.frame = view.containerView.bounds
        view.containerView.addSubview(view.pageController.view)
        view.pageController.didMoveToParentViewController(view)

        let about = AboutAssembly.createModule { (module) in }
        let home = HomeAssembly.createModule(resources: presenter.resources) { module in
            module.setupNavigation(self.navigationController!)
        }
        self.homeControllerOutput = home.output
        let pay = PayManageAssembly.createModule(presenter.resources) { (module) in
            module.setupNavigation(self.navigationController!)
            module.setupDelegate(self.presenter)
        }

        view.setupControllers([about, home, pay], defaultVC: home)
    }

    func updateUnpaidCounterWithBadge() {
        self.view.unpaidCountLabel.text = "\(UIApplication.sharedApplication().applicationIconBadgeNumber)"
        if UIApplication.sharedApplication().applicationIconBadgeNumber < 1 {
            self.view.unpaidViewHeight.constant = 0
        }
        else {
            self.view.unpaidViewHeight.constant = 28
        }
    }

    func updateUnpaidCounter(unpaidVisits: [VisitDomainModel]) {
        self.view.unpaidCountLabel.text = "\(unpaidVisits.count)"
        self.unpaidVisits = unpaidVisits
        if unpaidVisits.count < 1 {
            self.view.unpaidViewHeight.constant = 0
        }
    }

    func presentBookVisit()
    {
        var flow = BookVisitDefaultFlow()
        flow.first = true
        BookVisitAssembly.createModule(resources: presenter.resources, flow: flow) { module in
            module.setupDelegate(self.presenter)
            }.presentModal(from: view!)

    }

    func showUnpaidVisits() {
        if let visits = self.unpaidVisits {
            var flow = UnpaidFlow()
            var flowVisits = UnpaidNoFeedbackVisitsDomainModel()
            flowVisits.unpaid = visits
            flow.visits = flowVisits
            flow.delegate = self
            let unpaid = UnpaidVisitsListAssembly.createModule(presenter.resources, visits: flowVisits, flow: flow) {
                (module) in
            }
            if let nc = navigationController ?? view.navigationController {
                unpaid.present(inNavigation: nc)
            } else {
                unpaid.presentModal(from: view)
            }

        }
    }

    func showSavedUnpaidVisits() {
        if let unpaidVisits = self.unpaidVisits {
            let visits = UnpaidNoFeedbackVisitsDomainModel(unpaid: unpaidVisits, nofeedback: [VisitDomainModel]())
            var flow = UnpaidFlow(visits: visits)
            flow.delegate = self
            switch flow.initialViewType() {
            case 0:
                let unpaid = UnpaidVisitsListAssembly.createModule(presenter.resources, visits: visits, flow: flow) {
                    (module) in
                    //                            module.setupNavigation(self.navigationController!)
                    //            module.setupDelegate(self.presenter)
                }
                if let nc = navigationController ?? view.navigationController {
                    unpaid.present(inNavigation: nc)
                } else {
                    unpaid.presentModal(from: view)
                }
            case 1:
                let paid = PaidVisitAssembly.createModule(presenter.resources, flow: flow) { module in }
                paid.visitId = visits.unpaid[0].id
                paid.unpaid = true
                paid.singleUnpaidVisit = true
                if let nc = navigationController ?? view.navigationController {
                    paid.present(inNavigation: nc)
                } else {
                    paid.presentModal(from: view)
                }
            case 2:
                let paid = PaidVisitAssembly.createModule(presenter.resources, flow: flow) { module in }
                paid.visitId = visits.nofeedback[0].id
                paid.nofeedback = true
                if let nc = navigationController ?? view.navigationController {
                    paid.present(inNavigation: nc)
                } else {
                    paid.presentModal(from: view)
                }
            default:
                return
            }
        }
    }

    func showUnpaidView(visits : UnpaidNoFeedbackVisitsDomainModel) {
        if visits.unpaid.count > 0 {
            self.unpaidVisits = visits.unpaid
            self.view.unpaidViewHeight.constant = 28
            self.view.unpaidCountLabel.text = "\(visits.unpaid.count)"
        }
        var flow = UnpaidFlow(visits: visits)
        flow.delegate = self
        switch flow.initialViewType() {
        case 0:
            let unpaid = UnpaidVisitsListAssembly.createModule(presenter.resources, visits: visits, flow: flow) {
                (module) in
                //                            module.setupNavigation(self.navigationController!)
                //            module.setupDelegate(self.presenter)
            }
            if let nc = navigationController ?? view.navigationController {
                unpaid.present(inNavigation: nc)
            } else {
                unpaid.presentModal(from: view)
            }
        case 1:
            let paid = PaidVisitAssembly.createModule(presenter.resources, flow: flow) { module in }
            paid.visitId = visits.unpaid[0].id
            paid.unpaid = true
            paid.singleUnpaidVisit = true
            if let nc = navigationController ?? view.navigationController {
                paid.present(inNavigation: nc)
            } else {
                paid.presentModal(from: view)
            }
        case 2:
            let paid = PaidVisitAssembly.createModule(presenter.resources, flow: flow) { module in }
            paid.visitId = visits.nofeedback[0].id
            paid.nofeedback = true
            if let nc = navigationController ?? view.navigationController {
                paid.present(inNavigation: nc)
            } else {
                paid.presentModal(from: view)
            }
        default:
            return
        }
    }

    func dismissBookVisit()
    {
        view.dismissViewControllerAnimated(true) {}
    }

    func presetAssignVisitWithId(visitID: Int) {
        let assigned = AssignedVisitAssembly.createModule(presenter.resources) { module in }
        //        if let nc = navigationController ?? view.navigationController {
        //            assigned.present(inNavigation: nc)
        //        } else {
        assigned.visitId = visitID
        assigned.presentModalTransparent(from: view)
        //        }
    }

    func presetDoneVisitWithId(visitID: Int, failureMessage : String? = nil) {
        //!!!

        if self.unpaidVisits == nil {
            self.unpaidVisits = [VisitDomainModel]()
        }
        self.unpaidVisits?.append(VisitDomainModel(id: visitID, caregiverID: nil, services: [CareServiceDomainModel](), zipCode: nil, address: nil, phone: nil, date: nil, duration: nil, cost: nil, status: nil, rating: nil, feedback: nil, failureMessage: nil, customerName: nil, customerID: nil, customerAvatar: nil, teamMemberName: nil, teamMemberAvatar: nil, actualDuration: nil, isOwner: true))
        if self.view.unpaidViewHeight != nil {
            self.view.unpaidViewHeight.constant = 28
            self.view.unpaidCountLabel.text = "\(self.unpaidVisits!.count)"
        }
        var flow = UnpaidFlow(visits: nil)
        let paid = PaidVisitAssembly.createModule(presenter.resources, flow: flow) { module in }
        paid.visitId = visitID
        paid.unpaid = true
        paid.failureMessage = failureMessage
        paid.singleUnpaidVisit = true
        if let nc = navigationController ?? view.navigationController {
            paid.present(inNavigation: nc)
        } else {
            paid.presentModal(from: view)
        }
    }

    func presentVisitWithID(id : Int?) {
        let paid = PaidVisitAssembly.createModule(presenter.resources) { module in }
        paid.visitId = id
        if let nc = navigationController ?? view.navigationController {
            paid.present(inNavigation: nc)
        } else {
            paid.presentModal(from: view)
        }
    }


    func presentPaidVisit(visitID: Int)
    {
        let paid = PaidVisitAssembly.createModule(presenter.resources) { module in }
        paid.visitId = visitID
        if let nc = navigationController ?? view.navigationController {
            paid.present(inNavigation: nc)
        } else {
            paid.presentModal(from: view)
        }
    }
}
//
//  MainMainInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol MainInteractorInput: class
{
    func visitsCount()
    func checkUnpaidVisits()
}

//MARK: Output
protocol MainInteractorOutput: class
{
    func visitsCount(count: Int)
    func showUnpaidView(visits : UnpaidNoFeedbackVisitsDomainModel)
    func showVisitViewForVisit(visitId : Int)
}

// MARK: - Interactor
final class MainInteractor: MainInteractorInput
{
    weak var output: MainInteractorOutput!
    var visits: VisitsDSInterface
    
    init(visits: VisitsDSInterface)
    {
        self.visits = visits
    }
    
    func checkUnpaidVisits() {
        visits.unpaidNoFeedbackUpdate.observeResult { (result) in
            switch result {
            case .Success(let loadedVisits):
                

                UIApplication.sharedApplication().applicationIconBadgeNumber = loadedVisits.unpaid.count
                
                if loadedVisits.unpaid.count > 0 || loadedVisits.nofeedback.count > 0 {
                    self.output.showUnpaidView(loadedVisits)
                }
            default: break
            }
        }
        visits.loadUnpaidAndNoFeedbackVisits()
    }
    
    func visitsCount()
    {
        visits.visitsListUpdate.observeResult { (result) in
            switch result {
            case .Success(let visits):
                self.output?.visitsCount(visits.count)
            default: break
            }
        }
        
        visits.loadVisits()
    }
}
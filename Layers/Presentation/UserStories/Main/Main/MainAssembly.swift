//
//  MainMainAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class MainAssembly
{
    class func createModule(resources: Dependencies, configure: (module: MainModuleInput) -> Void) -> MainViewController
    {
        let vc = R.storyboard.main.mainViewController()!
        let interactor = MainInteractor(visits: resources.datasources.visits)
        let presenter = MainPresenter()
        let router = MainRouter()
        
        
        interactor.output = presenter
        
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        
        presenter.resources = resources
        
        resources.authService.delegate = presenter
        configure(module: presenter)
        
        vc.output = presenter
        vc.router = router
        
        router.view = vc
        router.presenter = presenter
        
        
        return vc
    }
}
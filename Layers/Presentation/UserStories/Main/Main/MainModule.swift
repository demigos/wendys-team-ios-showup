//
//  MainMainModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol MainModuleInput
{
    var notificationDelegate: PushNotificationsServiceDelegate? {get}
    func showUnpaidView(visits : UnpaidNoFeedbackVisitsDomainModel)
    func setupDelegate(output: MainModuleOutput)

}

//MARK: Output
protocol MainModuleOutput: class
{
    func userDidLogout()
}


// MARK: - Presenter
final class MainPresenter: MainModuleInput
    , MainViewOutput
    , MainInteractorOutput
    , AuthorizationServiceDelegate
    , BookVisitModuleOutput
    , PushNotificationsServiceDelegate
    , PayManageModuleOutput
{
    weak var resources: Dependencies!
    weak var view: MainViewInput!
    weak var router: MainRouter!
    var notificationDelegate: PushNotificationsServiceDelegate? {
        return self
    }

    var interactor: MainInteractorInput!

    weak var output: MainModuleOutput?
    
    func showVisitViewForVisit(visitId : Int) {
        self.router.presentVisitWithID(visitId)
    }

    func setupDelegate(output: MainModuleOutput)
    {
        self.output = output
    }
    

    
    func showUnpaidView(visits : UnpaidNoFeedbackVisitsDomainModel) {
        router.showUnpaidView(visits)
    }

    // MARK: Auth service delegate
    func authErrorDidHappened(error: WTError)
    {
        output?.userDidLogout()
    }
    
    func showUnpaidVisits() {
        self.router.showUnpaidVisits()
    }

    func userDidLogin() {
        // No operations
    }
    
    func resetCodeDidReceive(code: String) {
        // No operations
    }

    // MARK: - Interactor Output
    func visitsCount(count: Int)
    {
        if count <= 0
        {
            DispatchToMainQueue({
                self.router.presentBookVisit()
            })
        }
    }
    // MARK: - View Output
    func viewDidLoad()
    {
        DispatchToBackground {
            self.interactor.visitsCount()
            self.interactor.checkUnpaidVisits()
        }
    }

    // MARK: Book visit output
    func visitBookingDidCanceled()
    {

    }

    func visitDidBooked(visit: VisitDomainModel)
    {
        DispatchToMainQueue({
            var tracker = GAI.sharedInstance().defaultTracker
            let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Visit booked", label: nil, value: nil).build() as [NSObject: AnyObject]
            tracker.send(event)
            self.router.dismissBookVisit()
//            self.router.homeControllerOutput?.refreshOwner()
            self.router.homeControllerOutput?.reloadAll()
        })
    }

    // MARK: Notification delegate
    func visitDidPaid(visitID: Int)
    {
        router.presentPaidVisit(visitID)
    }

    func visitDidAssignCaregiver(visitID: Int)
    {
        router.presetAssignVisitWithId(visitID)
    }
    
    func paidWasFailedWithMessage(message : String?)
    {
        if let message = message {
             view.showError(message)
        }
        router.updateUnpaidCounterWithBadge()
    }
    
    func visitDidDoneWithFailureMessage(failureMessage : String?, visitID: Int) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        router.presetDoneVisitWithId(visitID, failureMessage: failureMessage)
    }
    
    func visitDidDone(visitID: Int) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        router.presetDoneVisitWithId(visitID)
    }


    // MARK: PayManageModuleOutput
    func logout()
    {
        output?.userDidLogout()
    }

}
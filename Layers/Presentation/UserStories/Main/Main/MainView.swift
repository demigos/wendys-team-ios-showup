//
//  MainMainView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol MainViewInput: BaseViewControllerInput
{
    func setupControllers(vcs: [UIViewController], defaultVC: UIViewController)
}

protocol MainViewOutput: class
{
    func viewDidLoad()
}

// MARK: - View Controller
final class MainViewController: BaseViewController
    , Routable
    , MainViewInput
{
    typealias RouterType = MainRouter
    var router: RouterType!
    var output: MainViewOutput!
    
    // MARK: Outlets
    @IBOutlet weak var unpaidViewHeight: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var unpaidCountLabel: UILabel!
    @IBOutlet weak var unpaidVisitsView: UIView!
    
    var pageController = CustomPageController()
    @IBOutlet weak var pageControl: CustomPageControl!
    
    // MARK: Actions
    @IBAction func phoneAction(sender: AnyObject)
    {
        PhoneService.call()
    }
    
    @IBAction func showUnpaidVisits(sender: AnyObject)
    {
        self.router.showSavedUnpaidVisits()
    }
    
    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        output.viewDidLoad()
        router.setupPageController()
        unpaidViewHeight.constant = 0
    }
    
    // MARK: View Input
    func setupControllers(vcs: [UIViewController], defaultVC: UIViewController)
    {
        pageController.pageControl = pageControl
        pageController.setup(vcs, defaultVC: defaultVC, delegate: pageController)
    }
}
//
//  PayManagePayManageInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol PayManageInteractorInput: class
{
    func loadOwner()
    func refresh()
}

//MARK: Output
protocol PayManageInteractorOutput: class
{
    func ownerDidLoad(owner: CustomerDomainModel)
    func ownerDidFailed()
}

// MARK: - Interactor
final class PayManageInteractor: PayManageInteractorInput
{
    weak var output: PayManageInteractorOutput!
    private var customers : CustomersDSInterface

    init(customersDataSource: CustomersDSInterface)
    {
        self.customers = customersDataSource
    }

    func loadOwner()
    {
        customers.ownerUpdate.observeResult { result in
            switch result {
            case .Success(let owner):
                self.output.ownerDidLoad(owner)
            case .Failure(let error):
                self.output.ownerDidFailed()
            }
        }
    }

    func refresh()
    {
        customers.loadCustomers()
    }
}
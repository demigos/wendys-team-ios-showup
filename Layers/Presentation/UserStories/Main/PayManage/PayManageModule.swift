//
//  PayManagePayManageModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface

protocol PayManageModuleInput {
    func setupDelegate(output: PayManageModuleOutput)
    
    func setupNavigation(nc: UINavigationController)
}

//MARK: Output

protocol PayManageModuleOutput: class {
    func logout()
}


// MARK: - Presenter

final class PayManagePresenter: PayManageModuleInput
    , PayManageViewOutput
    , PayManageInteractorOutput
    , CustomersListModuleOutput
    , CardsListModuleOutput
, CustomerProfileModuleOutput {
    weak var viewController: PayManageViewInput!
    var interactor: PayManageInteractorInput!
    weak var router: PayManageRouter!
    weak var output: PayManageModuleOutput?
    var offlineMode = false
    
    private var owner: CustomerDomainModel!
    
    func setupDelegate(output: PayManageModuleOutput) {
        self.output = output
    }
    
    func setupNavigation(nc: UINavigationController) {
        router.navigationController = nc
    }
    
    init() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(setOnlineMode),
                                                         name: NetworkService.networkReachable,
                                                         object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(setOfflineMode),
                                                         name: NetworkService.networkNotReachable,
                                                         object: nil)
    }
    
    func isOnline() -> Bool {
        return !self.offlineMode
    }
    
    func checkConnectionStatus() {
        if self.offlineMode {
            self.setOfflineMode()
        }
        else {
            self.setOnlineMode()
        }
    }
    
    @objc func setOnlineMode() {
        offlineMode = false
        self.router.setCustomersListOnlineMode(true)
        self.viewController.setOnline()
    }
    
    @objc func setOfflineMode() {
        offlineMode = true
        self.router.setCustomersListOnlineMode(false)
        self.viewController.setOffline()
    }

    
    // MARK: - Interactor Output
    func ownerDidLoad(owner: CustomerDomainModel) {
        self.owner = owner
        viewController.updateOwner(owner)
    }
    
    // MARK: - View Output
    func didLoad() {
        if let owner = ApplicationState.loadCustomer() {
            ownerDidLoad(owner)
        }
        interactor.loadOwner()
        router.presentCustomers()
    }
    
    func editProfileDidSelect() {
        if owner == nil || self.offlineMode{
            return
        }
        router.presentEditProfile(owner)
    }
    
    func ownerDidFailed() {
        //ownerLoadFailed = true
    }
    
    // MARK: CustomersListModuleOutput
    func customerDidSelect(customer: CustomerDomainModel) {
        router.presentEditProfile(customer)
    }
    
    func customersCountUpdated(count: Int) {
        viewController.updatePersonCount(count)
    }
    
    
    // MARK: CardsListModuleOutput
    func cardsFramePaddingDidChange(to padding: CGFloat) {
        router.customersList.changeInsets(to: padding)
    }
    
    //MARK: CustomerProfileModuleOutput
    func logout() {
        output?.logout()
    }
    
    func customerDidUpdate() {
        DispatchToBackground {
            self.interactor.refresh()
        }
    }
}
//
//  PayManagePayManageAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class PayManageAssembly
{
    class func createModule(resources: Dependencies, configure: (module: PayManageModuleInput) -> Void) -> PayManageViewController
    {
        let vc = R.storyboard.main.payManageViewController()!
        let interactor = PayManageInteractor(customersDataSource: resources.datasources.customers)
        let presenter = PayManagePresenter()
        let router = PayManageRouter()


        interactor.output = presenter

        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router
        
        router.viewController = vc
        router.presenter = presenter
        router.resources = resources
        
        return vc
    }
}
//
//  PayManagePayManageRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class PayManageRouter: VIPERRouter
{
    weak var viewController: PayManageViewController!
    weak var presenter: PayManagePresenter!
    var resources: Dependencies!
    weak var cardController : CardsListViewController?
    
    weak var customersList: CustomersListModuleInput!
    
    func presentEditProfile(customer: CustomerDomainModel)
    {
        CustomerProfileAssembly.createModule(resources) { (module) in
            module.setupCustomer(customer)
            module.setupDelegate(self.presenter)
        }.presentModal(from: viewController)
    }
    
    func setCustomersListOnlineMode(onlineMode : Bool) {
        if self.customersList != nil {
            self.customersList.setOnlineMode(onlineMode)
        }
    }
    
    func presentCardsList()
    {
        self.cardController = CardsListAssembly.createModule(resources) { (module) in
            module.setupDelegate(self.presenter)
            module.setupNavigation(self.navigationController!)
            module.refreshList()
        }
            self.cardController?.presentChild(from: viewController)
    }
    
    func showCardList(show : Bool)
    {
        self.cardController?.view.hidden = show
    }
    
    func presentCustomers()
    {
        CustomersListAssembly.createModule(resources) { [unowned self] (module) in
            self.customersList = module
            module.setupDelegate(self.presenter)
            module.setLayout(.Short)
        }.presentChild(from: viewController, using: viewController.container)
    }
}
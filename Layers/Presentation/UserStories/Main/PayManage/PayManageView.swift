//
//  PayManagePayManageView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol PayManageViewInput: class {
    func updatePersonCount(count: Int)
    
    func updateOwner(owner: CustomerDomainModel)
    func setOffline()
    func setOnline()
}

protocol PayManageViewOutput: class {
    func didLoad()
    func isOnline() -> Bool
    func editProfileDidSelect()
    func checkConnectionStatus()
}

// MARK: - View Controller

final class PayManageViewController:
    BaseViewController
    , Routable
, PayManageViewInput {
    var output: PayManageViewOutput!
    typealias RouterType = PayManageRouter
    var router: RouterType!
    
    var editMode = false
    
    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var manageButton: UIButton!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var customersCount: UILabel!
    @IBOutlet weak var personLabel: UILabel!
    
    let randomColor = AvatarRandomColor()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.tintColor = UIColor(red: 171 / 255, green: 187 / 255, blue: 223 / 255, alpha: 1)
        output.didLoad()
        
        router.presentCardsList()
        self.output.checkConnectionStatus()
    }
    
    func checkConnection() {
        if self.output.isOnline() {
            self.setOnline()
        }
        else {
            self.setOffline()
        }
    }

    override func viewDidAppear(animated: Bool) {
        //must not call base viewdidappear now
    }
    
    func setOffline() {
        if self.editProfileButton != nil {
            self.editProfileButton.enabled = false
        }
        if self.customersCount != nil {
            self.customersCount.hidden = true
        }
        if self.personLabel != nil {
            self.personLabel.hidden = true
        }
        if self.manageButton != nil {
            self.manageButton.enabled = false
        }
    }
    
    func setOnline() {
        if self.editProfileButton != nil {
            self.editProfileButton.enabled = true
        }
        if self.customersCount != nil {
            self.customersCount.hidden = false
        }
        if self.personLabel != nil {
            self.personLabel.hidden = false
        }
        if self.manageButton != nil {
            self.manageButton.enabled = true
        }
    }
    
    // MARK: View Input
    func updateOwner(owner: CustomerDomainModel) {
        //        clearProfile()
        profileNameLabel.text = owner.name
        
        if let imageURL = owner.thumb {
            profileImage.setupImage(fromURL: imageURL)
        }
        else {
                profileImage.tintColor = randomColor.colorForIndex(owner.id)
        }
    }
    
    func updatePersonCount(count: Int) {
        customersCount.text = "\(count)"
    }
    
    // MARK: Actions
    @IBAction func manageButtonAction(sender: AnyObject) {
        editMode = !editMode
        
        router.customersList.setEditMode(editMode)
        router.showCardList(editMode)
        updateButtonTitle()
    }
    
    @IBAction func editProfileAction(sender: AnyObject) {
        output.editProfileDidSelect()
    }
    
    // MARK: Private
    private func updateButtonTitle() {
        if editMode {
            manageButton.setTitle("Done", forState: .Normal)
        } else {
            manageButton.setTitle("Manage", forState: .Normal)
        }
    }
    
    private func clearProfile() {
        profileNameLabel.text = ""
        profileImage.image = R.image.big_avatar()
    }
}
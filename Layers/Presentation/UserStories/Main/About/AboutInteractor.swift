//
//  AboutAboutInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol AboutInteractorInput: class
{

}

//MARK: Output
protocol AboutInteractorOutput: class
{

}

// MARK: - Interactor
final class AboutInteractor: AboutInteractorInput
{
    weak var output: AboutInteractorOutput!
}
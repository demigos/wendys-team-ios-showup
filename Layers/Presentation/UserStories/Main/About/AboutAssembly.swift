//
//  AboutAboutAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class AboutAssembly
{
    class func createModule(configure: (module: AboutModuleInput) -> Void) -> AboutViewController
    {
        let vc = R.storyboard.main.aboutViewController()!
        let interactor = AboutInteractor()
        let presenter = AboutPresenter()
        let router = AboutRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor

        configure(module: presenter)

        vc.output = presenter
        vc.router = router
        
        return vc
    }
}
import UIKit

protocol TermsCellDelegate: class {
    func termsLinkSelected()
    func policyLinkSelected()
}

class TermsTableViewCell: UITableViewCell, TTTAttributedLabelDelegate {
    
    @IBOutlet
    var termsLabel: TTTAttributedLabel!
    weak var delegate:TermsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTermsText()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupTermsText() {
        let termsText = "Using our application, you agree to our Wendy’s Team Terms and Data Policy."
        let finalString = NSMutableAttributedString(string: termsText, attributes: nil)
        let overallRange: NSRange = (termsText as NSString).rangeOfString(termsText)
        finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica", size: 15.0)!, range: overallRange)
        let rangeTerm: NSRange = (termsText as NSString).rangeOfString("Wendy’s Team Terms")
        finalString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 125 / 255, green: 125 / 255, blue: 133 / 255, alpha: 1.0), range: overallRange)
        finalString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 13 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1.0), range: rangeTerm)
        let rangePrivacy: NSRange = (termsText as NSString).rangeOfString("Data Policy")
        finalString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 13 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1.0), range: rangePrivacy)
        
        termsLabel.attributedText = finalString
        termsLabel.delegate = self
        termsLabel.activeLinkAttributes = nil
        termsLabel.inactiveLinkAttributes = nil
        termsLabel.linkAttributes = nil
        termsLabel.addLinkToURL(NSURL(string: "termsURL"), withRange: (termsText as NSString).rangeOfString(termsText))
        termsLabel.addLinkToURL(NSURL(string: "policyURL"), withRange: rangePrivacy)
    }
    
    
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        if (url.isEqual(NSURL(string: "termsURL"))) {
            self.delegate?.termsLinkSelected()
        }
        if (url.isEqual(NSURL(string: "policyURL"))) {
            self.delegate?.policyLinkSelected()
        }
    }
    
    
    
}

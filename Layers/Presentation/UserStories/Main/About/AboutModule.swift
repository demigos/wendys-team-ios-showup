//
//  AboutAboutModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol AboutModuleInput
{
    func setupDelegate(output: AboutModuleOutput)
}

//MARK: Output
protocol AboutModuleOutput: class
{

}


// MARK: - Presenter
final class AboutPresenter:
    AboutModuleInput
    , AboutViewOutput
    , AboutInteractorOutput
{
    weak var view: AboutViewInput!
    var interactor: AboutInteractorInput!

    weak var output: AboutModuleOutput?

    func setupDelegate(output: AboutModuleOutput)
    {
        self.output = output
    }


    // MARK: - Interactor Output

    // MARK: - View Output
}
//
//  AboutAboutView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol AboutViewInput: class
{

}

protocol AboutViewOutput: class
{

}

// MARK: - View Controller
final class AboutViewController:
    UITableViewController
    , Routable
    , AboutViewInput
    , TermsCellDelegate
{
    var output: AboutViewOutput!
    typealias RouterType = AboutRouter
    var router: RouterType!

    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 80;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        /* Initialization here */
    }
    
    func goToGoogle() {
        if let url = NSURL(string: "https://www.yelp.com/biz/wendys-team-denver") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func goToWebSite() {
        if let url = NSURL(string: "http://wendys-team.com") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func termsLinkSelected() {
        let termsViewController = R.storyboard.authorization.termsViewController()!
        self.navigationController?.pushViewController(termsViewController, animated: true)
    }
    
    func policyLinkSelected() {
        let termsViewController = R.storyboard.authorization.termsViewController()!
        termsViewController.terms = false
        self.navigationController?.pushViewController(termsViewController, animated: true)
    }
    
    func goToFaceBook() {
        if let url = NSURL(string: "https://www.facebook.com/Wendys-Team-282371778792763/?fref=ts") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        switch indexPath.row {
        case 0:
            cell = tableView.dequeueReusableCellWithIdentifier("logoCell", forIndexPath: indexPath)
        case 1:
            cell = tableView.dequeueReusableCellWithIdentifier("missionCell", forIndexPath: indexPath)
        case 2:
            cell = tableView.dequeueReusableCellWithIdentifier("linkCell", forIndexPath: indexPath)
            let googleTouchGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToGoogle))
            (cell as! LinksTableViewCell).googleImageView.addGestureRecognizer(googleTouchGestureRecognizer)
            let facebookTouchGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToFaceBook))
            (cell as! LinksTableViewCell).faceBookImageView.addGestureRecognizer(facebookTouchGestureRecognizer)
            let webSiteTouchGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToWebSite))
            (cell as! LinksTableViewCell).websiteImageView.addGestureRecognizer(webSiteTouchGestureRecognizer)
        case 3:
            cell = tableView.dequeueReusableCellWithIdentifier("ratesCell", forIndexPath: indexPath)
        case 4:
            cell = tableView.dequeueReusableCellWithIdentifier("termsCell", forIndexPath: indexPath)
            (cell as! TermsTableViewCell).delegate = self
            cell.userInteractionEnabled = true
        default:
            break
        }
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    // MARK: View Input
}
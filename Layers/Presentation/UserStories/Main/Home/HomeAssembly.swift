//
//  HomeHomeAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class HomeAssembly
{
    class func createModule(resources resources: Dependencies, configure: (module: HomeModuleInput) -> Void) -> HomeViewController
    {
        let vc = R.storyboard.main.homeViewController()!
        let interactor = HomeInteractor(customersDataSource: resources.datasources.customers, visitsDataSource: resources.datasources.visits)
        let presenter = HomePresenter()
        let router = HomeRouter()


        interactor.output = presenter

        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources

        configure(module: presenter)

        vc.output = presenter
        vc.router = router
        
        router.presenter = presenter
        router.viewController = vc
        router.resources = resources
        
        return vc
    }
}
//
//  HomeHomeRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router

final class HomeRouter: VIPERRouter {
    weak var resources: Dependencies!
    weak var presenter: HomePresenter!
    weak var viewController: HomeViewController!
    weak var visitsController: VisitsListViewController?

    weak var customersList: CustomersListModuleInput!

    // MARK: Booking
    func presentBookVisit() {
        let flow = BookVisitDefaultFlow()
        BookVisitAssembly.createModule(resources: presenter.resources, flow: flow) {
            module in
            module.setupDelegate(self.presenter)
        }.presentModal(from: viewController!)
    }

    func presentBookVisitForCustomer(customer: CustomerDomainModel) {
        let flow = BookVisitDefaultFlow(initialStep: .ContactInfo, first: false)
        BookVisitAssembly.createModule(resources: resources, flow: flow) {
            (module) in
            module.setupDelegate(self.presenter)
            module.setupCustomer(customer)
        }.presentModal(from: viewController)
    }

    func presentUpdateVisit(visit: VisitDomainModel) {
        var flow = BookVisitDefaultFlow()
        flow.initialStep = .DurationAndTime
        BookVisitAssembly.createModule(resources: resources, flow: flow) {
            (module) in
            module.setupDelegate(self.presenter)
            module.updateVisit(visit)
        }.presentModal(from: viewController)
    }

    func presentEditVisit(visit: VisitDomainModel) {
        let flow = EditVisitFlow()
        BookVisitAssembly.createModule(resources: resources, flow: flow) {
            (module) in
            module.setupDelegate(self.presenter)
            module.updateVisit(visit)
        }.presentModal(from: viewController)
    }

    func dismissBookingScreen() {
        viewController!.dismissViewControllerAnimated(true) {
        }
    }
    
    func dismissAssignedScreen() {
        viewController!.dismissViewControllerAnimated(true) {
        }
    }

    // MARK: Visits
    func presentVisits() {
        let vc = VisitsListAssembly.createModule(presenter.resources) {
            (module) in
            module.setupNavigation(self.navigationController!)
            module.setupDelegate(self.presenter)
            module.refreshList()
        }
        self.visitsController = vc
        vc.presentChild(from: viewController)
    }
    
    func reloadVisits() {
//        self.visitsController.
    }
    
    func reloadCustomers() {
        customersList.refreshList()
    }

    // MARK: Customers
    func presentCustomers() {
        if let customersListView = self.customersList {
            customersListView.refreshList()
         return
        }
        CustomersListAssembly.createModule(resources) {
            [unowned self] (module) in
            self.customersList = module
            module.setupDelegate(self.presenter)
            }.presentChild(from: viewController, using: viewController.container)
    }


}
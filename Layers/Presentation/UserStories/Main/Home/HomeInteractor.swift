//
//  HomeHomeInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol HomeInteractorInput: class
{
    func loadOwner()
    func loadCounters()
    
}

//MARK: Output
protocol HomeInteractorOutput: class
{
    func visitsDidUpdate(visits : [VisitDomainModel])
    func ownerDidLoad(owner: CustomerDomainModel)
    func ownerDidFailed()
    
}

// MARK: - Interactor
final class HomeInteractor: HomeInteractorInput
{
    weak var output: HomeInteractorOutput!
    private var customers : CustomersDSInterface
    private var visits : VisitsDSInterface
    
    init(customersDataSource: CustomersDSInterface, visitsDataSource: VisitsDSInterface)
    {
        self.customers = customersDataSource
        self.visits = visitsDataSource
    }
    
    func loadCounters() {
        visits.visitsListUpdate.observeResult { result in
            switch result {
            case .Success(let visits):
                self.output.visitsDidUpdate(visits)
            case .Failure(let error):
                return
            }
        }
        customers.loadCustomers()
    }
    
    func loadOwner()
    {
        
        customers.ownerUpdate.observeResult { result in
            switch result {
            case .Success(let owner):
                self.output.ownerDidLoad(owner)
            case .Failure(let error):
                self.output.ownerDidFailed()
            }
        }
        customers.loadCustomers()
    }
}
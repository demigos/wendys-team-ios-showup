//
//  HomeHomeView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol HomeViewInput: class {
    func updateOwner(owner: CustomerDomainModel)
    func setOffline()
    func setOnline()
    func reloadAll()
}

protocol HomeViewOutput: class {
    func viewDidLoad()

    func bookVisitDidSelect(forCustomer customer: CustomerDomainModel?)

    func ownerDidSelect(owner: CustomerDomainModel)
    func checkConnectionStatus()
    func refreshOwner()
    func reloadVisits()
    func reloadAll()
}

// MARK: - View Controller

final class HomeViewController:
        BaseViewController
        , Routable
        , HomeViewInput {
    var output: HomeViewOutput!
    typealias RouterType = HomeRouter
    var router: RouterType!

    var owner: CustomerDomainModel!

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var mainPersonView: UIView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var totalVisitsLabel: UILabel!
    @IBOutlet weak var upcomingVisitsLabel: UILabel!
    @IBOutlet weak var repeatVisitsLabel: UILabel!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var bookButton : UIButton!

    let randomColor = AvatarRandomColor()

    @IBAction func bookVisitAction(sender: AnyObject) {
        if owner == nil {
            return
        }
        output.bookVisitDidSelect(forCustomer: nil)
    }
    
    func setOffline() {
        self.totalVisitsLabel.hidden = true
        self.upcomingVisitsLabel.hidden = true
        self.mainPersonView.alpha = 0.5
        self.bookButton.alpha = 0.5
    }
    
    func reloadAll() {
        self.viewDidLoad()
    }
    
    func setOnline() {
        self.totalVisitsLabel.hidden = false
        self.upcomingVisitsLabel.hidden = false
        self.mainPersonView.alpha = 1
        self.bookButton.alpha = 1
    }

    @IBAction func mainPersonDidSelect(sender: AnyObject) {
        if owner == nil {
            return
        }
        output.ownerDidSelect(owner)
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.tintColor = UIColor(red: 171 / 255, green: 187 / 255, blue: 223 / 255, alpha: 1)
        output.viewDidLoad()
        self.output.checkConnectionStatus()
    }

    func updateCounters(owner: CustomerDomainModel) {
        if let upcoming = owner.visits_upcoming {
            if let passed = owner.visits_passed {
                totalVisitsLabel.text = "\(upcoming + passed) visits"
                upcomingVisitsLabel.text = "\(upcoming) upcoming"
            }
        }
    }

    // MARK: View Input
    func updateOwner(owner: CustomerDomainModel) {
        self.owner = owner

//        clearProfile()
        profileNameLabel.text = owner.name

        if let imageURL = owner.thumb {
            profileImage.setupImage(fromURL: imageURL)
        }
        else {
                profileImage.tintColor = randomColor.colorForIndex(owner.id)
        }

        if let upcoming = owner.visits_upcoming {
            if let passed = owner.visits_passed {
                totalVisitsLabel.text = "\(upcoming + passed) visits"
                upcomingVisitsLabel.text = "\(upcoming) upcoming"
            }
        }
    }
}

private extension HomeViewController {
    func clearProfile() {
        profileNameLabel.text = ""
        profileImage.image = R.image.big_avatar()
        totalVisitsLabel.text = "0 visits"
        upcomingVisitsLabel.text = "0 upcoming"
    }
}
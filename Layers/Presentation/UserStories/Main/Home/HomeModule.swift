//
//  HomeHomeModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 20/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol HomeModuleInput
{
    func setupDelegate(output: HomeModuleOutput)
    func setupNavigation(nc: UINavigationController)
}

//MARK: Output
protocol HomeModuleOutput: class
{
}


// MARK: - Presenter
final class HomePresenter: HomeModuleInput
    , HomeViewOutput
    , HomeInteractorOutput
    , BookVisitModuleOutput
    , VisitsListModuleOutput
    , CustomersListModuleOutput
{
    weak var resources: Dependencies!
    weak var viewController: HomeViewInput!
    var interactor: HomeInteractorInput!
    weak var router: HomeRouter!
    var ownerLoadFailed = false
    var offlineMode = false
    var customersCount: Int?
    var visitsLoadSuccess = false
    

    
    weak var output: HomeModuleOutput?
    
    init() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(refreshFailedOwner),
                                                         name: NetworkService.networkReachable,
                                                         object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(setOfflineMode),
                                                         name: NetworkService.networkNotReachable,
                                                         object: nil)
    }
    
    func reloadAll() {
        
        router.presentCustomers()
    }
    
    func visitsLoadedWithSuccess(success: Bool) {
        self.visitsLoadSuccess = !success
    }
    
    func visitsDidUpdate(visits : [VisitDomainModel]) {
        
        print("visits update")
    }
    
    func reloadVisits() {
        router.reloadCustomers()
    }
    
    func customersCountUpdated(count: Int) {
        customersCount = count
        if (count < 1) {
            router.viewController.repeatVisitsLabel.hidden = true
        }
        else {
            router.viewController.repeatVisitsLabel.hidden = false
        }
    }
    
    func refreshCounters() {
        DispatchToBackground {
            self.interactor.loadCounters()
        }
    }
    
    func refreshOwner() {
        ownerLoadFailed = false
        DispatchToBackground {
            self.interactor.loadOwner()
        }
    }
    
    @objc func refreshFailedOwner() {
        self.setOnlineMode()
        if (ownerLoadFailed) {
            refreshOwner()
        }
    }
    
    func checkConnectionStatus() {
        if self.offlineMode {
            self.setOfflineMode()
        }
        else {
            self.setOnlineMode()
        }
    }
    
    func setOnlineMode() {
        offlineMode = false
        self.viewController.setOnline()
        if (customersCount < 1) {
            router.viewController.repeatVisitsLabel.hidden = true
        }
        else {
            router.viewController.repeatVisitsLabel.hidden = false
        }
        if !self.visitsLoadSuccess {
            self.reloadVisits()
        }
    }
    
    @objc func setOfflineMode() {
        offlineMode = true
        router.viewController.repeatVisitsLabel.hidden = true
        self.viewController.setOffline()
    }
    
    func setupDelegate(output: HomeModuleOutput)
    {
        self.output = output
    }
    
    func setupNavigation(nc: UINavigationController)
    {
        self.router.navigationController = nc
    }
    
    // MARK: - Interactor Output
    func ownerDidLoad(owner: CustomerDomainModel)
    {
        ownerLoadFailed = false
        viewController.updateOwner(owner)
    }
    
    // MARK: - View Output
    func viewDidLoad()
    {
        router.presentCustomers()
        router.presentVisits()
        if let owner = ApplicationState.loadCustomer() {
            ownerDidLoad(owner)
        }
        refreshOwner()
        refreshCounters()
    }
    
    func bookVisitDidSelect(forCustomer customer: CustomerDomainModel?)
    {
        if self.offlineMode == false {
            DispatchToMainQueue {
                self.router.presentBookVisit()
            }
        }
    }
    
    func ownerDidSelect(owner: CustomerDomainModel)
    {
        if !self.offlineMode {
            router.presentBookVisitForCustomer(owner)
        }
    }
    
    func ownerDidFailed() {
        ownerLoadFailed = true
    }
    
    // MARK: Visit booking output
    func visitBookingDidCanceled()
    {
        DispatchToMainQueue {
            self.router.dismissBookingScreen()
            //            self.refreshCounters()
            self.refreshOwner()
            self.router.presentCustomers()
        }
    }
    
    func presentCustomers() {
        interactor.loadCounters()
    }
    
    func visitDidBooked(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            var tracker = GAI.sharedInstance().defaultTracker
            let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Visit booked", label: nil, value: nil).build() as [NSObject: AnyObject]
            tracker.send(event)
            self.router.dismissBookingScreen()
            //            self.refreshCounters()
            self.refreshOwner()
            self.router.presentCustomers()
        }
        resources.datasources.visits.loadVisits()
    }
    
    
    
    // MARK: VisitsListModuleOutput
    func visitFramePaddingDidChange(to padding: CGFloat)
    {
        router.customersList.changeInsets(to: padding)
    }
    
    func visitNeedToRepeat(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.popToRoot()
            var visit = visit
            visit.date = nil
            visit.id = 0
            self.router.presentUpdateVisit(visit)
        }
    }
    
    func visitWillPayLater(visit: VisitDomainModel)
    {
        //        output?.visitWillPayLater(visit)
    }
    
    func visitNeedToEdit(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.popToRoot()
            self.router.presentEditVisit(visit)
        }
    }
    
    // MARK: CustomersListModuleOutput
    func customerDidSelect(customer: CustomerDomainModel)
    {
        router.presentBookVisitForCustomer(customer)
    }
}
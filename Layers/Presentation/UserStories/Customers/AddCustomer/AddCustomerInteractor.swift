//
//  AddCustomerInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 30/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol AddCustomerInteractorInput: class
{
    func addCustomer(customer: CustomerDomainModel, image: NSData?)
}

//MARK: Output
protocol AddCustomerInteractorOutput: class
{
    func customerDidSave()
    func customerSavingDidFailWithError(error: WTError)
}

// MARK: - Interactor
final class AddCustomerInteractor: AddCustomerInteractorInput
{
    weak var output: AddCustomerInteractorOutput!
    private var customers : CustomersDSInterface

    init(customersDataSource: CustomersDSInterface)
    {
        self.customers = customersDataSource
    }
    
    
    func addCustomer(customer: CustomerDomainModel, image: NSData?)
    {
        customers.createCustomer(customer, image: image, result: { (result) -> () in
            switch result
            {
            case .Success(_): self.output?.customerDidSave()
            case .Failure(let error): self.output?.customerSavingDidFailWithError(error)
            }
        })
    }
}

//
//  AddCustomerRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 30/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class AddCustomerRouter: VIPERRouter
{
    weak var viewController: AddCustomerViewController!
    weak var presenter: AddCustomerPresenter!
    
    override func popCurrentScreen()
    {
        viewController.dismissViewControllerAnimated(true, completion: nil)
    }
}

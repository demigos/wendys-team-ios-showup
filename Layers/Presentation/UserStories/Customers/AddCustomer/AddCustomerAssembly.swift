//
//  AddCustomerAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 30/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class AddCustomerAssembly
{
    class func createModule(resources: Dependencies, configure: (module: AddCustomerModuleInput) -> Void) -> AddCustomerViewController
    {
        let vc = R.storyboard.customers.addCustomerViewController()!
        let interactor = AddCustomerInteractor(customersDataSource: resources.datasources.customers)
        let presenter = AddCustomerPresenter()
        let router = AddCustomerRouter()


        interactor.output = presenter

        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
        router.presenter = presenter

        return vc
    }
}

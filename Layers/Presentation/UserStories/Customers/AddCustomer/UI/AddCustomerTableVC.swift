//
//  AddCustomerTableVC.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 16.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit
import ReactiveCocoa

class CustomerEditForm: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var buttonText = "Change photo" {
        didSet {
            if button != nil {
                button.setTitle(buttonText, forState: .Normal)
            }
        }
    }
    
    let imagePicker = UIImagePickerController()
    var alertController: UIAlertController?
    
    weak var viewController: AddCustomerViewInput?
    @IBOutlet weak var button: RoundedButton!
    // MARK: Form fields
    @IBOutlet weak var genderSwitch: GenderSwitch!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var phoneField: MaskTextField!
    @IBOutlet weak var avatarImage: RoundedImage!
    @IBOutlet weak var emailTextField: UITextField!
    
    var numberOfRows  = 6
    var userId: Int?
    var image: NSData?
    
    // MARK: Interface
    
    func customer() -> CustomerDomainModel {
        var customer = CustomerDomainModel()
        
        if let id = userId {
            customer.id = id
        }
        
        if let name = nameField.text {
            customer.name = name
        }
        
        if let zip = zipCodeField.text {
            customer.zipcode = zip
        }
        
        if let addr = adressField.text {
            customer.address = addr
        }
        
        if let phone = phoneField.text {
            customer.phone = phone
        }
        
        customer.gender = genderSwitch.gender?.rawValue
        
        return customer
    }
    
    func customerImage() -> NSData? {
        return image
    }
    
    @IBAction func photoButtonDidPress(sender: UIButton) {
        
        alertController = UIAlertController(title: "Wendys Team", message: "Please select photo source", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        alertController!.addAction(UIAlertAction(title: "Photos", style: UIAlertActionStyle.Default, handler: takePhotosFromLibrary))
        alertController!.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: takePhotosFromCamera))
        alertController!.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        presentViewController(alertController!, animated: true, completion: nil)
    }
    
    
    func takePhotosFromLibrary(action: UIAlertAction) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func takePhotosFromCamera(action: UIAlertAction) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .Camera
        imagePicker.cameraDevice = .Front
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func zipCodeEditingDidEnd(sender: UITextField) {
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.contentInset = UIEdgeInsetsMake(55, 0, 0, 0)
        automaticallyAdjustsScrollViewInsets = true
        setupBindings()
        imagePicker.delegate = self
        phoneField.setPhone()
        button.setTitle(buttonText, forState: .Normal)
        avatarImage.tintColor = UIColor(red: 165 / 255, green: 215 / 255, blue: 181 / 255, alpha: 1.0)
    }
    
    func bindTextField(textField: UITextField, dataSetter: String? -> Void) {
        textField.rac_textSignal()
            .toSignalProducer()
            .map {
                text in text as! String
            }
            .startWithNext {
                name in
                dataSetter(name)
        }
    }
    
    func setupBindings() {
        bindTextField(zipCodeField) {
            (text) -> Void in
            self.checkFields()
        }
        bindTextField(phoneField) {
            (text) -> Void in
            self.checkFields()
        }
    }
    
    func checkFields() {
        if zipCodeField.text?.characters.count == 5 && phoneField.text?.characters.count == 12 {
            self.viewController?.enableDoneButton(true)
        }
        else {
            print(phoneField.text?.characters.count)
            self.viewController?.enableDoneButton(false)
        }
    }
    
    // MARK: Image picker
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String:AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarImage.contentMode = .ScaleAspectFill
            let image = pickedImage.cropToBounds(400, height: 400)
            avatarImage.image = image
            self.image = UIImagePNGRepresentation(image)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

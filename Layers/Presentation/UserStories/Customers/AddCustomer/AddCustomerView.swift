//
//  AddCustomerView.swift
//  wendysteam-ios
//
//  Created by Alexey on 30/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol AddCustomerViewInput: BaseViewControllerInput
{
    func update(withError error: String)
    func updateWithSuccess()
    func enableDoneButton(enable : Bool)
}

protocol AddCustomerViewOutput: class
{
    func backButtonDidSelect()
    func doneButtonDidSelect(forCustomer customer:CustomerDomainModel, image: NSData?)
}

protocol AddCustomersDelegate{
    func customersAdded()
}

// MARK: - View Controller
final class AddCustomerViewController: BaseViewController
    , AddCustomerViewInput
    , Routable
{
    var output: AddCustomerViewOutput!
    typealias RouterType = AddCustomerRouter
    var router: RouterType!
    var delegate : AddCustomersDelegate?

    var form : CustomerEditForm!
    var image : UIImage?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var doneButton : UIBarButtonItem!

    @IBAction func backButtonAction(sender: AnyObject) {
        output?.backButtonDidSelect()
    }
    
    func enableDoneButton(enable: Bool) {
        self.doneButton.enabled = enable
    }
    
    func updateWithSuccess() {
        self.delegate?.customersAdded()
    }

    @IBAction func doneButtonAction(sender: AnyObject) {
        output?.doneButtonDidSelect(forCustomer: form.customer(), image: form.customerImage())
    }
    
//    func validateInfo() {
//        let disabled = (form.adressField.text?.characters.count ?? 0) == 0
//            || (form.zipCodeField.text?.characters.count ?? 0) != 5
//            || (form.phoneField.text?.characters.count ?? 0) != 10
//        print("\(disabled)")
//        continueButton.enabled = !disabled
//    }

    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        form = R.storyboard.customers.customerEditForm()!
        form.viewController = self
        addChildViewController(form)
        form.view.frame = containerView.bounds
        containerView.addSubview(form.view)
        form.didMoveToParentViewController(self)
    }

    // MARK: View Input
    func update(withError error: String)
    {
        showError(error)
    }
}

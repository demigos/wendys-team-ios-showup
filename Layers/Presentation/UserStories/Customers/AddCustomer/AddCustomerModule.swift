//
//  AddCustomerModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 30/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol AddCustomerModuleInput: class
{
    func setupDelegate(output: AddCustomerModuleOutput)
}

//MARK: Output
protocol AddCustomerModuleOutput: class
{
    func customerDidSave()
}


// MARK: - Presenter
final class AddCustomerPresenter: AddCustomerModuleInput
    , AddCustomerViewOutput
    , AddCustomerInteractorOutput
{
    var interactor: AddCustomerInteractorInput!
    weak var viewController: AddCustomerViewInput!
    weak var router: AddCustomerRouter!
    weak var output: AddCustomerModuleOutput?

    func setupDelegate(output: AddCustomerModuleOutput)
    {
        self.output = output
    }


    // MARK: - Interactor Output
    func customerDidSave() {
        viewController.updateWithSuccess()
        viewController.waitMode(false)
        output?.customerDidSave()
        router.popCurrentScreen()
    }
    
    func customerSavingDidFailWithError(error: WTError) {
        DispatchToMainQueue {
            self.viewController.waitMode(false)
            self.viewController.update(withError: error.errorDescription)
        }
    }

    // MARK: - View Output
    func doneButtonDidSelect(forCustomer customer:CustomerDomainModel, image: NSData?)
    {
        viewController.waitMode(true)
        DispatchToBackground {
            self.interactor.addCustomer(customer, image: image)
        }
    }
    
    func backButtonDidSelect()
    {
        router.popCurrentScreen()
    }
}

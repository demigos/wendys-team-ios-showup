//
//  CustomersListCustomersListInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol CustomersListInteractorInput: class
{
    func refresh()
    func deleteCustomer(customer: CustomerDomainModel)
}

//MARK: Output
protocol CustomersListInteractorOutput: class
{
    func customerDeleted(customer: CustomerDomainModel)
    func customersDidLoad(customers: [CustomerDomainModel])
    func customersDidNotLoad(error: WTError)

    func customerDidNotDelete(error: WTError)
}

// MARK: - Interactor
final class CustomersListInteractor: CustomersListInteractorInput
{
    weak var output: CustomersListInteractorOutput!

    var customers : CustomersDSInterface

    init(customers: CustomersDSInterface)
    {
        self.customers = customers
    }

    func refresh()
    {
        customers.customersListUpdate.observeResult { [weak self] result in
            switch result {
            case .Success(let customers):
                self?.handleCustomers(customers)
            case .Failure(let error):
                self?.output.customersDidNotLoad(error)
            }
        }
        customers.loadCustomers()
    }

    func deleteCustomer(customer: CustomerDomainModel)
    {
        customers.deleteCustomer(customer) { (result) in
            switch result {
            case .Success(_): self.output.customerDeleted(customer)
            case .Failure(let err): self.output.customerDidNotDelete(err)
            }
        }
    }

    //
    // MARK: Private

    func handleCustomers(customers: [CustomerDomainModel]?) {
        if let customers = customers {
            output?.customersDidLoad(customers)
        }
    }

}
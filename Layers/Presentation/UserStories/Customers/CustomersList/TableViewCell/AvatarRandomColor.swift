//
//  AvatarRandomColor.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 12/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class AvatarRandomColor {
    
    let colors = [
        UIColor(red: 0.65, green: 0.84, blue: 0.71, alpha: 1),
        UIColor(red: 0.80, green: 0.66, blue: 0.86, alpha: 1),
        UIColor(red: 0.91, green: 0.69, blue: 0.75, alpha: 1)
    ]
    
    private var currentColor = 0
    
    func nextColor() -> UIColor {
//        var next = currentColor + 1
//        if next >= colors.count {
//            next = 0
//        }
        
//        currentColor = next
        return colors[1]
    }
    
    func colorForIndex(index: Int) -> UIColor {
        //        var next = currentColor + 1
        //        if next >= colors.count {
        //            next = 0
        //        }
        
        //        currentColor = next
        return colors[index % colors.count]
    }
    
    
    
}
//
//  CustomerTableViewCell.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 14.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

enum CustomerCellLayout {
    case Full
    case Short
}

class CustomerTableViewCell: UITableViewCell {
    
    static let identifier = "CustomerTableViewCell"
    
    class func reusableInstance(forTableView tableView: UITableView, customer: CustomerDomainModel, layout: CustomerCellLayout) -> CustomerTableViewCell {
        var cell: CustomerTableViewCell!
        cell = tableView.dequeueReusableCellWithIdentifier(identifier) as? CustomerTableViewCell
        
        if cell == nil {
            tableView.registerNib(R.nib.customerTableViewCell(), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCellWithIdentifier(identifier) as? CustomerTableViewCell
        }
        
        cell.layout = layout
        cell.fill(withModel: customer)
        return cell
    }
    
    
    @IBOutlet weak var customerAvatar: UIImageView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var totalVisitsLabel: UILabel!
    @IBOutlet weak var upcomingVisitsLabel: UILabel!
    @IBOutlet weak var nameYConstraint: NSLayoutConstraint!
    
    
    // MARK: Initialization
    var layout: CustomerCellLayout {
        didSet {
            totalVisitsLabel.hidden = (layout == .Short)
            upcomingVisitsLabel.hidden = (layout == .Short)
            if layout == .Short {
                nameYConstraint.constant = 0
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        self.layout = .Short
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.layout = .Short
        super.init(coder: aDecoder)
    }
    
    let randomColor = AvatarRandomColor()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customerNameLabel.text = ""
        totalVisitsLabel.text = ""
        upcomingVisitsLabel.text = ""
    }
    
    
    func fill(withModel model: CustomerDomainModel) {
        if let name = model.name {
            customerNameLabel.text = name
        }
        
        if let upcoming = model.visits_upcoming {
            if let passed = model.visits_passed {
                totalVisitsLabel.text = "\(upcoming + passed) visits"
                upcomingVisitsLabel.text = "\(upcoming) upcoming"
            }
        }
        
        
        
        if let image = model.thumb {
            customerAvatar.setupImage(fromURL: image)
        } else {
            if var image = UIImage(named: "avatar") {
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                tintColor = randomColor.colorForIndex(model.id)
                customerAvatar.image = image
                
            }
        }
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame.origin.x = 0
    }
    
}

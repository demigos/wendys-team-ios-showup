//
//  CustomersListCustomersListAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class CustomersListAssembly
{
    class func createModule(resources: Dependencies
        , configure: (module: CustomersListModuleInput) -> Void
        ) -> CustomersListViewController
    {
        let vc = R.storyboard.customers.customersListViewController()!
        let interactor = CustomersListInteractor(customers: resources.datasources.customers)
        let presenter = CustomersListPresenter()
        let router = CustomersListRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
        
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc
        router.presenter = presenter

        return vc
    }
}
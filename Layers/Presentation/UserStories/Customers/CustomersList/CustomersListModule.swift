//
//  CustomersListCustomersListModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface

protocol CustomersListModuleInput: class {
    func setupDelegate(output: CustomersListModuleOutput)

    func setEditMode(status: Bool)

    func changeInsets(to padding: CGFloat)
    
    func refreshList()

    func setLayout(layout: CustomerCellLayout)
    
    func setOnlineMode(onlineMode : Bool)
}

//MARK: Output

protocol CustomersListModuleOutput: class {
    func customerDidSelect(customer: CustomerDomainModel)
    func customersCountUpdated(count: Int)
}

extension CustomersListModuleOutput {
    func customersCountUpdated(count: Int) {
        // No operations.
    }
}

// MARK: - Presenter

final class CustomersListPresenter: CustomersListModuleInput
        , CustomersListViewOutput
        , CustomersListInteractorOutput {
    weak var view: CustomersListViewInput!
    var interactor: CustomersListInteractorInput!
    weak var router: CustomersListRouter!
    weak var output: CustomersListModuleOutput?
    weak var resources: Dependencies!

    var loadFailed = false

    init() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                selector: #selector(refreshFailedList),
                name: NetworkService.networkReachable,
                object: nil)
    }
    
    func setOnlineMode(onlineMode : Bool) {
        self.view.setOnlineMode(onlineMode)
    }

    func refreshList() {
        loadFailed = false
        DispatchToBackground {
            self.interactor.refresh()
        }
    }

    @objc func refreshFailedList() {
        if (loadFailed) {
            refreshList()
        }
    }


    func setupDelegate(output: CustomersListModuleOutput) {
        self.output = output
    }

    func setEditMode(status: Bool) {
        view.setEditMode(status)
    }

    func changeInsets(to padding: CGFloat) {
        view.changeInsets(to: padding)
    }

    func setLayout(layout: CustomerCellLayout) {
        view.setLayout(layout)
    }

    // MARK: - Interactor Output
    func customersDidLoad(customers: [CustomerDomainModel]) {
        loadFailed = false
        let clients = customers.filter {
            !$0.isOwner
        }
        DispatchToMainQueue {
            self.view.waitMode(false)
            self.view.update(withCustomers: clients)
            self.output?.customersCountUpdated(clients.count)
        }
    }

    func customerDeleted(customer: CustomerDomainModel) {
        view.waitMode(false)
        interactor.refresh()
    }

    func customersDidNotLoad(error: WTError) {
        loadFailed = true
        DispatchToMainQueue {
            self.view.waitMode(false)
        }
    }

    func customerDidNotDelete(error: WTError) {
        view.waitMode(false)
        view.update(withError: error)
    }

    // MARK: - View Output
    func viewDidLoad() {
        view.waitMode(true)
        DispatchToBackground {
            self.interactor.refresh()
        }
    }

    func addCustomerDidSelect() {
        router.presentAddCustomer()
    }

    func customerWasSelected(customer: CustomerDomainModel) {
        output?.customerDidSelect(customer)
    }

    func deleteCustomer(customer: CustomerDomainModel) {
        view.waitMode(true)
        interactor.deleteCustomer(customer)
    }

}
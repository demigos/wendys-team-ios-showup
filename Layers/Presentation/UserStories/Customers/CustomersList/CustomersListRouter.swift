//
//  CustomersListCustomersListRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class CustomersListRouter: VIPERRouter
{
    weak var view: CustomersListViewController!
    weak var presenter: CustomersListPresenter!
    
    func presentAddCustomer()
    {
        let vc = AddCustomerAssembly.createModule(presenter.resources) { (module) in
        }
         vc.delegate = self.view
        vc.presentModal(from: view)
    }
}
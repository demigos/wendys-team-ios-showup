//
//  CustomersListCustomersListView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol CustomersListViewInput: BaseViewControllerInput
{
    func update(withCustomers customers: [CustomerDomainModel])
    func changeInsets(to padding: CGFloat)
    func setEditMode(status: Bool)
    func update(withError error: WTError)
    func setLayout(layout: CustomerCellLayout)
    func setOnlineMode(onlineMode : Bool)
}

protocol CustomersListViewOutput: class
{
    func addCustomerDidSelect()
    func customerWasSelected(customer: CustomerDomainModel)
    func deleteCustomer(customer: CustomerDomainModel)
    func viewDidLoad()
}

// MARK: - View Controller
final class CustomersListViewController: BaseViewController
    , CustomersListViewInput
    , Routable
    , UITableViewDataSource
    , UITableViewDelegate
    , AddCustomersDelegate
{
    
    var output: CustomersListViewOutput!
    typealias RouterType = CustomersListRouter
    var router: RouterType!
    var online = true
    
    var layout: CustomerCellLayout = .Full
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var personsCount: UILabel!
    @IBOutlet weak var managePanel: UIView!
    @IBOutlet weak var managePanelHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    @IBOutlet weak var addPersonView: UIView!
    
    var manageble: Bool = false {
        didSet {
            managePanel.hidden = !manageble
            managePanelHeight.constant = manageble ? 44 : 0
            addPersonView.hidden = !manageble
        }
    }
    
    var customers : [CustomerDomainModel]?
    
    @IBAction func addPersonAction(sender: AnyObject)
    {
        if self.online {
            output?.addCustomerDidSelect()
        }
    }
    
    func customersAdded() {
        self.viewDidLoad()
    }
    
    func setOnlineMode(onlineMode: Bool) {
        self.online = onlineMode
        if onlineMode {
            self.addPersonView.alpha = 1
        }
        else {
            self.addPersonView.alpha = 0.5
        }
    }
    
    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        output.viewDidLoad()
        separatorHeight.constant = 0.5
        if self.layout == .Full {
            self.addPersonView.frame = CGRect(x: 0, y: 0, width: 200, height: 0)
            self.addPersonView.hidden = true
        }
        else {
            self.addPersonView.hidden = false
        }
    }
    
    // MARK: View Input
    func update(withCustomers customers: [CustomerDomainModel])
    {
        self.customers = customers
        if customers.count < 1 {
            self.addPersonView.frame = CGRect(x: 0, y: 0, width: 200, height: 75)
            self.addPersonView.hidden = false
        }
        else {
            if self.layout == .Full
            {            self.addPersonView.frame = CGRect(x: 0, y: 0, width: 200, height: 0)
                self.addPersonView.hidden = true
            }
        }
        tableView.reloadData()
    }
    
    func changeInsets(to padding: CGFloat)
    {
        tableView.contentInset.bottom = padding
    }
    
    func setEditMode(status: Bool)
    {
        tableView.editing = status
    }
    
    func update(withError error: WTError)
    {
        showError(error.errorDescription)
    }
    
    func setLayout(layout: CustomerCellLayout)
    {
        self.layout = layout
    }
    
    //MARK: - DataSource
    
    func customerForIndexPath(indexPath: NSIndexPath) -> CustomerDomainModel?
    {
        if let customers = customers where indexPath.row < customers.count {
            return customers[indexPath.row]
        }
        
        return nil
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = customers?.count {
            return count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let customer = customerForIndexPath(indexPath)
        {
            return CustomerTableViewCell.reusableInstance(forTableView: tableView, customer: customer, layout: layout)
        }
        
        return UITableViewCell()
    }
    
    //MARK: - Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let customer = customerForIndexPath(indexPath) {
            output?.customerWasSelected(customer)
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        guard editingStyle == .Delete else { return }
        
        if let customer = customerForIndexPath(indexPath) {
            output.deleteCustomer(customer)
        }
    }
    
}
//
//  CustomerProfileModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol CustomerProfileModuleInput: class
{
    func setupDelegate(output: CustomerProfileModuleOutput)
    func setupCustomer(customer: CustomerDomainModel)
}

//MARK: Output
protocol CustomerProfileModuleOutput: class
{
    func logout()
    func customerDidUpdate()
}


// MARK: - Presenter
final class CustomerProfilePresenter: CustomerProfileModuleInput
    , CustomerProfileViewOutput
    , CustomerProfileInteractorOutput
{
    var interactor: CustomerProfileInteractorInput!
    weak var viewController: CustomerProfileViewInput!
    weak var router: CustomerProfileRouter!
    weak var output: CustomerProfileModuleOutput?
    
    private var customer: CustomerDomainModel!

    func setupDelegate(output: CustomerProfileModuleOutput)
    {
        self.output = output
    }

    func setupCustomer(customer: CustomerDomainModel)
    {
        self.customer = customer
    }

    // MARK: - Interactor Output
    func customerDidUpdate()
    {
        DispatchToMainQueue {
            NSNotificationCenter.defaultCenter().postNotificationName(ProfileDidUpdateNotification, object: nil)
            self.viewController.waitMode(false)
            self.output?.customerDidUpdate()
            self.router.popCurrentScreen()
        }
    }
    
    func customerUpdatingDidFail(withError error: WTError)
    {
        DispatchToMainQueue {
            self.viewController.waitMode(false)
            self.viewController.showError(error.errorDescription)
        }
    }

    // MARK: - View Output
    func didLoad()
    {
        viewController.update(customer: customer)
    }
    
    func backActionDidSelect()
    {
        router.popCurrentScreen()
    }
    
    func logoutActionDidSelect()
    {
        viewController.waitMode(true)
        output?.logout()
    }
    
    func doneButtonDidSelect(forCustomer customer:CustomerDomainModel, image: NSData?)
    {
        viewController.waitMode(true)
        DispatchToBackground {
            self.interactor.updateCustomer(customer, image: image)
        }
    }
}

//
//  CustomerProfileInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol CustomerProfileInteractorInput: class
{
    func updateCustomer(customer: CustomerDomainModel, image: NSData?)
}

//MARK: Output
protocol CustomerProfileInteractorOutput: class
{
    func customerDidUpdate()
    func customerUpdatingDidFail(withError error: WTError)
}

// MARK: - Interactor
final class CustomerProfileInteractor: CustomerProfileInteractorInput
{
    weak var output: CustomerProfileInteractorOutput!
    private var customers : CustomersDSInterface

    init(customersDataSource: CustomersDSInterface)
    {
        self.customers = customersDataSource
    }

    func updateCustomer(customer: CustomerDomainModel, image: NSData?)
    {
        customers.updateCustomer(customer, image: image, result: { (result) in
            switch result
            {
            case .Success(_): self.output?.customerDidUpdate()
            case .Failure(let error): self.output?.customerUpdatingDidFail(withError: error)
                break
            }
        })
    }
}

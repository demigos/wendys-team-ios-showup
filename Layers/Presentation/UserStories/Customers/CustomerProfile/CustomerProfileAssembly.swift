//
//  CustomerProfileAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class CustomerProfileAssembly
{
    class func createModule(resources: Dependencies, configure: (module: CustomerProfileModuleInput) -> Void) -> CustomerProfileViewController
    {
        let vc = R.storyboard.customers.customerProfileViewController()!
        let interactor = CustomerProfileInteractor(customersDataSource: resources.datasources.customers)
        let presenter = CustomerProfilePresenter()
        let router = CustomerProfileRouter()


        interactor.output = presenter

        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
        router.presenter = presenter

        return vc
    }
}

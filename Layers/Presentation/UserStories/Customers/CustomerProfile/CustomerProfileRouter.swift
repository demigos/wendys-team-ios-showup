//
//  CustomerProfileRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class CustomerProfileRouter: VIPERRouter
{
    weak var viewController: CustomerProfileViewController!
    weak var presenter: CustomerProfilePresenter!
    
    override func popCurrentScreen()
    {
        viewController.dismissViewControllerAnimated(true, completion: nil)
    }
}

//
//  CustomerProfileView.swift
//  wendysteam-ios
//
//  Created by Alexey on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol CustomerProfileViewInput: BaseViewControllerInput
{
    func update(customer customer: CustomerDomainModel)
}

protocol CustomerProfileViewOutput: class
{
    func didLoad()
    func backActionDidSelect()
    func logoutActionDidSelect()
    func doneButtonDidSelect(forCustomer customer:CustomerDomainModel, image: NSData?)
}

// MARK: - View Controller
final class CustomerProfileViewController: BaseViewController
    , CustomerProfileViewInput
    , Routable
{
    var output: CustomerProfileViewOutput!
    typealias RouterType = CustomerProfileRouter
    var router: RouterType!

    var form : CustomerEditForm!
    let randomColor = AvatarRandomColor()

    @IBOutlet weak var containerView: UIView!


    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        form = R.storyboard.customers.customerEditForm()!

        addChildViewController(form)
        form.view.frame = containerView.bounds
        containerView.addSubview(form.view)
        form.didMoveToParentViewController(self)

        output.didLoad()
    }

    // MARK: Actions

    @IBAction func backAction(sender: AnyObject)
    {
        output.backActionDidSelect()
    }

    @IBAction func doneAction(sender: AnyObject)
    {
        output.doneButtonDidSelect(forCustomer: form.customer(), image: form.customerImage())
    }

    @IBAction func logoutAction(sender: AnyObject)
    {
        output.logoutActionDidSelect()
    }


    // MARK: View Input
    func update(customer customer: CustomerDomainModel)
    {

        if customer.isOwner {
            form.nameField.placeholder = "Enter your name"
            form.adressField.placeholder = "Enter your address"
            form.zipCodeField.placeholder = "Enter your ZIP code"
            form.phoneField.placeholder = "Enter your number"
            form.numberOfRows = 7
        }

        form.userId = customer.id

        if let name = customer.name {
            form.nameField.text = name
        } else {
            form.nameField.text = ""
        }

        if let address = customer.address {
            form.adressField.text = address
        } else {
            form.adressField.text = ""
        }

        if let zip = customer.zipcode {
            form.zipCodeField.text = zip
        } else {
            form.zipCodeField.text = ""
        }

        if let phone = customer.phone {
            form.phoneField.text = phone
        } else {
            form.phoneField.text = ""
        }

        form.emailTextField.enabled = false
        if let email = customer.email {
            form.emailTextField.text = email
        } else {
            form.emailTextField.text = ""
        }

        if let image = customer.thumb {
            form.avatarImage.setupImage(fromURL: image)
        } else {
            if var image = UIImage(named: "avatar") {
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                form.avatarImage.tintColor = randomColor.colorForIndex(customer.id)
//                form.avatarImage.tintColor = UIColor.blueColor()
                form.avatarImage.image = image

            }
        }

        if let gender = customer.gender {
            form.genderSwitch.gender = Gender(rawValue: gender)!
        }

    }
}

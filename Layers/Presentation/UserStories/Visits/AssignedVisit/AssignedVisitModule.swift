//
//  AssignedVisitModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol AssignedVisitModuleInput
{
    func setupDelegate(output: AssignedVisitModuleOutput)
}

//MARK: Output
protocol AssignedVisitModuleOutput: class
{
    func dismissBookingScreen()
}


// MARK: - Presenter
final class AssignedVisitPresenter: AssignedVisitModuleInput
    , AssignedVisitViewOutput
    , AssignedVisitInteractorOutput,
    VisitDetailModuleOutput, BookVisitModuleOutput
{
    weak var view: AssignedVisitViewInput!
    var interactor: AssignedVisitInteractorInput!
	weak var router: AssignedVisitRouter!
    weak var output: AssignedVisitModuleOutput?
    
    func visitBookingDidCanceled() {
        self.view.discard()
    }
    
    func visitDidBooked(visit: VisitDomainModel) {
        self.view.discard()
    }

    func setupDelegate(output: AssignedVisitModuleOutput)
    {
        self.output = output
    }
    
    func visitDidCanceled(visit: VisitDomainModel) {
        self.view.discard()
    }
    
    func visitNeedToEdit(visit: VisitDomainModel) {
        
    }
    
    func visitNeedToRepeat(visit: VisitDomainModel) {
        DispatchToMainQueue {
//            self.router.popToRoot()
            var visit = visit
            visit.date = nil
            visit.id = 0
            self.router.presentUpdateVisit(visit)
            
            
        }
    }
    
    func visitWillPayLater(visit: VisitDomainModel) {
        
    }
    
    func visitUpdated(visit: VisitDomainModel) {
        DispatchToMainQueue {
            self.view.update(withVisit: visit)
        }
    }
    
    func detailsDidSelect(visit: VisitDomainModel) {
        router.presentVisit(visit)
    }
    
    func loadVisitByID(visitID : Int) {
        interactor.loadVisit(visitID)
    }

    func cancelButtonDidSelect() {
        DispatchToMainQueue {
            self.output?.dismissBookingScreen()
        }
    }

    // MARK: - Interactor Output

    // MARK: - View Output
}
//
//  AssignedVisitView.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol AssignedVisitViewInput: class {
    func update(withVisit visit: VisitDomainModel)
    
    func discard()
}

protocol AssignedVisitViewOutput: class {
    func cancelButtonDidSelect()
    
    func loadVisitByID(visitID: Int)
    
    func detailsDidSelect(visit: VisitDomainModel)
}

// MARK: - View Controller

final class AssignedVisitViewController: BaseViewController
    , AssignedVisitViewInput
, Routable {
    var output: AssignedVisitViewOutput!
    typealias RouterType = AssignedVisitRouter
    var router: RouterType!
    var visitId: Int?
    var visit: VisitDomainModel?
    let randomColor = AvatarRandomColor()
    
    
    @IBOutlet weak var tmAvatarImageView: RoundedImage!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var teamMemberLabel: UILabel!
    
    @IBOutlet weak var infoView: UIView!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        infoView.hidden = true
        if let id = self.visitId {
            self.output.loadVisitByID(id)
        }
        /* Initialization here */
    }
    
    func discard() {
        if let nc = navigationController {
            router.popCurrentScreen()
        } else {
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
    func update(withVisit visit: VisitDomainModel) {
        self.visit = visit
        infoView.hidden = false
        if let date = visit.date {
            let dateFormatter = NSDateFormatter()
            let cal = NSCalendar.currentCalendar()
            var components = cal.components(.Day, fromDate: NSDate())
            let today = cal.dateFromComponents(components)
            components = cal.components(.Day, fromDate: date)
            let visitDate = cal.dateFromComponents(components)
            var dateText = ""
            var timeText = ""
            
            dateFormatter.dateFormat = "hh:mm aa"
            let timeString = dateFormatter.stringFromDate(date)
            
            if let t = today, let vd = visitDate where t.isEqualToDate(vd) == true {
                dateText = "Today "
                timeText = "Today \(timeString)"
            } else {
                dateFormatter.dateFormat = "MM.dd"
                dateText = dateFormatter.stringFromDate(date)
                timeText = timeString
            }
            
            
            dateLabel.text = dateText
            
            timeLabel.text = timeText
        }
        if let name = visit.customerName {
            self.customerLabel.text = name
        }
        if let teamName = visit.teamMemberName {
            self.teamMemberLabel.text = teamName
        }
        if let imageUrl = visit.teamMemberAvatar {
            tmAvatarImageView.setupImage(fromURL: imageUrl)
        }
        else {
            if var image = UIImage(named: "avatar") {
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                tmAvatarImageView.tintColor = randomColor.nextColor()
                tmAvatarImageView.image = image
                
            }
        }
    }
    
    // MARK: View Input
    
    @IBAction func detailsButtonDidSelect(sender: AnyObject) {
        if let visit = visit {
            output.detailsDidSelect(visit)
        }
        
    }
    
    @IBAction func cancelButtonDidSelect(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
}
//
//  AssignedVisitRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class AssignedVisitRouter: VIPERRouter
{
    weak var viewController: AssignedVisitViewController!
    weak var presenter: AssignedVisitPresenter!
    var resources: Dependencies!
    
    
    func presentVisit(visit: VisitDomainModel)
    {
        VisitDetailAssembly.createModule(resources) { (module) in
            module.setupDelegate(self.presenter)
            //            module.setupNavigation(self.navigationController!)
            module.setupVisit(visit)
            }.presentModal(from: viewController)
    }
    
    func presentUpdateVisit(visit : VisitDomainModel) {
        var flow = BookVisitDefaultFlow()
        flow.initialStep = .DurationAndTime
        BookVisitAssembly.createModule(resources: resources, flow: flow) {
            (module) in
            module.setupDelegate(self.presenter)
            module.updateVisit(visit)
            }.presentModal(from: viewController)
    }
    
}
//
//  AssignedVisitInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol AssignedVisitInteractorInput: class
{
    func loadVisit(visitId : Int)
}

//MARK: Output
protocol AssignedVisitInteractorOutput: class
{
    func visitUpdated(visit: VisitDomainModel)
}

// MARK: - Interactor
final class AssignedVisitInteractor: AssignedVisitInteractorInput
{
    weak var output: AssignedVisitInteractorOutput!
    
    private var visits : VisitsDSInterface
    
    init(visitsDS: VisitsDSInterface)
    {
        self.visits = visitsDS
    }
    
    func loadVisit(visitId: Int)
    {
        visits.loadVisit(UInt(visitId)) { result in
            switch result {
            case .Success(let updated_visit): self.output.visitUpdated(updated_visit)
            case .Failure(_): break
            }
        }
    }
}
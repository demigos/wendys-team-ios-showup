//
//  AssignedVisitAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class AssignedVisitAssembly
{
    class func createModule(resources: Dependencies, configure: (module: AssignedVisitModuleInput) -> Void) -> AssignedVisitViewController
    {
        let vc = R.storyboard.visits.assignedVisitViewController()!
        let interactor = AssignedVisitInteractor(visitsDS: resources.datasources.visits)
        let presenter = AssignedVisitPresenter()
        let router = AssignedVisitRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.resources = resources
        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
import UIKit

final class UnpaidVisitsRouter: VIPERRouter, PaidVisitDelegate
{
    weak var view: UnpaidVisitsViewController!
    weak var presenter: UnpaidVisitsPresenter!
    var resources: Dependencies!
    var successView: UIViewController?
    var flow: UnpaidFlow?


    func presentVisit(visit: VisitDomainModel)
    {
        let paid = PaidVisitAssembly.createModule(resources, flow: self.flow) { module in }
        paid.visitId = visit.id
        paid.unpaid = true
        paid.delegate = self
        if let nc = navigationController ?? view.navigationController {
            paid.present(inNavigation: nc)
        } else {
            paid.presentModal(from: view)
        }
    }

    func visitPaid(visitId: Int) {
        self.removeVisitById(visitId)
    }

    func removeVisitById(visitId: Int) {
        var removedIndex = -1
        if let unpaidVisits = self.flow?.visits?.unpaid {
            for index in 0..<unpaidVisits.count {
                if unpaidVisits[index].id == visitId {
                    removedIndex = index
                }
            }
            if removedIndex >= 0 {
                self.flow?.visits?.unpaid.removeAtIndex(removedIndex)
                self.flow?.delegate?.updateUnpaidCounter((self.flow?.visits?.unpaid)!)
            }
        }
        removedIndex = -1
        if let noFeedbackVisits = self.flow?.visits?.nofeedback {
            for index in 0..<noFeedbackVisits.count {
                if noFeedbackVisits[index].id == visitId {
                    removedIndex = index
                }
            }
            if removedIndex >= 0 {
                self.flow?.visits?.nofeedback.removeAtIndex(removedIndex)
            }
        }
        if self.flow?.visits?.unpaid.count < 1 {
            self.unpaidViewClosing()
        }
        else {
            self.view.visitPaid(visitId)
        }
    }

    func allVisitsPayed() {
        if let flow = self.flow {
            if flow.visits?.nofeedback.count > 0 {
                let paid = PaidVisitAssembly.createModule(resources, flow: self.flow) { module in }
                paid.visitId = flow.visits?.nofeedback[0].id
                paid.nofeedback = true
                if let nc = navigationController ?? view.navigationController {
                    paid.present(inNavigation: nc)
                } else {
                    if let successView = view.success {
                        paid.presentModal(from: successView)
                    }
                    else {
                        paid.presentModal(from: view)
                    }
                }
            }
            else {
                self.popToRoot()
            }
        }
    }


    func unpaidViewClosing() {
        self.allVisitsPayed()
    }

    func dismissDialog() {
        navigationController?.popToRootViewControllerAnimated(true)
        //        successView?.removeFromParentViewController()
        //        self.navigationController?.popViewControllerAnimated(false)
    }

    func presentSuccessDialog()
    {
        guard let success = view.success else {
            return
        }
        success.titleText = "Thank you!"
        success.summaryText = "Your payment was successful.\r\n\r\nSincerely,\r\nWendy’s Team"

        if let nc = view.navigationController {
            present(success, using: nc)
        } else {
            self.successView = success
            presentModal(success, from: view)
        }
    }

}
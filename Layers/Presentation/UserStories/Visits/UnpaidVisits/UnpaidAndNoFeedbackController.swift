import UIKit

final class UnpaidAndNoFeedbackController: UINavigationController, Routable {
    //    var output: BookVisitViewOutput!
    typealias RouterType = UnpaidVisitsRouter
    var router: RouterType!
    
    // MARK: - Life cycle
    deinit {
        print("[D] \(self) destroyed")
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
}

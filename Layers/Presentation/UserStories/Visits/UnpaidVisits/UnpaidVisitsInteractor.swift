import Foundation

protocol UnpaidVisitsInteractorInput: class
{
    func refreshVisits()
    func payForVisits(visits: [VisitDomainModel])
    func loadUnpaidVisits()
}

//MARK: Output
protocol UnpaidVisitsInteractorOutput: class
{
    func paymentSentForVisits(visits: [VisitDomainModel])
    func sentFailed(error: WTError)
    func visitsLoaded(visits : [VisitDomainModel])
    func visitsDidNotLoad(error: WTError)
}

final class UnpaidVisitsInteractor: UnpaidVisitsInteractorInput
{
    weak var output: UnpaidVisitsInteractorOutput!
    private var visits : VisitsDSInterface
    
    init(visitsDS: VisitsDSInterface)
    {
        self.visits = visitsDS
    }
    
    func setup()
    {
    }
    
    func payForVisits(visits: [VisitDomainModel]) {
        self.visits.payForVisits(visits) { result in
            switch result {
            case .Success(_):
                self.output.paymentSentForVisits(visits)
                var tracker = GAI.sharedInstance().defaultTracker
                let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Payment Success", label: nil, value: nil).build() as [NSObject: AnyObject]
                tracker.send(event)
            case .Failure(let error): self.output.sentFailed(error)
            }
        }
    }
    
    func handleVisits(visits:[VisitDomainModel])
    {
    }
    
    func loadUnpaidVisits() {
        visits.unpaidNoFeedbackUpdate.observeResult { result in
            switch result {
            case .Success(let visits):
                self.output.visitsLoaded(visits.unpaid)
//                self.output.visitsDidUpdate(visits)
            case .Failure(let error):
                self.output.visitsDidNotLoad(error)
                return
            }
        }
        visits.loadUnpaidAndNoFeedbackVisits()
    }
    
    func refreshVisits()
    {
        setup()
        visits.loadVisits()
    }
}
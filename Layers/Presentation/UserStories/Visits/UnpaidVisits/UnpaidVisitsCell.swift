import UIKit

class UnpaidVisitsCell: UITableViewCell {
    
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var clientLabel: UILabel!
    @IBOutlet weak var teamMemberLabel: UILabel!

}

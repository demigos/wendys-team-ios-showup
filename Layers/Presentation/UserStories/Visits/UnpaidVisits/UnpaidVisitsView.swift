import UIKit

// MARK: - Interface
protocol UnpaidVisitsViewInput: BaseViewControllerInput
{
//    func paymentSent()
    func reloadDataWithNewVisits(visits : [VisitDomainModel])
    func update(withError error: WTError)
}

protocol UnpaidVisitsViewOutput: class
{
    func sendFeedback()
//    func paymentSent()
    func payForVisits(visits : [VisitDomainModel])
    func successDoneButtonDidSelect()
    func removeVisitById(visitId: Int)
    func closeUnpaidsList()
    func loadVisits()
}

extension RangeReplaceableCollectionType where Generator.Element : Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}

final class UnpaidVisitsViewController: BaseViewController
    , UnpaidVisitsViewInput
    , Routable
    ,UITableViewDataSource
    , UITableViewDelegate
{
    var output: UnpaidVisitsViewOutput!
    typealias RouterType = UnpaidVisitsRouter
    var router: RouterType!
    var visits = [VisitDomainModel]()
    var success: SuccessFeedbackViewController?
    let randomColor = AvatarRandomColor()
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var payButton: PayButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupTitle()
        self.setupPayment()
        self.waitMode(true)
        output.loadVisits()
    }
    
//    func paymentSent() {
//        output.paymentSent()
//    }

    @IBAction func phoneButtonAction(sender: AnyObject) {
        PhoneService.call()
    }
    
    func reloadDataWithNewVisits(visits: [VisitDomainModel]) {
        self.waitMode(false)
        self.visits = visits
        self.setupTitle()
        self.setupPayment()
        self.tableView.reloadData()
        //        output.loadVisits()
    }
    
    func visitPaid(visitId: Int) {
        var removedIndex = -1
        for index in 0..<self.visits.count {
            if self.visits[index].id == visitId {
                removedIndex = index
            }
        }
        if removedIndex >= 0 {
            self.visits.removeAtIndex(removedIndex)
            self.setupTitle()
            self.tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.visits.count
    }
    
    func update(withError error: WTError)
    {
        self.waitMode(false)
        showError(error.errorDescription)
    }
    
    
    @IBAction func payForAllButtonAction(sender: AnyObject) {
        output?.payForVisits(self.visits)
    }
    
    @IBAction func payLaterAction(sender: AnyObject) {
        self.output.closeUnpaidsList()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "UnpaidListCell"
        
        if let cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! UnpaidVisitsCell! {
            if let imageUrl = self.visits[indexPath.row].teamMemberAvatar {
                cell.serviceImage.setupImage(fromURL: imageUrl)
            }
            else {
                if var image = UIImage(named: "avatar") {
                    image = image.imageWithRenderingMode(.AlwaysTemplate)
                    cell.serviceImage.tintColor = randomColor.nextColor()
                    cell.serviceImage.image = image
                    
                }
            }
            if let cost = self.visits[indexPath.row].cost {
                cell.priceLabel.text = "$\(cost)"
            }
            if let failureMessage = self.visits[indexPath.row].failureMessage {
                if failureMessage.characters.count > 0 {
                    cell.backgroundColor = UIColor.init(red: 1, green: 0, blue: 0, alpha: 0.1)
                }
            }
            else {
                cell.backgroundColor = UIColor.clearColor()
            }
            if let date = self.visits[indexPath.row].date {
                let dateFormatter = NSDateFormatter()
                let cal = NSCalendar.currentCalendar()
                var components = cal.components(.Day, fromDate: NSDate() )
                let today = cal.dateFromComponents(components)
                components = cal.components(.Day, fromDate: date)
                let visitDate = cal.dateFromComponents(components)
                var dateText = ""
                var timeText = ""
                
                dateFormatter.dateFormat = "hh:mm aa"
                let timeString = dateFormatter.stringFromDate(date)
                
                if let t = today, let vd = visitDate where t.isEqualToDate(vd) == true {
                    dateText = "Today "
                    timeText = "Today \(timeString)"
                } else {
                    dateFormatter.dateFormat = "MM.dd"
                    dateText = dateFormatter.stringFromDate(date)
                    timeText = timeString
                }
                cell.dateLabel.text = dateText
                cell.timeLabel.text = timeText
            }
            cell.clientLabel.text = self.visits[indexPath.row].customerName
            cell.teamMemberLabel.text = self.visits[indexPath.row].teamMemberName
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    func setupTitle() {
        if visits.count == 0 {
            self.navigationBar.topItem?.title = "UNPAID VISITS"
        }
        else {
            var containFailedVisits = false
            for visit in self.visits {
                if visit.failureMessage != nil && visit.failureMessage?.characters.count > 0 {
                    containFailedVisits = true
                }
            }
            if containFailedVisits {
                self.navigationBar.topItem?.title = "\(visits.count) FAILED AND UNPAID VISITS"
            }
            else {
                self.navigationBar.topItem?.title = "\(visits.count) UNPAID VISITS"
            }
        }
    }
    
    func setupPayment() {
        var summaryCost: Float = 0.0
        var summaryTimeHours: Int = 0
        var summaryTimeMinutes: Int = 0
        for visit in self.visits {
            if let cost = visit.cost {
                summaryCost += cost
            }
            if let time = visit.actualDuration {
                let (hours, minutes) = time.separateHoursMinutes()
                summaryTimeHours += Int(hours)
                summaryTimeMinutes += Int(minutes)
            }
            else {
                if visit.status != .Cancelled {
                    summaryTimeHours += 24
                }
            }
        }
        summaryTimeHours += summaryTimeMinutes / 60
        summaryTimeMinutes = summaryTimeMinutes % 60
        payButton.costLabel.text = "$\(summaryCost)"
        var finalString = "\(summaryTimeHours) h"
        if summaryTimeMinutes > 0 {
            finalString += " \(summaryTimeMinutes) m"
        }
        payButton.hoursLabel.text = finalString
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.router.presentVisit(self.visits[indexPath.row])
        
    }
    
    @IBAction func backButtonAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

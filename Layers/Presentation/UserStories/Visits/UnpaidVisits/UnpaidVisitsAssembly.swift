import UIKit

final class UnpaidVisitsListAssembly
{
    class func createModule(resources: Dependencies, visits: UnpaidNoFeedbackVisitsDomainModel, flow: UnpaidFlow, configure: (module: UnpaidVisitsModuleInput) -> Void) -> UnpaidVisitsViewController
    {
        
//        
//        if visits.unpaid.count > 1 {
//            self.output.showUnpaidView(loadedVisits)
//        }
//        else {
//            if visits.unpaid.count == 1 {
//                let id  = visits.unpaid[0].id
//                self.output.showVisitViewForVisit(id)
//            }
//            else {
//                if visits.nofeedback.count > 0 {
//                    self.output.showVisitViewForVisit(visits.nofeedback[0].id)
//                }
//            }
//        }
//        
        
        
        let vc = R.storyboard.rateAndFeedback.unpaidVisitsViewController()!
        let interactor = UnpaidVisitsInteractor(visitsDS: resources.datasources.visits)
        let presenter = UnpaidVisitsPresenter()
        let router = UnpaidVisitsRouter()
        router.flow = flow
        
        let successDialog = R.storyboard.rateAndFeedback.successFeedbackViewController()!
        successDialog.parent2 = vc
        vc.success = successDialog
        
        interactor.output = presenter
        
        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router
        
        configure(module: presenter)
        
        vc.output = presenter
        vc.router = router
//        if let visits = visits.unpaid {
//            vc.visits = visits.unpaid
//        }
        
        router.view = vc
        router.resources = resources
        router.presenter = presenter
        router.navigationController = vc.navigationController

        
        return vc
    }
}
import UIKit


// MARK: - Module interface
protocol UnpaidVisitsModuleInput: class
{
    func setupNavigation(nc: UINavigationController)
}

//MARK: Output
protocol UnpaidVisitsModuleOutput: class
{
    func feedBackSent()
}

protocol UnpaidFlowDelegate: class {
    func updateUnpaidCounter(unpaidVisits: [VisitDomainModel])
}

class UnpaidFlow {
    
    var visits : UnpaidNoFeedbackVisitsDomainModel?
    var lastSentFeedbackIndex = -1
    weak var delegate: UnpaidFlowDelegate?
    
    func initialViewType() -> Int {
        if self.visits?.unpaid.count > 1 {
            return 0
        }
        if self.visits?.unpaid.count == 1 {
            return 1
        }
        if self.visits?.nofeedback.count > 0 {
            return 2
        }
        return 3
    }
    
    convenience init(visits : UnpaidNoFeedbackVisitsDomainModel?) {
        self.init()
        self.visits = visits
    }
    
    func removeUnpaidVisit(id: Int)  {
        var removedIndex = -1
        if let unpaidVisits = self.visits?.unpaid {
            for index in 0..<unpaidVisits.count {
                if unpaidVisits[index].id == id {
                    removedIndex = index
                }
            }
            if removedIndex >= 0 {
                self.visits?.unpaid.removeAtIndex(removedIndex)
                self.delegate?.updateUnpaidCounter((self.visits?.unpaid)!)
            }
        }
        removedIndex = -1
        if let noFeedbackVisits = self.visits?.nofeedback {
            for index in 0..<noFeedbackVisits.count {
                if noFeedbackVisits[index].id == id {
                    removedIndex = index
                }
            }
            if removedIndex >= 0 {
                self.visits?.nofeedback.removeAtIndex(removedIndex)
            }
        }
    }
}

// MARK: - Presenter
final class UnpaidVisitsPresenter: UnpaidVisitsModuleInput
    , UnpaidVisitsViewOutput
    , UnpaidVisitsInteractorOutput
    , VisitDetailModuleOutput
{
    var interactor: UnpaidVisitsInteractorInput!
    weak var viewController: UnpaidVisitsViewInput!
    weak var router: UnpaidVisitsRouter!
    weak var output: UnpaidVisitsModuleOutput?
    var visits = [VisitDomainModel]()
    
    
    func payForVisits(visits: [VisitDomainModel]) {
        viewController.waitMode(true)
        interactor.payForVisits(visits)
    }
    
    func sendFeedback() {
        
    }
    
    func loadVisits() {
       interactor.loadUnpaidVisits()
    }
    
    func visitsDidNotLoad(error: WTError) {
        DispatchToMainQueue {
            self.viewController.waitMode(false)
        }
    }
    
    func visitsLoaded(visits: [VisitDomainModel]) {
        self.visits = visits
        self.router.flow?.visits?.unpaid = self.visits
        self.viewController.reloadDataWithNewVisits(visits)
    }
    
    func closeUnpaidsList() {
        self.router.unpaidViewClosing()
    }
    
    func paymentSentForVisits(visits: [VisitDomainModel]) {
//        for visit in visits {
//            self.router.flow?.removeUnpaidVisit(visit.id)
//        }
        self.viewController.waitMode(false)
        self.router.presentSuccessDialog()
    }
    
    func sentFailed(error: WTError) {
        self.viewController.waitMode(false)
        viewController.update(withError: error)
    }
    
    func removeVisitById(visitId: Int) {
        router.removeVisitById(visitId)
    }
    
    func successDoneButtonDidSelect() {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        router.flow?.delegate?.updateUnpaidCounter([VisitDomainModel]())
        router.allVisitsPayed()
    }
    
    func feedBackSent() {
        self.router.presentSuccessDialog()
    }
    
    func visitDidCanceled(visit: VisitDomainModel) {
        
    }
    func visitNeedToEdit(visit: VisitDomainModel) {
        
    }
    func visitNeedToRepeat(visit: VisitDomainModel) {
        
    }
    func visitWillPayLater(visit: VisitDomainModel) {
        
    }
    
    func setupNavigation(nc: UINavigationController) {
        router.navigationController = nc
    }
    
    
}
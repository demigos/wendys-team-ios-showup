//
//  FeedbackVisitAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 01/08/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class FeedbackVisitAssembly
{
    class func createModule(configure: (module: FeedbackVisitModuleInput) -> Void) -> FeedbackVisitViewController
    {
        let vc = R.storyboard.rateAndFeedback.feedbackVisitViewController()!
        let interactor = FeedbackVisitInteractor()
        let presenter = FeedbackVisitPresenter()
        let router = FeedbackVisitRouter()

        interactor.output = presenter

        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
        router.presenter = presenter

        return vc
    }
}

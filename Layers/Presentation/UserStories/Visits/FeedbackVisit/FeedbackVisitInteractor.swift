//
//  FeedbackVisitInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 01/08/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol FeedbackVisitInteractorInput: class
{

}

//MARK: Output
protocol FeedbackVisitInteractorOutput: class
{

}

// MARK: - Interactor
final class FeedbackVisitInteractor: FeedbackVisitInteractorInput
{
    weak var output: FeedbackVisitInteractorOutput!
}

//
//  FeedbackVisitModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 01/08/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol FeedbackVisitModuleInput: class
{
    func setupDelegate(output: FeedbackVisitModuleOutput)
    func setInitialComment(comment: String)
}

//MARK: Output
protocol FeedbackVisitModuleOutput: class
{
    func userDidLeaveFeedback(feedback: String)
}


// MARK: - Presenter
final class FeedbackVisitPresenter: FeedbackVisitModuleInput
    , FeedbackVisitViewOutput
    , FeedbackVisitInteractorOutput
{
    var interactor: FeedbackVisitInteractorInput!
    weak var viewController: FeedbackVisitViewInput!
    weak var router: FeedbackVisitRouter!
    weak var output: FeedbackVisitModuleOutput?
    var initialComment = ""

    func setupDelegate(output: FeedbackVisitModuleOutput)
    {
        self.output = output
    }
    
    func setInitialComment(comment: String) {
        self.initialComment = comment
    }
    
    func viewDidLoad() {
        updateComment(initialComment)
    }
    
    func updateComment(comment: String) {
        viewController.updateComment(comment)
    }

    // MARK: - Interactor Output

    // MARK: - View Output
    func userDidLeaveFeedback(feedback: String)
    {
        output?.userDidLeaveFeedback(feedback)
    }

    func back() {
        router.popCurrentScreen()
    }

}

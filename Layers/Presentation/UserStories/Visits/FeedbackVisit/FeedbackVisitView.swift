//
//  FeedbackVisitView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 01/08/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol FeedbackVisitViewInput: class
{
    func updateComment(comment: String)
}

protocol FeedbackVisitViewOutput: class
{
    func userDidLeaveFeedback(feedback: String)
    func viewDidLoad()
    func back()
}

// MARK: - View Controller
final class FeedbackVisitViewController: BaseViewController
    , FeedbackVisitViewInput
    , Routable
    , UITextViewDelegate
{
    var output: FeedbackVisitViewOutput!
    typealias RouterType = FeedbackVisitRouter
    var router: RouterType!

    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: RoundedButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        textView.text = " "
        textViewDidChange(textView)
        textView.text = ""
        sendButton.enabled = false
        output.viewDidLoad()
    }
    @IBAction func backAction(sender: AnyObject) {
        output.back()
    }

    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }

    @IBAction func sendClicked(sender: AnyObject) {
        let commentText = textView.text
        output.userDidLeaveFeedback(commentText)
    }
    
    // MARK: View Input
    func textViewDidChange(textView: UITextView) {
        topConstraint.constant = textView.contentSize.height + 8
        if let text = textView.text {
            sendButton.enabled = text.characters.count > 0
        }
    }
    
    func updateComment(comment: String) {
        textView.text = comment
        textViewDidChange(textView)
    }

    //MARK: - Keyboard handling
    //
    override func keyboardWillShow(kb: KeyboardParameters) {
        let height = CGRectGetHeight(kb.frameEnd)

        UIView.animateWithDuration(kb.animationDuration, delay: 0, options: kb.animationCurve, animations: {
            self.bottomPadding.constant = height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }) {
            (_) in }
    }

    override func keyboardWillHide(kb: KeyboardParameters) {
        UIView.animateWithDuration(kb.animationDuration, delay: 0, options: kb.animationCurve, animations: {
            self.bottomPadding.constant = 0
        }) {
            (_) in
            //
        }
    }
}

//
//  FeedbackVisitRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 01/08/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class FeedbackVisitRouter: VIPERRouter
{
    weak var viewController: FeedbackVisitViewController!
    weak var presenter: FeedbackVisitPresenter!
}

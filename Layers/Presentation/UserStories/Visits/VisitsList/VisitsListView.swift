//
//  VisitsListView.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol VisitsListViewInput: class
{
    func updateWithData(upcomingVisits: [VisitDomainModel], pastVisits: [VisitDomainModel])
}

protocol VisitsListViewOutput: class
{
    func visitDidSelect(visit: VisitDomainModel)
    func framePaddingDidChange(to padding:CGFloat)
}

// MARK: - View Controller
final class VisitsListViewController: BaseViewController
    , VisitsListViewInput
    , Routable
    , UITableViewDataSource
    , UITableViewDelegate
{
    var output: VisitsListViewOutput!
    typealias RouterType = VisitsListRouter
    var router: RouterType!

    typealias VisitCellModel = (kind: VisitCellKind , items:[VisitDomainModel])
    var data : [VisitCellModel]?

    var rowHeight = CGFloat(80)
    var headerHeight = CGFloat(34)
    var phHeight = CGFloat(0)

    enum State : Int {
        case Collapsed = 0
        case Extended = 1
    }

    var state : State = .Collapsed {
        didSet
        {
            applyState()
        }
    }

    override func viewDidAppear(animated: Bool) {
        //must not call baseview didappear
    }

    private var tableHeaders = [VisitsSectionHeader]()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placeholderHeight: NSLayoutConstraint!


    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        phHeight = placeholderHeight.constant
    }

    func reloadVisits() {
        //        self.output.
    }

    override func viewDidLayoutSubviews() {
        applyState()
    }


    override func didMoveToParentViewController(parent: UIViewController?)
    {
        roundCorners()
        applyState()
    }


    // MARK: View Input
    func updateWithData(upcomingVisits: [VisitDomainModel], pastVisits: [VisitDomainModel])

    {
        var data = [VisitCellModel]()

        if upcomingVisits.count > 0 {
            data.append((kind: .Upcoming, items:upcomingVisits))
            tableView.backgroundColor = UIColor(red:0.11, green:0.57, blue:1.00, alpha:1.0)
        }

        if pastVisits.count > 0 {
            data.append((kind: .Past, items:pastVisits))
            tableView.backgroundColor = UIColor(red:0.16, green:0.38, blue:0.70, alpha:1.0)
        }


        self.data = data
        applyState()
        tableView.reloadData()
    }

    // MARK: Actions
    //
    @IBAction func panAction(sender: UIPanGestureRecognizer)
    {
        if hasData() && sender.translationInView(view).y < 0 && state == .Collapsed {
            state = .Extended
        }

        if hasData() && sender.translationInView(view).y > 0 && state == .Extended {
            state = .Collapsed
        }
    }

    @IBAction func swipeAction(sender: UISwipeGestureRecognizer)
    {
        if sender.direction == .Down && hasData() && state == .Extended {
            state = .Collapsed
        }

        if sender.direction == .Up && hasData() && state == .Collapsed {
            state = .Extended
        }
    }

    func applyState()
    {
        if state == .Collapsed {
            let offset = hasData() ? rowHeight - 1 + headerHeight : placeholderHeight.constant
            if hasData() { placeholderHeight.constant = 0 } else { placeholderHeight.constant = phHeight }
            moveTop(offset)
            tableView.setContentOffset(CGPoint(x: 0,y: 0), animated: false)
            tableView.scrollEnabled = false
        } else {
            if let pvcBounds = parentViewController?.view.bounds {
                let offset = CGRectGetHeight(pvcBounds)
                moveTop(offset)
                tableView.scrollEnabled = true
            }
        }

        for header in tableHeaders {
            header.changeState(state)
        }
    }

    func moveTop(offset: CGFloat)
    {


        if let pvcBounds = parentViewController?.view.bounds {
            output.framePaddingDidChange(to: offset)
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
                self.view.frame = CGRectMake(pvcBounds.origin.x,
                    pvcBounds.origin.y + CGRectGetHeight(pvcBounds) - offset,
                    CGRectGetWidth(pvcBounds),
                    CGRectGetHeight(pvcBounds))
                self.tableView.flashScrollIndicators()
                }, completion: { (_) in

            })
        }

    }

    func roundCorners()
    {
        let path = UIBezierPath(roundedRect: view.frame, byRoundingCorners: [.TopLeft, .TopRight], cornerRadii: CGSizeMake(16.0, 16.0)).CGPath
        let mask = CAShapeLayer()
        mask.frame = view.frame
        mask.path = path
        view.layer.mask = mask

    }
    //
    // MARK: Data Source
    //



    func hasData() -> Bool
    {
        return (data?.count > 0)
    }



    func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        if let data = data {
            return data.count

        }

        return 0

    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if let data = data where section < data.count {
            return data[section].items.count
        }

        return 0
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard let data = data where section < data.count else {
            return UITableViewCell()
        }

        let sectionData = data[section]

        let identifier = sectionData.kind.headerIdentifier

        let header = tableView.dequeueReusableCellWithIdentifier(identifier) as! VisitsSectionHeader
        header.fill(sectionData.items.count)

        if tableHeaders.indexOf(header) == nil {
            tableHeaders.append(header)
            header.changeState(state)
        }

        return header
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        guard let data = data where indexPath.section < data.count else {
            return UITableViewCell()
        }

        let sectionData = data[indexPath.section]

        let identifier = sectionData.kind.cellIdentifier

        let cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! VisitTableViewCell

        let row = indexPath.row
        if  row < sectionData.items.count {
            let visit = sectionData.items[row]
            cell.fill(withVisit: visit)
        }

        return cell
    }

    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let data = data  where data.count > 1 && section < data.count - 1 {
            return tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.visitsSectionFooter)


        }

        let v = UIView(frame: CGRectZero)
        v.backgroundColor = UIColor.clearColor()
        return v
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {

        if let data = data where data.count > 1 && section == 0 {
            return CGFloat(20)
        }

        return CGFloat.min
    }


    //MARK: TableView Delegate


    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let targetOffset = targetContentOffset.memory
        if targetOffset.y == 0 && scrollView.contentOffset.y == 0 {
            state = .Collapsed
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return rowHeight
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let section = indexPath.section
        let row = indexPath.row
        if let data = data where section < data.count && row < data[section].items.count {
            let model = data[section].items[row]
            output?.visitDidSelect(model)
        }
    }
}
//
//  VisitsListInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol VisitsListInteractorInput: class
{
    func refreshVisits()
}

//MARK: Output
protocol VisitsListInteractorOutput: class
{
    func visitsDidLoad(upcoming upcoming:[VisitDomainModel], past:[VisitDomainModel])
    func visitsDidFailedToLoad(error: [String]?)
}

// MARK: - Interactor
final class VisitsListInteractor: VisitsListInteractorInput
{
    weak var output: VisitsListInteractorOutput!
    private var visits : VisitsDSInterface

    init(visitsDS: VisitsDSInterface)
    {
        self.visits = visitsDS
    }

    func setup()
    {
        visits.visitsListUpdate.observeResult { result in
            switch result {
            case .Success(let visits):
                self.handleVisits(visits)
            case .Failure(let error):
                print("was error")
                self.output?.visitsDidFailedToLoad([error.errorDescription])
            }
        }
    }

    func handleVisits(visits:[VisitDomainModel])
    {
        var upcomingVisits = [VisitDomainModel]()
        var pastVisits = [VisitDomainModel]()

        for visit in visits
        {
            if visit.isUpcoming {
                upcomingVisits.append(visit)
            }

            if visit.isPast {
                pastVisits.append(visit)
            }
        }

        output?.visitsDidLoad(upcoming: upcomingVisits, past: pastVisits)

    }


    func refreshVisits()
    {
        setup()
        visits.loadVisits()
    }
}
//
//  VisitsListRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class VisitsListRouter: VIPERRouter
{
    weak var view: VisitsListViewController!
    weak var presenter: VisitsListPresenter!
    var resources: Dependencies!

    
    func presentVisit(visit: VisitDomainModel)
    {
        VisitDetailAssembly.createModule(resources) { (module) in
            module.setupDelegate(self.presenter)
            module.setupNavigation(self.navigationController!)
            module.setupVisit(visit)
        }.present(inNavigation: navigationController!)
    }
}
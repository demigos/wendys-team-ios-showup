//
//  VisitsListAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class VisitsListAssembly
{
    class func createModule(resources: Dependencies, configure: (module: VisitsListModuleInput) -> Void) -> VisitsListViewController
    {
        let vc = R.storyboard.visits.visitsListViewController()!
        let interactor = VisitsListInteractor(visitsDS: resources.datasources.visits)
        let presenter = VisitsListPresenter()
        let router = VisitsListRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc
        router.resources = resources
        router.presenter = presenter

        return vc
    }
}
//
//  VisitTableViewCell.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 14.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class VisitTableViewCell: UITableViewCell {


    class func identifier(forKind kind: VisitCellKind) -> String {
        return kind.rawValue
    }


    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var servicesImage: MultiServiceImage!


    func clear()
    {
        dateLabel.text = ""
        timeLabel.text = ""
        customerLabel.text = ""
        servicesImage.hidden = true
        servicesLabel.text = ""
    }

    func fill(withVisit visit: VisitDomainModel)
    {
        clear()
        let dateFormatter = NSDateFormatter()


        if let date = visit.date {
            dateFormatter.dateFormat = "MM.dd"
            dateLabel?.text = dateFormatter.stringFromDate(date)

            dateFormatter.dateFormat = "hh:mm  aa"
            timeLabel?.text = dateFormatter.stringFromDate(date)

        } else {
            dateLabel?.text = ""
            timeLabel.text = ""
        }

        if let name = visit.customerName {
            customerLabel.text = name
        } else {
            customerLabel.text = ""
        }

        let services = visit.services
        if services.count > 0 {
            servicesImage.count = UInt(services.count)
            servicesImage.service = services.first!
            servicesImage.hidden = false
            var servicesText = ""
            for (i,s) in services.enumerate() {
                servicesText += s.description
                if i < services.count - 1 {
                    servicesText += ", "
                }
            }
            servicesLabel.text = servicesText
        }



        setNeedsDisplay()

    }




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

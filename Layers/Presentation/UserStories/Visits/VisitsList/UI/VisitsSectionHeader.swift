//
//  VisitsSectionHeader.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 14.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class VisitsSectionHeader: UITableViewCell
{
    let height = CGFloat(24)
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var upcomingArrow: UIImageView!
    @IBOutlet weak var pastArrow: UIImageView!
    
    class func identifier(forKind kind: VisitCellKind) -> String
    {
        return kind.rawValue
    }

    func fill(count: Int)
    {
        countLabel.text = String(count)
    }
    
    func changeState(state: VisitsListViewController.State)
    {
        switch state {
        case .Collapsed:
            upcomingArrow?.image = R.image.arrow()
            pastArrow?.image = R.image.arrow()
        case .Extended:
            upcomingArrow?.image = R.image.arrowReversed()
            pastArrow?.image = R.image.arrowReversed()
        }
    }
    
}

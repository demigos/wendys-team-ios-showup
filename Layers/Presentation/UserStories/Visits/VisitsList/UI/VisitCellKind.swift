//
//  VisitCellKind.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 12.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

enum VisitCellKind: String {
    case Upcoming
    case Past = "VisitPastHeader"
    
    var headerIdentifier : String {
        switch self {
        case .Upcoming:
            return "VisitUpcomingHeader"
        case .Past:
            return "VisitPastHeader"
        }
    }

    var cellIdentifier : String {
        switch self {
        case .Upcoming:
            return "VisitUpcomingCell"
        case .Past:
            return "VisitPastCell"
        }
    }
}
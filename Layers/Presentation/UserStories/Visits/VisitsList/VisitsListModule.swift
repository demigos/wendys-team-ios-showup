//
//  VisitsListModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol VisitsListModuleInput
{
    func setupDelegate(output: VisitsListModuleOutput)
    func setupNavigation(nc: UINavigationController)
    func refreshList()
}

//MARK: Output
protocol VisitsListModuleOutput: class
{
    func visitsLoadedWithSuccess(success : Bool)
    func visitNeedToRepeat(visit: VisitDomainModel)
    func visitNeedToEdit(visit: VisitDomainModel)
    func visitWillPayLater(visit: VisitDomainModel)
    func visitFramePaddingDidChange(to padding:CGFloat)
}

// MARK: UpdateVisitFlow
struct EditVisitFlow: BookVisitFlow
{
    var initialStep = BookVisitFlowStep.DurationAndTime

    func isFirstFlow() -> Bool {
        return false
    }
    
    func next(step: BookVisitFlowStep) -> BookVisitFlowStep
    {
        switch step {

        case .PersonSelect: return .ContactInfo
        case .ContactInfo: return .Services
        case .Services: return .DurationAndTime
        case .DurationAndTime: return .Summary
        case .Success
        , .SummaryContactInfo
        , .SummaryDurationAndTime
        , .SummaryServices
            : return .Summary
        case .Summary:
            return .Success
        default:
            return .Summary
        }

    }

    func validStepFor(step: BookVisitFlowStep) -> BookVisitFlowStep
    {
        switch step {
        // Prohibited steps
        case .PersonSelect, .Services, .SummaryServices: return .Summary
        default: return step
        }
    }

}

// MARK: - Presenter
final class VisitsListPresenter: VisitsListModuleInput
    , VisitsListViewOutput
    , VisitsListInteractorOutput
    , VisitDetailModuleOutput
{
    weak var view: VisitsListViewInput!
    var interactor: VisitsListInteractorInput!
    weak var router: VisitsListRouter!
    weak var output: VisitsListModuleOutput?
    var visitsLoadFailed = true

    init() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(refreshFailedList),
                                                         name: NetworkService.networkReachable,
                                                         object: nil)
    }

    func setupDelegate(output: VisitsListModuleOutput)
    {
        self.output = output
    }

    func setupNavigation(nc: UINavigationController)
    {
        router.navigationController = nc
    }

    func refreshList()
    {
//        visitsLoadFailed = false
        DispatchToBackground {
            self.interactor.refreshVisits()
        }
    }

    @objc func refreshFailedList() {
        if (visitsLoadFailed) {
            refreshList()
        }
    }


    // MARK: - Interactor Output
    func visitsDidLoad(upcoming upcoming: [VisitDomainModel], past: [VisitDomainModel])
    {
        visitsLoadFailed = false
        self.output?.visitsLoadedWithSuccess(true)
        view.updateWithData(upcoming, pastVisits: past)
    }

    func visitsDidFailedToLoad(error: [String]?) {
        self.output?.visitsLoadedWithSuccess(false)
        visitsLoadFailed = true
    }

    // MARK: - View Output
    func framePaddingDidChange(to padding: CGFloat)
    {
        output?.visitFramePaddingDidChange(to: padding)
    }

    func visitDidSelect(visit: VisitDomainModel)
    {
        router.presentVisit(visit)
    }

    // MARK: VisitDetailModuleOutput
    func visitWillPayLater(visit: VisitDomainModel) {
        DispatchToMainQueue {
            self.output?.visitWillPayLater(visit)
        }
    }

    func visitDidCanceled(visit: VisitDomainModel)
    {
        refreshList()
    }

    func visitNeedToRepeat(visit: VisitDomainModel)
    {
        output?.visitNeedToRepeat(visit)
    }

    func visitNeedToEdit(visit: VisitDomainModel)
    {
        output?.visitNeedToEdit(visit)
    }

}
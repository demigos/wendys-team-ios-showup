//
// Created by Maksim Bazarov on 17.04.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import UIKit

protocol VisitServiceCellDelegate {
    func didPressedMinus(inCellIndexPath: NSIndexPath)
}

class VisitServiceCell: UITableViewCell {
    class func identifier() -> String {
        return "VisitServiceCell"
    }

    @IBOutlet weak var serviceView: MultiServiceImage!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var minusShadowImage: UIImageView!
    var editableMode = false
    var indexPath: NSIndexPath?
    var delegate: VisitServiceCellDelegate?

    func clear() {
        serviceView.hidden = true
        serviceName.text = ""
    }

    @IBAction func minusPresed() {
        if let indexPath = self.indexPath {
            self.delegate?.didPressedMinus(indexPath)
        }
    }

    func fill(withService service: CareServiceDomainModel, fillColor: UIColor, color: UIColor, editableMode: Bool, indexPath: NSIndexPath) {
        serviceName.text = service.description
        serviceView.service = service

        serviceView.count = 1
        serviceView.borderWidth = 1
        serviceView.fillColor = fillColor
        serviceView.mainColor = color
        serviceView.hidden = false

        self.editableMode = editableMode
        self.setMinusVisible(self.editableMode)

        contentView.backgroundColor = fillColor
        backgroundColor = fillColor
        self.indexPath = indexPath
        self.needsUpdateConstraints()
    }

    func setMinusVisible(visible: Bool) {
        self.minusButton.hidden = !visible
        self.minusShadowImage.hidden = !visible
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame.origin.x = 0
    }
}

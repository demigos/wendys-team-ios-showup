//
//  VisitDetailAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class VisitDetailAssembly
{
    class func createModule(resources: Dependencies, configure: (module: VisitDetailModuleInput) -> Void) -> VisitDetailViewController
    {
        let vc = R.storyboard.visits.visitDetailViewController()!
        let interactor = VisitDetailInteractor(visitsDS: resources.datasources.visits)
        let presenter = VisitDetailPresenter()
        let router = VisitDetailRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc
        router.presenter = presenter
        router.resources = resources

        return vc
    }
}
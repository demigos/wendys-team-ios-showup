//
//  VisitDetailView.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit


// MARK: - Interface

protocol VisitDetailViewInput: BaseViewControllerInput {
    func setupInitialState()

    func update(withVisit visit: VisitDomainModel)

    func update(withError error: String, visit: VisitDomainModel)

    func visitShouldBeCancelled(visit: VisitDomainModel, message: String)
}

protocol VisitDetailViewOutput: class {
    func didLoad()

    func visitsServicesDidUpdate(updatedVisit visit: VisitDomainModel)

    func backButtonDidSelect(forVisit visit: VisitDomainModel)

    func cancelButtonDidSelect(forVisit visit: VisitDomainModel)

    func repeatButtonDidSelect(forVisit visit: VisitDomainModel)

    func editButtonDidSelect(forVisit visit: VisitDomainModel)

    func cancellationDialogPayNowButtonDidSelect(visit: VisitDomainModel)

    func cancellationDialogPayLaterButtonDidSelect(visit: VisitDomainModel)
}

// MARK: - View Controller

final class VisitDetailViewController: BaseViewController
        , VisitDetailViewInput
        , Routable
        , UITableViewDelegate
        , UITableViewDataSource
        , VisitServiceCellDelegate {
    var output: VisitDetailViewOutput!
    typealias RouterType = VisitDetailRouter
    var router: RouterType!

    @IBOutlet weak var tableView: UITableView!
    var inEditMode = false


    @IBOutlet weak var servicesCountLabel: UILabel!
    @IBOutlet weak var managePanel: UIView!
    @IBOutlet weak var manageButton: UIButton!

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var customerAvatarImageView: UIImageView!
    @IBOutlet weak var visitDateLabel: UILabel!
    @IBOutlet weak var visitTimeLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var repeatButtonLeading: NSLayoutConstraint!
    @IBOutlet weak var editButton: UIBarButtonItem!

    @IBOutlet weak var repeatButton: UIButton!

    enum VisitKind: Int {
        case Upcoming = 0
        case Past = 1

        var bgColor: UIColor {
            switch self {
            case Upcoming: return UIColor(red: 0.11, green: 0.57, blue: 1.00, alpha: 1.0)
            case Past: return UIColor(red: 0.16, green: 0.38, blue: 0.70, alpha: 1.0)
            }
        }

        var fgColor: UIColor {
            switch self {
            case Upcoming: return UIColor(red: 0.32, green: 0.67, blue: 1.00, alpha: 1.0)
            case Past: return UIColor(red: 0.36, green: 0.53, blue: 0.77, alpha: 1.0)
            }
        }

        var separatorColor: UIColor {
            switch self {
            case Upcoming: return UIColor(red: 0.61, green: 0.81, blue: 1.00, alpha: 1.0)
            case Past: return UIColor(red: 0.63, green: 0.73, blue: 0.87, alpha: 1.0)
            }
        }

        var title: String {
            switch self {
            case Upcoming: return "UPCOMING VISIT"
            case Past: return "PAST VISIT"
            }
        }

    }

    var visit = VisitDomainModel()
    var visitKind: VisitKind = .Upcoming {
        didSet {
            updateForKind()
        }
    }

    var visitServices = [CareServiceDomainModel]()
    var cancelButtonRegularWidth = CGFloat(0)
    var confirmController: UIAlertController!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.didLoad()

        cancelButtonRegularWidth = CGFloat(111) //cancelButtonWidth.constant
        setupInitialState()
    }

    override func viewWillDisappear(animated: Bool) {
        tableView.setEditing(false, animated: false)
        self.inEditMode = false
        super.viewWillDisappear(animated)
    }

    // MARK: View Input
    func setupInitialState() {
        confirmController = UIAlertController(title: "Wendys Team", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        confirmController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: payNowAction))
        confirmController.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: cancelAction))
    }

    func update(withVisit visit: VisitDomainModel) {
        self.visit = visit
        clear()
        if visit.isUpcoming {
            self.visitKind = .Upcoming
        } else {
            self.visitKind = .Past
        }
        fill(withVisit: visit)
    }

    func visitShouldBeCancelled(visit: VisitDomainModel, message: String) {
        showPayConfirmaton(message)
    }

    func update(withError error: String, visit: VisitDomainModel) {
        showError(error)
        update(withVisit: visit)
    }

    //MARK: - Actions
    //
    @IBAction func backButtonAction(sender: AnyObject) {
        output.backButtonDidSelect(forVisit: visit)
    }

    @IBAction func cancelButtonAction(sender: AnyObject) {
        if let visitDate = visit.date where minutesDiff(NSDate(), visitDate) > 60 {
            let confirmAlert = UIAlertController(title: "Wendys Team", message: "Are you sure to cancel the visit?", preferredStyle: UIAlertControllerStyle.Alert)
            confirmAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: forceCancel))
            confirmAlert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: cancelAction))
            presentViewController(confirmAlert, animated: true, completion: nil)
        } else {
            output.cancelButtonDidSelect(forVisit: visit)
        }
    }

    private func minutesDiff(date1: NSDate, _ date2: NSDate) -> Int {
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date1, toDate: date2, options: []).minute
    }

    func forceCancel(sender: AnyObject) {
        output.cancelButtonDidSelect(forVisit: visit)
    }

    @IBAction func repeatButtonAction(sender: AnyObject) {
        output.repeatButtonDidSelect(forVisit: visit)
    }


    @IBAction func manageButtonAction(sender: AnyObject) {
        if let tableView = tableView {
            self.inEditMode = !self.inEditMode
//            tableView.editing = tableView.editing ? false : true
            updateButtonTitle()
            tableView.reloadData()
        }
    }

    @IBAction func editButtonAction(sender: AnyObject) {
        output.editButtonDidSelect(forVisit: visit)
    }


    //
    func updateButtonTitle() {
        if self.inEditMode {
            manageButton.setTitle("Done", forState: .Normal)
        } else {
            manageButton.setTitle("Manage", forState: .Normal)
        }
    }

    func clear() {
        visitDateLabel.text = ""
        visitTimeLabel.text = ""
        customerNameLabel.text = ""
        customerAvatarImageView.hidden = true
    }

    func fill(withVisit visit: VisitDomainModel) {
        clear()

        servicesCountLabel.text = "\(visit.services.count)"

        if visit.services.count > 0 {
            visitServices = visit.services
            tableView.reloadData()
        }

        let dateFormatter = NSDateFormatter()
        if let date = visit.date {
            dateFormatter.dateFormat = "MM.dd"
            visitDateLabel?.text = dateFormatter.stringFromDate(date)

            dateFormatter.dateFormat = "hh:mm aa"
            visitTimeLabel?.text = dateFormatter.stringFromDate(date)

        } else {
            visitDateLabel?.text = ""
            visitTimeLabel.text = ""
        }

        if let name = visit.customerName {
            customerNameLabel.text = name
        } else {
            customerNameLabel.text = ""
        }

        if let image = visit.customerAvatar {
            customerAvatarImageView.setupImage(fromURL: image)
            customerAvatarImageView.hidden = false
        } else {
            if var image = UIImage(named: "avatar") {
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                customerAvatarImageView.tintColor = UIColor.whiteColor().colorWithAlphaComponent(0.4)
                customerAvatarImageView.image = image
                customerAvatarImageView.hidden = false
            }
        }
    }


    func updateForKind() {
        view.backgroundColor = visitKind.bgColor
        tableView.backgroundColor = visitKind.bgColor
        tableView.separatorColor = visitKind.separatorColor

        managePanel.backgroundColor = visitKind.fgColor

        navigationBar.topItem?.title = visitKind.title
        if visitKind == .Upcoming {
            showCancelButton()
            editButton?.enabled = true
            manageButton.hidden = false
            var buttonsArray = navigationBar.items![0].rightBarButtonItems
            buttonsArray?.removeAll()
            navigationBar.items![0].rightBarButtonItems = buttonsArray

        } else {
            hideCancelButton()
            var buttonsArray = navigationBar.items![0].rightBarButtonItems
            buttonsArray?.removeAll()
            navigationBar.items![0].rightBarButtonItems = buttonsArray
            manageButton.hidden = true
        }
        self.view.needsUpdateConstraints()
    }

    func hideCancelButton() {
        cancelButton.hidden = true
    }

    func showCancelButton() {
        cancelButton.hidden = false
    }

    func showDeleteButton() {
        cancelButton.hidden = false
        cancelButton.titleLabel?.text = "Delete"
    }

    func showPayConfirmaton(message: String) {
        confirmController.message = message

        presentViewController(confirmController, animated: true, completion: nil)
    }

    func payNowAction(sender: UIAlertAction) {
        output.cancellationDialogPayNowButtonDidSelect(visit)
    }

    func payLaterAction(sender: UIAlertAction) {
        output.cancellationDialogPayLaterButtonDidSelect(visit)
    }

    func cancelAction(sender: UIAlertAction) {
        //
    }

    // MARK: - TABLE VIEW
    func tryToDeleteCell(indexPath: NSIndexPath) {

        if cellModelForIndexpath(indexPath) != nil {
            visitServices.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
            visit.services = visitServices
            output?.visitsServicesDidUpdate(updatedVisit: visit)
        }
    }


    func cellModelForIndexpath(indexPath: NSIndexPath) -> CareServiceDomainModel? {
        let row = indexPath.row
        if row < visitServices.count {
            let model = visitServices[row]
            return model
        }

        return nil
    }

    //MARK: Datasource

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return visitServices.count

    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier(VisitServiceCell.identifier()) as! VisitServiceCell
        cell.clear()

        if let cellModel = cellModelForIndexpath(indexPath) {
            cell.fill(withService: cellModel, fillColor: visitKind.bgColor, color: UIColor.whiteColor(), editableMode: self.inEditMode, indexPath: indexPath)
            cell.delegate = self
        }

        return cell
    }

    func didPressedMinus(inCellIndex: NSIndexPath) {
        tryToDeleteCell(inCellIndex)
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGFloat.min)
        let view = UIView(frame: frame)
        view.clipsToBounds = true
        return view
    }


    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGFloat.min)
        let view = UIView(frame: frame)
        view.clipsToBounds = true
        return view
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }


    //MARK: Delegate

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if visitKind == .Upcoming {
            return true
        } else {
            return false
        }
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            tryToDeleteCell(indexPath)
        }
    }

    func tableView(tableView: UITableView, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

}
//
//  VisitDetailRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class VisitDetailRouter: VIPERRouter
{
    weak var presenter: VisitDetailPresenter!
    weak var view: VisitDetailViewController!
    weak var resources: Dependencies!
    
    func moveBack()
    {
        if let nc = navigationController {
            popCurrentScreen()
        }
        else {
            self.view.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}
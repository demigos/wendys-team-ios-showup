//
//  VisitDetailInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol VisitDetailInteractorInput: class
{
    func loadVisit(visit: VisitDomainModel)
    func updateServices(forVisit visit: VisitDomainModel)
    func requestVisitCancellation(visit: VisitDomainModel)
    func cancelVisit(visit: VisitDomainModel)
    func payLaterForVisit(visit: VisitDomainModel)
}

//MARK: Output
protocol VisitDetailInteractorOutput: class
{
    func visitUpdated(visit: VisitDomainModel)
    func visitUpdateDidFail(visit: VisitDomainModel, error: WTError)
    
    func visitCanBeCancelled(visit: VisitDomainModel, withResult: CancellationResultDomainModel)
    func visitCancellationRequestDidFail(visit: VisitDomainModel, error: WTError)
    
    func visitDidCancelled(visit: VisitDomainModel, withResult: CancellationResultDomainModel)
    func visitCancellationDidFail(visit: VisitDomainModel, error: WTError)
}

// MARK: - Interactor
final class VisitDetailInteractor: VisitDetailInteractorInput
{
    weak var output: VisitDetailInteractorOutput!
    var visits: VisitsDSInterface

    
    init(visitsDS: VisitsDSInterface)
    {
        self.visits = visitsDS
    }
    
    func loadVisit(visit: VisitDomainModel)
    {
        visits.loadVisit(UInt(visit.id)) { result in
            switch result {
            case .Success(let updated_visit): self.output.visitUpdated(updated_visit)
            case .Failure(_): break
            }
        }
    }
    
    
    func requestVisitCancellation(visit: VisitDomainModel)
    {
        visits.cancelVisit(visit, charge: nil, result: {
            (result) in switch result {
            case .Success(let cancellationResult):
                if cancellationResult.canceled {
                    self.output.visitDidCancelled(visit, withResult: cancellationResult)
                } else {
                    self.output.visitCanBeCancelled(visit, withResult: cancellationResult)
                }
                
            case .Failure(let error): self.output.visitCancellationDidFail(visit, error: error)
            }
        })
    }
    
    func cancelVisit(visit: VisitDomainModel)
    {
        visits.cancelVisit(visit, charge: true, result: {
            (result) in switch result {
            case .Success(let cancellationResult): self.output.visitDidCancelled(visit, withResult: cancellationResult)
            case .Failure(let error): self.output.visitCancellationDidFail(visit, error: error)
            }
        })
    }
    
    func updateServices(forVisit visit: VisitDomainModel)
    {
        visits.updateVisit(visit) { result in
            switch result {
            case .Success(let updated_visit): self.output.visitUpdated(updated_visit)
            case .Failure(let error): self.output.visitUpdateDidFail(visit, error: error)
            }
        }
    }
    
    func payLaterForVisit(visit: VisitDomainModel)
    {
        visits.cancelVisit(visit, charge: false, result: {
            (result) in switch result {
            case .Success(let cancellationResult): self.output.visitDidCancelled(visit, withResult: cancellationResult)
            case .Failure(let error): self.output.visitCancellationDidFail(visit, error: error)
            }
        })
    }

}
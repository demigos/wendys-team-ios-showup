//
//  VisitDetailModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol VisitDetailModuleInput
{
    func setupDelegate(output: VisitDetailModuleOutput)
    func setupNavigation(nc: UINavigationController)
    func setupVisit(visit: VisitDomainModel)
}

//MARK: Output
protocol VisitDetailModuleOutput: class
{
    func visitDidCanceled(visit: VisitDomainModel)
    func visitNeedToEdit(visit: VisitDomainModel)
    func visitNeedToRepeat(visit: VisitDomainModel)
    func visitWillPayLater(visit: VisitDomainModel)
}


// MARK: - Presenter
final class VisitDetailPresenter: VisitDetailModuleInput
    , VisitDetailViewOutput
    , VisitDetailInteractorOutput
{
    weak var view: VisitDetailViewInput!
    var interactor: VisitDetailInteractorInput!
    weak var router: VisitDetailRouter!
    weak var output: VisitDetailModuleOutput?

    var visit: VisitDomainModel!

    func setupDelegate(output: VisitDetailModuleOutput)
    {
        self.output = output
    }

    func setupNavigation(nc: UINavigationController)
    {
        router.navigationController = nc
    }

    func setupVisit(visit: VisitDomainModel)
    {
        self.visit = visit
    }


    // MARK: - Interactor Output
    func visitUpdated(visit: VisitDomainModel) {
        DispatchToMainQueue {
            self.view.update(withVisit: visit)
        }

    }

    func visitUpdateDidFail(visit: VisitDomainModel, error: WTError) {
        interactor.loadVisit(visit)
        DispatchToMainQueue{
            self.view.update(withError: error.errorDescription, visit: visit)
        }

    }

    func visitCanBeCancelled(visit: VisitDomainModel, withResult: CancellationResultDomainModel)
    {
        self.view.waitMode(false)
        if let message = withResult.message, let amount = withResult.amount {
            DispatchToMainQueue{
                self.view.visitShouldBeCancelled(visit, message: "Sorry, Team Member is on the way to you. \nWe will charge $\(amount) for the visit cancellation")
            }
        }
    }

    func visitCancellationRequestDidFail(visit: VisitDomainModel, error: WTError)
    {
        DispatchToMainQueue{
            self.view.update(withError: error.errorDescription, visit: visit)
        }
    }

    func visitDidCancelled(visit: VisitDomainModel, withResult: CancellationResultDomainModel)
    {
        self.view.waitMode(false)
        DispatchToMainQueue{
            self.router.moveBack()
            self.output?.visitDidCanceled(visit)
        }
    }

    func visitCancellationDidFail(visit: VisitDomainModel, error: WTError)
    {
        view.waitMode(false)
        DispatchToMainQueue{
            self.view.update(withError: error.errorDescription, visit: visit)
        }
    }

    // MARK: - View Output
    func didLoad()
    {
        DispatchToMainQueue {
            self.view.update(withVisit: self.visit!)
        }

    }

    func visitsServicesDidUpdate(updatedVisit visit: VisitDomainModel)
    {
        DispatchToBackground {
            self.interactor.updateServices(forVisit: visit)
        }
    }

    func backButtonDidSelect(forVisit visit: VisitDomainModel)
    {
        router.moveBack()
    }

    func cancelButtonDidSelect(forVisit visit: VisitDomainModel)
    {
        view.waitMode(true)
        DispatchToBackground
            {
                self.interactor.requestVisitCancellation(visit)
        }
    }

    func repeatButtonDidSelect(forVisit visit: VisitDomainModel)
    {
        DispatchToMainQueue{
            self.router.moveBack()
            self.output?.visitNeedToRepeat(visit)
        }
    }

    func cancellationDialogPayNowButtonDidSelect(visit: VisitDomainModel)
    {
        view.waitMode(true)
        DispatchToBackground{
            self.interactor.cancelVisit(visit)
        }
    }

    func cancellationDialogPayLaterButtonDidSelect(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.output?.visitWillPayLater(visit)
        }
        view.waitMode(true)
        DispatchToBackground{
            self.interactor.payLaterForVisit(visit)
        }
    }

    func editButtonDidSelect(forVisit visit: VisitDomainModel)
    {
        output?.visitNeedToEdit(visit)
    }
}
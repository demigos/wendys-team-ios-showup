//
//  PaidVisitInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol PaidVisitInteractorInput: class
{
    func loadVisit(visitId : Int)
    func sendFeedback(visit: VisitDomainModel)
    func payForVisit(visit: VisitDomainModel)
}

//MARK: Output
protocol PaidVisitInteractorOutput: class
{
    func visitUpdated(visit: VisitDomainModel)
    func feedbackSent(id : Int)
    func paymentSent(id : Int)
    func showError(error: WTError)
}

// MARK: - Interactor
final class PaidVisitInteractor: PaidVisitInteractorInput
{
    
    private var visits : VisitsDSInterface
    
    init(visitsDS: VisitsDSInterface)
    {
        self.visits = visitsDS
    }
    
    weak var output: PaidVisitInteractorOutput!
    
    func loadVisit(visitId: Int)
    {
        visits.loadVisit(UInt(visitId)) { result in
            switch result {
            case .Success(let updated_visit): self.output.visitUpdated(updated_visit)
            case .Failure(_): break
            }
        }
    }
    
    func payForVisit(visit: VisitDomainModel) {
        visits.payForVisit(visit) {
            result in
            switch result {
            case .Success(_): self.output.paymentSent(visit.id)
            case .Failure(let error): self.output.showError(error)
            }
        }
    }
    
    func sendFeedback(visit: VisitDomainModel) {
        visits.feedbackForVisit(visit) {
            result in
            switch result {
            case .Success(_): self.output.feedbackSent(visit.id)
            case .Failure(let error): self.output.showError(error)
            }
        }
    }
}

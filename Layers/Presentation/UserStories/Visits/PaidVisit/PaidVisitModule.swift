//
//  PaidVisitModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface

protocol PaidVisitModuleInput: class {
    func setupDelegate(output: PaidVisitModuleOutput)
}

//MARK: Output

protocol PaidVisitModuleOutput: class {
    func visitUpdated(visit: VisitDomainModel)
    
    func cancel()
}


// MARK: - Presenter

final class PaidVisitPresenter: PaidVisitModuleInput
        , PaidVisitViewOutput
        , PaidVisitInteractorOutput
        , FeedbackVisitModuleOutput {
    var interactor: PaidVisitInteractorInput!
    weak var viewController: PaidVisitViewInput!
    weak var router: PaidVisitRouter!
    weak var output: PaidVisitModuleOutput?
    var visitId: Int?
    var visit: VisitDomainModel?

    func setupDelegate(output: PaidVisitModuleOutput) {
        self.output = output
    }

    func loadVisitByID(visitID: Int) {
        viewController.waitMode(true)
        interactor.loadVisit(visitID)
    }

    func visitUpdated(visit: VisitDomainModel) {
        viewController.waitMode(false)
        self.visitId = visit.id
        self.visit = visit
        DispatchToMainQueue {
            self.viewController.update(withVisit: visit)
        }
    }


    // MARK: - Interactor Output

    // MARK: - View Output
    func userWantsToLeaveFeedback() {
        self.router.presentFeedbackDialog(visit?.feedback ?? "")
    }

    func userDidUpdateRating(rating: Int) {
        visit?.rating = rating
    }

    func cancel() {
        output?.cancel()
    }

    // MARK: Feedback dialog
    func userDidLeaveFeedback(feedback: String) {
        visit?.feedback = feedback
        self.router.dismissTopController()
    }

    func sendFeedback() {
        if (visit?.rating ?? 0) == 0 {
            viewController.showError("Please make your choice")
            return
        }
        guard let visit = visit else {
            return
        }
        viewController.waitMode(true)
        interactor.sendFeedback(visit)
    }
    
    func pay() {
        if ((visit?.rating ?? 0) == 0) && (visit?.status != .Cancelled) {
            viewController.showError("Please make your choice")
            return
        }
        guard let visit = visit else {
            return
        }
        viewController.waitMode(true)
        interactor.payForVisit(visit)
    }

    func feedbackSent(id : Int) {
        viewController.waitMode(false)
        self.router.setLastFeedbackVisit(id)
        self.router.presentSuccessDialog()
    }
    
    func paymentSent(id : Int) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber - 1
        viewController.waitMode(false)
        self.router.removeUnpaidVisit(id)
        self.router.presentSuccessDialog()
    }

    func showError(error: WTError) {
        viewController.waitMode(false)
        self.viewController.showError(error.errorDescription)
    }

    func successDoneButtonDidSelect() {
        router.visitDidPaid(visit?.id)
    }

    func feedbackSendSuccess() {
        router.feedbackSent()
    }
    
    func goToFeedback() {
        self.router.goToFeedbackFromVisit(visit?.id)
    }
    
    func doItLater() {
        router.dismissDialog()
    }


}

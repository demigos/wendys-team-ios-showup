//
//  PaidVisitView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol PaidVisitViewInput: BaseViewControllerInput {
    func update(withVisit visit: VisitDomainModel)
    
    func showError(error: String)
    
    func dismiss()
}

protocol PaidVisitDelegate: class {
    func visitPaid(visitId: Int)
}

protocol PaidVisitViewOutput: class {
    func cancel()
    
    func userDidUpdateRating(rating: Int)
    
    func userWantsToLeaveFeedback()
    
    func sendFeedback()
    
    func loadVisitByID(visitID: Int)
    
    func successDoneButtonDidSelect()
    
    func feedbackSendSuccess()
    
    func pay()
    
    func doItLater()
    
    func goToFeedback()
}

// MARK: - View Controller

final class PaidVisitViewController: BaseViewController
    , PaidVisitViewInput
, Routable {
    var output: PaidVisitViewOutput!
    typealias RouterType = PaidVisitRouter
    var router: RouterType!
    var visitId: Int?
    var success: SuccessFeedbackViewController?
    var unpaid = false
    var failureMessage : String?
    var nofeedback = false
    weak var delegate: PaidVisitDelegate?
    var singleUnpaidVisit = false
    // MARK: Interface builder
    
    @IBOutlet weak var doItLaterButton: RoundedButton!
    @IBOutlet var ratingButtons: [UIButton]!
    @IBOutlet weak var payButton: PayButton!
    @IBOutlet weak var tmAvatarImageView: RoundedImage!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var teamMemberLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var unpaidView: UIView!
    @IBOutlet weak var payForAllButton: PayButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var payLaterButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var visitMessageLabel: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentButton: UIButton!

    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }
    
    func visitPaid() {
        if let id = visitId {
            self.delegate?.visitPaid(id)
        }
    }
    
    let randomColor = AvatarRandomColor()
    
    func hideBackButton() {
        backButton.enabled = false
        backButton.image = nil
    }
    
    func showbackButton() {
        backButton.enabled = true
        backButton.image = UIImage.init(named: "back")
    }
    
    
    @IBAction func backButtonSelected(sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(false)
        } else {
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
    @IBAction func ratingButtonAction(sender: AnyObject) {
        if let rating = sender.tag where rating <= ratingButtons.count {
            updateRating(rating)
            output.userDidUpdateRating(rating)
        }
    }
    
    @IBAction func payLaterButtonClicked(sender: AnyObject) {
        output.goToFeedback()
    }
    
    @IBAction func sendButtonClicked(sender: AnyObject) {
        output?.sendFeedback()
    }
    
    @IBAction func payNowButtonClicked(sender: AnyObject) {
        output?.pay()
    }
    
    @IBAction func leaveCommentAction(sender: AnyObject) {
        output.userWantsToLeaveFeedback()
    }
    
    @IBAction func leaveCommentAction2(sender: AnyObject) {
        output.userWantsToLeaveFeedback()
    }
    
    @IBAction func doItLaterClicked(sender: AnyObject) {
        output?.doItLater()
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if unpaid {
            self.bottomConstraint.constant = 50
            self.unpaidView.hidden = false
            if singleUnpaidVisit {
                self.payLaterButton.hidden = false
                self.bottomConstraint.constant = 128
            }

            ratingView.hidden = true
            ratingHeightConstraint.constant = 0
            self.commentButton.hidden = true

        }
        infoView.hidden = true
        self.payButton.layer.cornerRadius = self.payButton.layer.frame.size.height / 2
        if let id = visitId {
            self.output.loadVisitByID(id)
        }
        self.hideBackButton()
    }
    
    func update(withVisit visit: VisitDomainModel) {
        infoView.hidden = false
        if let failureMessage = visit.failureMessage {
            if failureMessage.characters.count > 0 {
                self.visitMessageLabel.text = failureMessage
                self.visitMessageLabel.textColor = UIColor.redColor()
                
                //            self.updateViewConstraints()
            }
        }
        if (visit.status != .Cancelled || !unpaid) {
            ratingView.hidden = false
            ratingHeightConstraint.constant = 64
            self.commentButton.hidden = false
        }
        if unpaid {
            self.visitMessageLabel.text = "Thank you for your visit"
            self.navigationBar.topItem?.title = "UNPAID VISIT"
            if let cost = visit.cost {
                payForAllButton.costLabel.text = "$\(cost)"
            }
            if let duration = visit.duration {
                payForAllButton.hoursLabel.text = visit.actualDurationString
            }
            else {
                payForAllButton.hoursLabel.text = "24 h"
            }
            self.router.presentCardsList()
            if !singleUnpaidVisit {
                self.showbackButton()
            }
        }
        else {
            self.navigationBar.topItem?.title = "PAID VISIT"
        }
        
        if let date = visit.date {
            let dateFormatter = NSDateFormatter()
            let cal = NSCalendar.currentCalendar()
            var components = cal.components(.Day, fromDate: NSDate())
            let today = cal.dateFromComponents(components)
            components = cal.components(.Day, fromDate: date)
            let visitDate = cal.dateFromComponents(components)
            var dateText = ""
            var timeText = ""
            
            dateFormatter.dateFormat = "hh:mm aa"
            let timeString = dateFormatter.stringFromDate(date)
            
            if let t = today, let vd = visitDate where t.isEqualToDate(vd) == true {
                dateText = "Today "
                timeText = "Today \(timeString)"
            } else {
                dateFormatter.dateFormat = "MM.dd"
                dateText = dateFormatter.stringFromDate(date)
                timeText = timeString
            }
            
            
            dateLabel.text = dateText
            
            timeLabel.text = timeText
        }
        if let name = visit.customerName {
            self.customerLabel.text = name
        }
        if let teamName = visit.teamMemberName {
            self.teamMemberLabel.text = teamName
        }
        if let imageUrl = visit.teamMemberAvatar {
            tmAvatarImageView.setupImage(fromURL: imageUrl)
        }
        else {
            if var image = UIImage(named: "avatar") {
                image = image.imageWithRenderingMode(.AlwaysTemplate)
                tmAvatarImageView.tintColor = randomColor.nextColor()
                tmAvatarImageView.image = image
                
            }
        }
        self.statusLabel.text = visit.priceDurationString
    }
    
    // MARK: View Input
    
    // MARK: Private
    func updateRating(rating: Int) {
        for button in ratingButtons {
            if button.tag <= rating {
                button.selected = true
            }
            else {
                button.selected = false
            }
        }
//        for (i, b) in ratingButtons.enumerate() {
//            b.selected = (i < rating)
//        }
    }
    
    func dismiss() {
        navigationController?.popViewControllerAnimated(false)
    }
}

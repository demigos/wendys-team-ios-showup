//
//  PaidVisitRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router

final class PaidVisitRouter: VIPERRouter {
    weak var viewController: PaidVisitViewController!
    weak var presenter: PaidVisitPresenter!
    weak var cardController : CardsListViewController?
    var resources: Dependencies!
    var flow: UnpaidFlow?

    func presentFeedbackDialog(comment: String) {
        FeedbackVisitAssembly.createModule {
            module in
            module.setupDelegate(self.presenter)
            module.setInitialComment(comment)
            }.present(inNavigation: navigationController!)
    }

    func dismissDialog() {
        navigationController?.popToRootViewControllerAnimated(true)
    }

    func dismissTopController() {
        navigationController?.popViewControllerAnimated(true)
    }

    func feedbackSent() {
        if let flow = self.flow {
            if flow.lastSentFeedbackIndex < (flow.visits?.nofeedback.count)! - 1 {
                let paid = PaidVisitAssembly.createModule(resources, flow: self.flow) { module in }
                paid.visitId = flow.visits?.nofeedback[flow.lastSentFeedbackIndex + 1].id
                paid.nofeedback = true
                if let nc = navigationController ?? viewController.navigationController {
                    paid.present(inNavigation: nc)
                } else {
                    if let successView = viewController.success {
                        paid.presentModal(from: successView)
                    }
                    else {
                        paid.presentModal(from: viewController)
                    }
                }
            }
            else {
                self.popToRoot()
            }
        }
        else {
            self.popToRoot()
        }
    }

    func goToFeedbackFromVisit(visitId: Int?) {
        if let id = visitId {
            self.removeVisitFromFeedbackById(id)
        }
        if let flow = self.flow {
            if flow.visits?.nofeedback.count > 0 {
                let paid = PaidVisitAssembly.createModule(resources, flow: self.flow) { module in }
                paid.visitId = flow.visits?.nofeedback[0].id
                paid.nofeedback = true
                if let nc = navigationController ?? viewController.navigationController {
                    paid.present(inNavigation: nc)
                } else {
                    if let successView = viewController.success {
                        paid.presentModal(from: successView)
                    }
                    else {
                        paid.presentModal(from: viewController)
                    }
                }
            }
            else {
                self.popToRoot()
            }
        }
        else {
            self.popToRoot()
        }
    }

    func removeVisitFromFeedbackById(visitId: Int) {
        var removedIndex = -1
        if let noFeedbackVisits = self.flow?.visits?.nofeedback {
            for index in 0..<noFeedbackVisits.count {
                if noFeedbackVisits[index].id == visitId {
                    removedIndex = index
                }
            }
            if removedIndex >= 0 {
                self.flow?.visits?.nofeedback.removeAtIndex(removedIndex)
            }
        }
    }

    func visitDidPaid(visitId: Int?) {
        var tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Payment Success", label: "success", value: nil).build() as [NSObject: AnyObject]
        tracker.send(event)
        if let id = visitId {
            self.removeVisitFromFeedbackById(id)
        }
        if let flow = self.flow {
            if flow.visits?.unpaid.count > 0 {
                self.pop2()
            }
            else {
                if flow.visits?.nofeedback.count > 0 {
                    let paid = PaidVisitAssembly.createModule(resources, flow: self.flow) { module in }
                    paid.visitId = flow.visits?.nofeedback[0].id
                    paid.nofeedback = true
                    if let nc = navigationController ?? viewController.navigationController {
                        paid.present(inNavigation: nc)
                    } else {
                        if let successView = viewController.success {
                            paid.presentModal(from: successView)
                        }
                        else {
                            paid.presentModal(from: viewController)
                        }
                    }
                }
                else {
                    self.popToRoot()
                }
            }
        }
        else {
            self.popToRoot()
        }
    }

    func removeUnpaidVisit(id: Int) {
        if let flow = self.flow {
            flow.removeUnpaidVisit(id)
        }
    }

    func setLastFeedbackVisit(id: Int) {
        if let flow = self.flow {
            flow.lastSentFeedbackIndex += 1
        }
    }

    func pop2() {
        self.viewController.visitPaid()
        let viewControllers = self.navigationController?.viewControllers
        self.navigationController?.popToViewController(viewControllers![(viewControllers?.count)! - 3], animated: true)
    }

    func presentCardsList()
    {
        self.cardController = CardsListAssembly.createModule(resources) { (module) in
            module.setupNavigation(self.navigationController!)
            module.refreshList()
        }
        self.cardController?.openedFromPaidView = true
        self.cardController?.presentChild(from: viewController)
    }

    func presentSuccessDialog()
    {
        guard let success = viewController.success else {
            return
        }
        success.titleText = "Thank you!"
        if self.viewController.unpaid {
            success.titleText = "Thank you!"
            success.summaryText = "We value your feedback.\r\n\r\nSincerely,\r\nWendy’s Team"
        }
        else {
            success.titleText = "Thank you!"
            success.summaryText = "We value your feedback.\r\n\r\nSincerely,\r\nWendy’s Team"
        }

        if let nc = viewController.navigationController {
            present(success, using: nc)
        } else {
            presentModal(success, from: viewController)
        }
    }

}

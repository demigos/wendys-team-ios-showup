//
//  PaidVisitAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 31/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class PaidVisitAssembly
{
    class func createModule(resources: Dependencies, flow : UnpaidFlow? = nil, configure: (module: PaidVisitModuleInput) -> Void) -> PaidVisitViewController
    {
        let vc = R.storyboard.rateAndFeedback.paidVisitViewController()!
        let successDialog = R.storyboard.rateAndFeedback.successFeedbackViewController()!
        successDialog.parent = vc
        vc.success = successDialog

        let interactor = PaidVisitInteractor(visitsDS: resources.datasources.visits)

        let presenter = PaidVisitPresenter()
        let router = PaidVisitRouter()
        router.resources = resources
        router.flow = flow


        interactor.output = presenter

        presenter.viewController = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
        router.presenter = presenter

        return vc
    }
}

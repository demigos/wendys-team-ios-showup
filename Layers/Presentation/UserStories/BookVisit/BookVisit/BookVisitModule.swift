//
//  BookVisitModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Book visit flow (steps)
enum BookVisitFlowStep  {
    
    case PersonSelect
    case ContactInfo
    case Services
    case DurationAndTime
    case Summary
    case Prev
    case SummaryContactInfo
    case SummaryDurationAndTime
    case SummaryServices
    case Success
}

protocol BookVisitFlow
{
    var initialStep: BookVisitFlowStep { get }
    func isFirstFlow() -> Bool
    func next(step: BookVisitFlowStep) -> BookVisitFlowStep
    
    // Validates step and return valid step for input step
    func validStepFor(step: BookVisitFlowStep) -> BookVisitFlowStep
}

struct BookVisitDefaultFlow: BookVisitFlow
{
    
    var initialStep = BookVisitFlowStep.PersonSelect
    var first = false
    
    func isFirstFlow() -> Bool {
        return self.first
    }
    
    func next(step: BookVisitFlowStep) -> BookVisitFlowStep
    {
        switch step {
            
        case .PersonSelect: return .ContactInfo
        case .ContactInfo: return .Services
        case .Services: return .DurationAndTime
        case .DurationAndTime:
            return .Summary
        case .Success: return .Summary
        case .SummaryContactInfo
        , .SummaryDurationAndTime
        , .SummaryServices:
            return .Prev
        case .Summary:
            return .Success
        case .Prev:
            return .Summary
        }
    }
    
    
    func validStepFor(step: BookVisitFlowStep) -> BookVisitFlowStep
    {
        return step
    }
    
}

protocol BookVisitStepModuleInput: class
{
    func setup(output output: BookVisitStepModuleOutput, step: BookVisitFlowStep)
    func updateVisit(visit: VisitDomainModel)
}


protocol BookVisitStepModuleOutput: class
{
    func stepDidCancel(step: BookVisitFlowStep, visit: VisitDomainModel)
    func stepDidSuccess(step: BookVisitFlowStep, visit: VisitDomainModel)
    func isFirstFlow() -> Bool
    func visitDidCreate(visit: VisitDomainModel)
}

// MARK: Book Visit (Summary)
protocol BookVisitModuleInput: class
{
    func setupDelegate(output: BookVisitModuleOutput)
    func setupCustomer(customer: CustomerDomainModel)
    func updateVisit(visit: VisitDomainModel)
}

//MARK: Output
protocol BookVisitModuleOutput: class
{
    func visitBookingDidCanceled()
    func visitDidBooked(visit: VisitDomainModel)
}


// MARK: - Presenter
final class BookVisitPresenter: BookVisitModuleInput
    , BookVisitViewOutput
    , BookVisitInteractorOutput
    , BookVisitStepModuleOutput
    , BookingSummaryModuleOuput,
    DateTimeModuleOuput
{
    weak var view: BookVisitViewInput!
    weak var resources: Dependencies!
    var interactor: BookVisitInteractorInput!
    weak var router: BookVisitRouter!
    weak var output: BookVisitModuleOutput?
    var step: BookVisitFlowStep!
    var prevStep = BookVisitFlowStep.PersonSelect
    var visit: VisitDomainModel!
    
    var flow: BookVisitFlow
    
    func isFirstFlow() -> Bool {
        return flow.isFirstFlow()
    }
    
    init(flow: BookVisitFlow)
    {
        self.flow = flow
    }
    
    func setupDelegate(output: BookVisitModuleOutput)
    {
        self.output = output
        self.step = .Summary
    }
    
    func setupCustomer(customer: CustomerDomainModel)
    {
        step = .ContactInfo
        visit = interactor.createVisit(forCustomer: customer)
    }
    
    func updateVisit(visit: VisitDomainModel)
    {
        self.visit = visit
    }
    
    // MARK: - Interactor Output
    
    // MARK: - View Output
    
    // MARK: - Step Output (BookVisitStepModuleOutput)
    func stepDidSuccess(step: BookVisitFlowStep, visit: VisitDomainModel)
    {
        let next = flow.next(step)
        if next == .Prev {
            DispatchToMainQueue {
                
                let viewControllers = self.router.navigationController?.viewControllers
                let contr = viewControllers![(viewControllers?.count)! - 2]
                if let contr1 = contr as? BookingSummaryViewController {
                    contr1.updateVisit(visit)
                }
                
                self.router.popCurrentScreen()
                self.step = self.prevStep
            }
        }
        else {
            self.prevStep = self.step
            DispatchToMainQueue {
                self.router.presentStep(next, visit: visit)
            }
        }
    }
    
    func stepDidCancel(step: BookVisitFlowStep, visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.popCurrentScreen()
            self.step = self.prevStep
        }
    }
    
    // MARK: BookingSummaryModuleOuput
    func visitDidCreate(visit: VisitDomainModel)
    {
        output?.visitDidBooked(visit)
    }
    
    func servicesDidSelect(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.presentStep(self.flow.validStepFor(.SummaryServices), visit: visit)
        }
    }
    
    func dateTimeDidSelect(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.presentStep(self.flow.validStepFor(.SummaryDurationAndTime), visit: visit)
        }
    }
    
    func contactInfoDidSelect(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.presentStep(self.flow.validStepFor(.SummaryContactInfo), visit: visit)
        }
    }
    
    func bookingDidCancel()
    {
        //        output?.visitBookingDidCanceled()
    }
    
}
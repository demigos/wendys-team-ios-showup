//
//  BookVisitAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class BookVisitAssembly
{
    
    class func createModule(resources resources: Dependencies
        , flow: BookVisitFlow
        , configure: (module: BookVisitModuleInput) -> Void) -> BookVisitViewController
    {
        
        let interactor = BookVisitInteractor()
        let presenter = BookVisitPresenter(flow: flow)
        let router = BookVisitRouter()
        
        presenter.visit = VisitDomainModel()
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
        interactor.output = presenter
        router.presenter = presenter
        
        configure(module: presenter)
        
        presenter.step = flow.initialStep
        let vc = router.rootViewController()
        if vc is ChosePersonViewController {
            (vc as! ChosePersonViewController).firstVisit = flow.isFirstFlow()
        }
        let nc = BookVisitViewController(rootViewController: vc)
        nc.firstVisit = flow.isFirstFlow()
        nc.navigationBarHidden = true
        nc.router = router
        nc.output = presenter
        presenter.view = nc
        router.navigationController = nc

        return nc
    }
}
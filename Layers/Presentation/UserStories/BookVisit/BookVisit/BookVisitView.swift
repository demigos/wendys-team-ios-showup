//
//  BookVisitView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol BookVisitViewInput: class
{

}

protocol BookVisitViewOutput: class
{

}

// MARK: - View Controller
final class BookVisitViewController: UINavigationController
    , BookVisitViewInput
	, Routable
{
    var output: BookVisitViewOutput!
    typealias RouterType = BookVisitRouter
    var router: RouterType!
    var firstVisit = false
    // MARK: - Life cycle
    deinit {
        print("[D] \(self) destroyed")
    }

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        /* Initialization here */
    }

    // MARK: View Input
}
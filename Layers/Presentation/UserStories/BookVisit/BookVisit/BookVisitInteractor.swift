//
//  BookVisitInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol BookVisitInteractorInput: class
{
    func createVisit(forCustomer customer: CustomerDomainModel) -> VisitDomainModel
}

//MARK: Output
protocol BookVisitInteractorOutput: class
{

}

// MARK: - Interactor
final class BookVisitInteractor: BookVisitInteractorInput
{
    weak var output: BookVisitInteractorOutput!

    // MARK: Interface
    
    func createVisit(forCustomer customer: CustomerDomainModel) -> VisitDomainModel
    {
        var visit = VisitDomainModel()
        
        visit.isOwner = customer.isOwner
        visit.customerID = String(customer.id)
        visit.customerName = customer.name
        visit.address = customer.address
        visit.zipCode = customer.zipcode
        visit.phone = customer.phone
        visit.services = []
        
        return visit
    }
    

}
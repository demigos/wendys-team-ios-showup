//
//  BookVisitRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class BookVisitRouter: VIPERRouter
{
    weak var presenter: BookVisitPresenter!
    weak var root: UIViewController?
    
    func presentStep(step: BookVisitFlowStep, visit: VisitDomainModel)
    {
//        guard step != presenter.step else { return }
        
        presenter.step = step
        guard let nc = navigationController else { return }
        let stepController = stepViewController(step, visit: visit)
        present(stepController, using: nc)
    }
    
    func stepViewController(step: BookVisitFlowStep, visit: VisitDomainModel) -> UIViewController
    {
        switch step {
            
        case .PersonSelect:
            return ChosePersonAssembly.createModule(resources: presenter.resources) { (module) in
                module.updateVisit(visit)
                module.setup(output: self.presenter, step: step)
            }
            
        case .ContactInfo, .SummaryContactInfo:
            return ContactsAssembly.createModule({ (module) in
                module.updateVisit(visit)
                module.setup(output: self.presenter, step: step)
            })
            
        case .Services, .SummaryServices:
            return SelectServicesAssembly.createModule({ (module) in
                module.updateVisit(visit)
                module.setup(output: self.presenter, step: step)
            })
            
        case .DurationAndTime, .SummaryDurationAndTime:
            return DateTimeAssembly.createModule(resources: presenter.resources) { (module) in
                module.updateVisit(visit)
                module.setup(output: self.presenter, step: step)
            }
            
        case .Summary:
            return BookingSummaryAssembly.createModule(resources: presenter.resources) { (module) in
                module.updateVisit(visit)
                module.setup(output: self.presenter, step: step)
            }
            
        case .Success:
            return UIViewController()
          
        default:
            return UIViewController()
        }
    }
    
    
    func presentSuccessScreen()
    {
        //
    }

    func rootViewController() -> UIViewController
    {
        root = stepViewController(presenter.step, visit: presenter.visit)
        return root!
    }
    
    override func popCurrentScreen()
    {
        if navigationController?.topViewController == root {
            presenter.output?.visitBookingDidCanceled()
        } else {
            super.popCurrentScreen()
        }
    }
    
}
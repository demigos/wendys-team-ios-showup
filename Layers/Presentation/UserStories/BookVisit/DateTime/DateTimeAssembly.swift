//
//  DateTimeAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class DateTimeAssembly
{
    class func createModule(resources resources: Dependencies, configure: (module: BookVisitStepModuleInput) -> Void) -> DateTimeViewController
    {
        let vc = R.storyboard.bookVisit.dateTimeViewController()!
        let successDialog = R.storyboard.bookVisit.successViewController()!
        vc.success = successDialog
        successDialog.parent1 = vc
        let ds = resources.datasources
        let interactor = DateTimeInteractor(visits: ds.visits, cards: ds.cards)
        let presenter = DateTimePresenter()
        let router = DateTimeRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc
        router.presenter = presenter

        return vc
    }
}
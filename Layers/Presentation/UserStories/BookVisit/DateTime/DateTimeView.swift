//
//  DateTimeView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit


// MARK: - Interface
struct DateTimeDuration
{
    var dateTime: NSDate?
    var duration: Int

}

protocol DateTimeViewInput: class
{
    func fill(info: DateTimeDuration)
    func update(withError error: String)
}

protocol DateTimeViewOutput: class
{
    func viewDidLoad()
    func back()
    func next(info: DateTimeDuration)
    func successDoneButtonDidSelect()
}

// MARK: - View Controller
final class DateTimeViewController: BaseViewController
    , DateTimeViewInput
	, Routable
{
    var output: DateTimeViewOutput!
    typealias RouterType = DateTimeRouter
    var router: RouterType!
    var info: DateTimeDuration!
    var success: SuccessViewController!
    var duration : UInt = 4 {
        didSet {
            selectedHoursLabel.text = "\(duration)h"
            slider.value = Float(duration)
        }
    }

    // MARK: Interface Builder
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var selectedHoursLabel: UILabel!
    @IBOutlet weak var slider: UISlider!

    @IBAction func sliderValueChanged(sender: AnyObject)
    {
        duration = UInt(slider.value)
    }

    @IBAction func backButtonAction(sender: AnyObject)
    {
        output.back()
    }

    @IBAction func nextButtonAction(sender: AnyObject)
    {
        info.dateTime = datePicker.date
        info.duration = Int(slider.value)
        output.next(info)
    }

    @IBAction func phoneAction(sender: AnyObject)
    {
        PhoneService.call()
    }

    func update(withError error: String)
    {
        showError(error)
    }

    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        output.viewDidLoad()
        datePicker.minimumDate = NSDate()
    }

    // MARK: View Input

    func fill(info: DateTimeDuration)
    {
        self.info = info
        if let date = info.dateTime {
            datePicker.date = date
        }

        self.duration = UInt(info.duration)
    }

}
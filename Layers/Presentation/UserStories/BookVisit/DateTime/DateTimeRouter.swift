//
//  DateTimeRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class DateTimeRouter: VIPERRouter
{
    weak var view: DateTimeViewController!
    weak var presenter: DateTimePresenter!
    
    func presentSuccessDialog()
    {
        let success = view.success
        
        if let nc = view.navigationController {
            present(success, using: nc)
        } else {
            presentModal(success, from: view)
        }
    }
    
    func dismissAddCardScreen()
    {
        view.dismissViewControllerAnimated(true) {}
    }
    
    func presentAddCardScreen()
    {
        AddCardAssembly.createModule(presenter.resources) { module in
            module.setupDelegate(self.presenter)
            }.present(inNavigation: self.view.navigationController!)
    }
}
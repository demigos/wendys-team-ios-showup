//
//  DateTimeModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
// Used BookVisitStepModuleInput
// Used BookVisitStepModuleOutput

protocol DateTimeModuleOuput: class
{
    func visitDidCreate(visit: VisitDomainModel)
}



// MARK: - Presenter
final class DateTimePresenter: BookVisitStepModuleInput
    , DateTimeViewOutput
    , DateTimeInteractorOutput,
    AddCardModuleOutput
{
    weak var view: DateTimeViewInput!
    var interactor: DateTimeInteractorInput!
    weak var router: DateTimeRouter!
    weak var output: BookVisitStepModuleOutput?
    var step: BookVisitFlowStep!
    var visit: VisitDomainModel!
    var resources: Dependencies!
    
    func setup(output output: BookVisitStepModuleOutput, step: BookVisitFlowStep)
    {
        self.output = output
        self.step = step
    }
    
    func needsCard(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.presentAddCardScreen()
        }
    }
    
    func successDoneButtonDidSelect()
    {
        output?.visitDidCreate(visit)
    }
    
    func cardAddingDidCancel()
    {
        DispatchToMainQueue {
            self.router.dismissAddCardScreen()
        }
    }
    
    func cardDidAdd(card: PaymentCardDomainModel)
    {
        if let flowOutput = self.output {
//            if flowOutput.isFirstFlow() {
//                self.interactor.bookVisit(visit)
//                return
//            }
        }
//        DispatchToMainQueue {
////            self.router.dismissAddCardScreen()
//        }
    }
    
    
    func updateVisit(visit: VisitDomainModel)
    {
        self.visit = visit
    }
    
    func visitDidCreate(visit: VisitDomainModel)
    {
        self.visit = visit
        DispatchToMainQueue {
            self.router.presentSuccessDialog()
        }
    }
    
    func visitDidFailToCreate(error: WTError)
    {
        DispatchToMainQueue {
            self.view.update(withError: error.errorDescription)
        }
    }
    
    func viewDidLoad()
    {
        let info = DateTimeDuration(dateTime: visit.date, duration: visit.duration ?? 4)
        view.fill(info)
    }
    
    // MARK: - Interactor Output
    
    // MARK: - View Output
    func back()
    {
        output?.stepDidCancel(step, visit: visit)
    }
    
    func next(info: DateTimeDuration) {
        visit.date = info.dateTime
        visit.duration = info.duration
        if let flowOutput = self.output {
//            if flowOutput.isFirstFlow() {
//                self.interactor.bookVisit(visit)
//                return
//            }
        }
        output?.stepDidSuccess(step, visit: visit)
    }
}
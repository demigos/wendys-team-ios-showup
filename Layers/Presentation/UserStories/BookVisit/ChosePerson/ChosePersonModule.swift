//
//  ChosePersonModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
// Used BookVisitStepModuleInput
// Used BookVisitStepModuleOutput


// MARK: - Presenter

final class ChosePersonPresenter: BookVisitStepModuleInput
        , ChosePersonViewOutput
        , ChosePersonInteractorOutput
        , CustomersListModuleOutput {
    weak var resources: Dependencies!
    weak var view: ChosePersonViewInput!
    var interactor: ChosePersonInteractorInput!
    weak var router: ChosePersonRouter!
    weak var output: BookVisitStepModuleOutput?

    var step: BookVisitFlowStep!
    var visit: VisitDomainModel!
    var customer: CustomerDomainModel?
    var owner: CustomerDomainModel?

    func setup(output output: BookVisitStepModuleOutput, step: BookVisitFlowStep) {
        self.output = output
        self.step = step
        self.interactor.loadOwner()
    }

    func updateVisit(visit: VisitDomainModel) {
        self.visit = visit
    }

    // MARK: - Interactor Output
    func ownerDidLoad(owner: CustomerDomainModel) {
        self.owner = owner
    }

    // MARK: - View Output

    func viewDidLoad() {
        if let owner = ApplicationState.loadCustomer() {
            ownerDidLoad(owner)
        }
        router.presentCustomersList()
    }

    func back() {
        output?.stepDidCancel(step, visit: visit)
    }

    func meDidSelect() {
        guard let owner = owner else {
            // self.view.updateWithError("There is no any owner on this account!")
            return
        }

        nextStep(customer: owner)

    }


    // MARK: CustomersListModuleOutput
    func customerDidSelect(customer: CustomerDomainModel) {
        nextStep(customer: customer)
    }

    // MARK: Private
    private func nextStep(customer customer: CustomerDomainModel) {
        let updatedVisit = interactor.updateVisit(visit, forCustomer: customer)
        output?.stepDidSuccess(step, visit: updatedVisit)
    }

}
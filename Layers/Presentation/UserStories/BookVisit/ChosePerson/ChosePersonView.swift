//
//  ChosePersonView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol ChosePersonViewInput: class
{
    func updateWithError(error: String)
}

protocol ChosePersonViewOutput: class
{
    func viewDidLoad()
    
    func back()
    func meDidSelect()
//    func lovedOneDidSelect()
}

// MARK: - View Controller
final class ChosePersonViewController: BaseViewController
    , ChosePersonViewInput
    , Routable
    , TwoOptionsSwitchDelegate
{
    var output: ChosePersonViewOutput!
    typealias RouterType = ChosePersonRouter
    var router: RouterType!
    var firstVisit = false
    weak var resources: Dependencies!
    
    // MARK: Interface Builder
    @IBOutlet weak var personSwitch: PersonSwitch!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var continueButton: RoundedButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    @IBAction func backButtonAction(sender: AnyObject)
    {
        output.back()
    }
    
    @IBAction func nextButtonAction(sender: AnyObject)
    {
        if personSwitch.person == .Me {
            output.meDidSelect()
        }
    }
    
    @IBAction func phoneAction(sender: AnyObject)
    {
        PhoneService.call()
    }
    
    
    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        output.viewDidLoad()
        personSwitch.delegate = self
        container.hidden = false
        continueButton.enabled = false
        meState()
        if firstVisit {
            self.hideBackButton()
        }
    }
    
    func hideBackButton() {
        backButton.enabled = false
        backButton.image = nil
    }
    
    // MARK: View Input
    
    func updateWithError(error: String)
    {
        DispatchToMainQueue {
            self.showError(error)
        }
        
    }
    
    
    // MARK: TwoOptionsSwitchDelegate
    func optionDidSelect(index: Int, optionsSwitch: TwoOptonsSwitch)
    {
        if personSwitch.person == .Me {
            continueButton.enabled = true
            meState()
        } else {
            lovedOneState()
        }
    }
    
    // MARK: Private
    
    func meState()
    {
//        continueButton.enabled = true
        container.hidden = true
        bottomView.hidden = false
    }
    
    func lovedOneState()
    {
        container.hidden = false
        continueButton.enabled = false
        if firstVisit {
            self.goToAddPerson()
        }
    }
    
    func goToAddPerson() {
        if let nc = navigationController {
        AddCustomerAssembly.createModule(self.resources) { (module) in
            }.presentModal(from: self)
        }
    }
    
}
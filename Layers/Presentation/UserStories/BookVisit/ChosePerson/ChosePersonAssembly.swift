//
//  ChosePersonAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class ChosePersonAssembly
{
    class func createModule(resources resources: Dependencies, configure: (module: BookVisitStepModuleInput) -> Void) -> ChosePersonViewController
    {
        let vc = R.storyboard.bookVisit.chosePersonViewController()!
        let interactor = ChosePersonInteractor(customers: resources.datasources.customers)
        let presenter = ChosePersonPresenter()
        let router = ChosePersonRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
		
        configure(module: presenter)
        vc.output = presenter
        vc.router = router
        vc.resources = resources
        router.viewController = vc
        router.presenter = presenter

        return vc
    }
}
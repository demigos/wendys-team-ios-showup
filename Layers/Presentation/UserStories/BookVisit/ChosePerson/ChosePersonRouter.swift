//
//  ChosePersonRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class ChosePersonRouter: VIPERRouter
{
    weak var viewController: ChosePersonViewController!
    weak var presenter: ChosePersonPresenter!
    
    func presentCustomersList()
    {
        
        let vc = CustomersListAssembly.createModule(presenter.resources) { (module) in
            module.setupDelegate(self.presenter)
            module.setLayout(.Short)
        }
        
        let parent = viewController

        parent.addChildViewController(vc)
        vc.view.frame = parent.view.bounds
        parent.container.addSubview(vc.view)
        vc.didMoveToParentViewController(parent)

    }
}
//
//  ChosePersonInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol ChosePersonInteractorInput: class
{
    func loadOwner()
    func updateVisit(visit: VisitDomainModel, forCustomer customer: CustomerDomainModel) -> VisitDomainModel
}

//MARK: Output
protocol ChosePersonInteractorOutput: class
{
    func ownerDidLoad(owner: CustomerDomainModel)
}

// MARK: - Interactor
final class ChosePersonInteractor: ChosePersonInteractorInput
{
    weak var output: ChosePersonInteractorOutput!

    var customers : CustomersDSInterface

    init(customers: CustomersDSInterface)
    {
        self.customers = customers
    }

    func loadOwner()
    {
        customers.ownerUpdate.observeResult({[weak self] result in
            switch result {
            case .Success(let owner):
                self?.handleOwner(owner)
            default:break
            }
        })


        customers.loadCustomers()
    }

    func handleOwner(owner: CustomerDomainModel)
    {
        output?.ownerDidLoad(owner)
    }


    func updateVisit(visit: VisitDomainModel, forCustomer customer: CustomerDomainModel) -> VisitDomainModel
    {
        var newVisit = visit

        newVisit.customerID = String(customer.id)
        newVisit.isOwner = customer.isOwner
        newVisit.customerName = customer.name
        newVisit.address = customer.address
        newVisit.zipCode = customer.zipcode
        newVisit.phone = customer.phone
        newVisit.services = []

        return newVisit
    }

}
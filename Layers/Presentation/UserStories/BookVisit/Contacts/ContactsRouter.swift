//
//  ContactsRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class ContactsRouter: VIPERRouter
{
    weak var viewController: ContactsViewController!
    
    func presentForm()
    {
        let vc = viewController.form
        let parent = viewController
        
        parent.addChildViewController(vc)
        vc.view.frame = parent.container.bounds
        parent.container.addSubview(vc.view)
        vc.didMoveToParentViewController(parent)
        
    }

}
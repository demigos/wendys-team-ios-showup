//
//  ContactsAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class ContactsAssembly
{
    class func createModule(configure: (module: BookVisitStepModuleInput) -> Void) -> ContactsViewController
    {
        let vc = R.storyboard.bookVisit.contactsViewController()!
        let form = R.storyboard.bookVisit.contactsFormController()!
        vc.form = form
        form.parent = vc
        
        let interactor = ContactsInteractor()
        let presenter = ContactsPresenter()
        let router = ContactsRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc

        return vc
    }
}
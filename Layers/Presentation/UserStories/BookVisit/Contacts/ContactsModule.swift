//
//  ContactsModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
// Used BookVisitStepModuleInput
// Used BookVisitStepModuleOutput

// MARK: - Presenter
final class ContactsPresenter: BookVisitStepModuleInput
    , ContactsViewOutput
    , ContactsInteractorOutput
{
    weak var view: ContactsViewInput!
    var interactor: ContactsInteractorInput!
	weak var router: ContactsRouter!
    weak var output: BookVisitStepModuleOutput?
    
    var step: BookVisitFlowStep!
    var visit: VisitDomainModel!

    func setup(output output: BookVisitStepModuleOutput, step: BookVisitFlowStep)
    {
        self.output = output
        self.step = step
    }
    
    func updateVisit(visit: VisitDomainModel)
    {
        self.visit = visit
    }

    // MARK: - Interactor Output

    // MARK: - View Output
    
    func viewDidLoad()
    {
        let info = ContactInfo(zipcode: visit.zipCode ?? "", address: visit.address ?? "", phone: visit.phone ?? "", isOwner: visit.isOwner)
        view.fill(info)
    }
    
    func next(info: ContactInfo)
    {
        let updatedVisit = interactor.updateVisit(visit, info: info)
        output?.stepDidSuccess(step, visit: updatedVisit)
    }
    
    func back()
    {
        output?.stepDidCancel(step, visit: visit)
    }
}
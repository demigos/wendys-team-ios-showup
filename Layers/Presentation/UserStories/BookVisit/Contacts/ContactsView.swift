//
//  ContactsView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

struct ContactInfo {
    let zipcode: String
    let address: String
    let phone: String
    let isOwner: Bool
}

protocol ContactsViewInput: class {
    func updateWithError(error: String)

    func fill(info: ContactInfo)
}

protocol ContactsViewOutput: class {
    func viewDidLoad()

    func back()

    func next(info: ContactInfo)

}

// MARK: - Form Controller

final class ContactsFormController: UITableViewController {
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var phoneField: MaskTextField!

    var isOwner = false

    @IBAction func textEditAction(sender: AnyObject) {
        parent.validateInfo()
    }

    weak var parent: ContactsViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        clear()
        parent.output.viewDidLoad()
    }

    func clear() {
        zipCodeField.text = ""
        adressField.text = ""
        phoneField.text = ""
    }

    func updateForLovedOne() {

    }

    func fill(info: ContactInfo) {
        self.isOwner = info.isOwner
        if !isOwner {
            zipCodeField.placeholder = "Enter loved one's ZIP code"
            adressField.placeholder = "Enter loved one's address"
            phoneField.placeholder = "Enter loved one's number"
        }
        zipCodeField.text = info.zipcode
        adressField.text = info.address
        phoneField.text = info.phone
    }

    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectMake(0, 0, 0, 0))
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
}

// MARK: - View Controller

final class ContactsViewController: BaseViewController
        , ContactsViewInput
        , Routable {
    var output: ContactsViewOutput!
    typealias RouterType = ContactsRouter
    var router: RouterType!
    var form: ContactsFormController!

    // MARK: Interface Builder
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var continueButton: RoundedButton!

    var originalFrame = CGRectZero

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var contentViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!

//    var form : CIFormViewController!

    @IBAction func backButtonAction(sender: AnyObject) {
        output.back()
    }

    @IBAction func nextButtonAction(sender: AnyObject) {
        let info = ContactInfo(zipcode: form.zipCodeField.text ?? "", address: form.adressField.text ?? "", phone: form.phoneField.text ?? "", isOwner: form.isOwner)
        output.next(info)
    }

    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }

    func validateInfo() {
        let disabled = (form.adressField.text?.characters.count ?? 0) == 0
                || (form.zipCodeField.text?.characters.count ?? 0) != 5
                || (form.phoneField.text?.characters.count ?? 0) != 12
        print("\(disabled)")
        continueButton.enabled = !disabled
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router.presentForm()
    }

    // MARK: View Input
    func fill(info: ContactInfo) {
        if !info.isOwner {
            titleLabel.text = "Enter loved one's contacts"
        }
        form.fill(info)
        validateInfo()
    }

    func updateWithError(error: String) {
        DispatchToMainQueue {
            self.showError(error)
        }
    }

    //MARK: - Keyboard handling
    //
    override func keyboardWillShow(kb: KeyboardParameters) {
        let height = CGRectGetHeight(kb.frameEnd)

        UIView.animateWithDuration(kb.animationDuration, delay: 0, options: kb.animationCurve, animations: {
            self.bottomPadding.constant = height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }) {
            (_) in }
    }

    override func keyboardWillHide(kb: KeyboardParameters) {
        UIView.animateWithDuration(kb.animationDuration, delay: 0, options: kb.animationCurve, animations: {
            self.bottomPadding.constant = 0
        }) {
            (_) in
            //
        }
    }

}

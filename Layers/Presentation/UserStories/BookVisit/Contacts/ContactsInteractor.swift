//
//  ContactsInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol ContactsInteractorInput: class
{
    func updateVisit(visit: VisitDomainModel, info: ContactInfo) -> VisitDomainModel
}

//MARK: Output
protocol ContactsInteractorOutput: class
{

}

// MARK: - Interactor
final class ContactsInteractor: ContactsInteractorInput
{
    weak var output: ContactsInteractorOutput!
    
    
    func updateVisit(visit: VisitDomainModel, info: ContactInfo) -> VisitDomainModel
    {
        var newVisit = visit
        
        newVisit.address = info.address
        newVisit.zipCode = info.zipcode
        newVisit.phone = info.phone
        
        return newVisit
    }
}
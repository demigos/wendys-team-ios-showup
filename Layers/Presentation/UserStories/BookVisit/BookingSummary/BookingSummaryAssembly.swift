//
//  BookingSummaryAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class BookingSummaryAssembly
{
    class func createModule(resources resources: Dependencies, configure: (module: BookingSummaryModuleInput) -> Void) -> BookingSummaryViewController
    {
        let vc = R.storyboard.bookVisit.bookingSummaryViewController()!
        let form = R.storyboard.bookVisit.summaryTableViewController()!
        let successDialog = R.storyboard.bookVisit.successViewController()!
        vc.success = successDialog
        vc.form = form
        form.parent = vc
        successDialog.parent = vc
        
        let ds = resources.datasources
        let interactor = BookingSummaryInteractor(visits: ds.visits, cards: ds.cards)
        let presenter = BookingSummaryPresenter()
        let router = BookingSummaryRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
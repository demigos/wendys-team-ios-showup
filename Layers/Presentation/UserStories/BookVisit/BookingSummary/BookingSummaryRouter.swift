//
//  BookingSummaryRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class BookingSummaryRouter: VIPERRouter
{
    weak var viewController: BookingSummaryViewController!
	weak var presenter: BookingSummaryPresenter!
    
    func presentForm()
    {
        let vc = viewController.form
        let parent = viewController
        
        parent.addChildViewController(vc)
        vc.view.frame = parent.container.bounds
        parent.container.addSubview(vc.view)
        vc.didMoveToParentViewController(parent)
        
    }
    
    func presentSuccessDialog()
    {
        let success = self.viewController.success
        if let nc = self.viewController.navigationController {
            present(success, using: nc)
        } else {
            presentModal(success, from: self.viewController)
        }
    }
    
    func presentAddCardScreen()
    {
        
        AddCardAssembly.createModule(presenter.resources) { module in
            module.setupDelegate(self.presenter)
        }.present(inNavigation: self.viewController.navigationController!)
        
    }
    
    func dismissAddCardScreen()
    {
        viewController.dismissViewControllerAnimated(true) {}
    }
}
//
//  SummaryTableViewController.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 23/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

enum SummaryCell : Int {
    case Services = 0
    case Date = 1
    case Time = 2
    case Duration = 3
    case ZipCode = 5
    case Address = 6
    case Phone = 7
}


class SummaryTableViewController: UITableViewController
{
    
    weak var parent: BookingSummaryViewController!
    var visit: VisitDomainModel!
    
    // MARK: Interface Builder
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var zipCodeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    
    // MARK: Form properties
    let rowHeight = CGFloat(56)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parent.output.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = rowHeight
    }
    
    func clear()
    {
        servicesLabel.text = ""
        dateLabel.text = ""
        timeLabel.text = ""
        nameLabel.text = ""
        durationLabel.text = ""
        zipCodeLabel.text = ""
        addressLabel.text = ""
        phoneLabel.text = ""
    }
    
    func fill(visit: VisitDomainModel)
    {
        clear()
        self.visit = visit
        let services = visit.services
        if  services.count > 0 {
            var servicesText = ""
            for (i,s) in services.enumerate() {
                servicesText += s.description
                if i < services.count - 1 {
                    servicesText += ", "
                }
            }
            
            servicesLabel.text = servicesText
        }
        
        if let name = visit.customerName {
            nameLabel.text = name
        }
        
        if let date = visit.date {
            let dateFormatter = NSDateFormatter()
            let cal = NSCalendar.currentCalendar()
            var components = cal.components(.Day, fromDate: NSDate() )
            let today = cal.dateFromComponents(components)
            components = cal.components(.Day, fromDate: date)
            let visitDate = cal.dateFromComponents(components)
            var dateText = ""
            var timeText = ""
            
            dateFormatter.dateFormat = "hh:mm  aa"
            let timeString = dateFormatter.stringFromDate(date)
            
            if let t = today, let vd = visitDate where t.isEqualToDate(vd) == true {
                dateText = "Today "
                timeText = "Today \(timeString)"
            } else {
                dateFormatter.dateFormat = "MMMM dd"
                dateText = dateFormatter.stringFromDate(date) + " "
                timeText = timeString
            }
            
            
            dateLabel.text = dateText
            
            timeLabel.text = timeText
        }
        
        if let duration = visit.duration {
            durationLabel.text = "\(duration)h about $\(UInt(duration * 20))"
        }
        
        if let zipcode = visit.zipCode {
            zipCodeLabel.text = zipcode
        }
        
        if let address = visit.address {
            addressLabel.text = address
        }
        
        if let phone = visit.phone {
            phoneLabel.text = phone
        }
        tableView.reloadData()
        
    }
    
    // MARK: TableView Delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        guard let cell = SummaryCell(rawValue: indexPath.row) else {
            return
        }
        
        switch  cell {
        case .Services:
            parent.output?.servicesDidSelect(visit)
            
        case .Date, .Time, .Duration:
            parent.output?.dateTimeDidSelect(visit)
            
        case .ZipCode, .Address, .Phone:
            parent.output?.contactInfoDidSelect(visit)
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let row = indexPath.row
        if [1,2,3,4,5,7].contains(row) { // rows without autoheight
            return rowHeight
        }
        
        return UITableViewAutomaticDimension
    }
    
    
    
}

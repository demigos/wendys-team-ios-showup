//
//  BookingSummaryView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol BookingSummaryViewInput: class
{
    func fill(visit: VisitDomainModel)
    func waitMode(on: Bool)
    func update(withError error: String)
}

protocol BookingSummaryViewOutput: class
{
    func viewDidLoad()
    func back()
    func next(visit: VisitDomainModel)
    
    func servicesDidSelect(visit: VisitDomainModel)
    func dateTimeDidSelect(visit: VisitDomainModel)
    func contactInfoDidSelect(visit: VisitDomainModel)
    
    func successDoneButtonDidSelect()
    func reloadTable()
    func updateVisit(visit: VisitDomainModel)
    func bookVisit(visit: VisitDomainModel)
}


// MARK: - View Controller
final class BookingSummaryViewController: BaseViewController
    , BookingSummaryViewInput
	, Routable
{
    var output: BookingSummaryViewOutput!
    typealias RouterType = BookingSummaryRouter
    var router: RouterType!
    var form: SummaryTableViewController!
    var success: SuccessViewController!
    
    // MARK: Interface Builder
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBAction func backButtonAction(sender: AnyObject)
    {
        output.back()
    }
    
    @IBAction func nextButtonAction(sender: AnyObject)
    {
        waitMode(true)
        output.next(form.visit)
    }
    
    func updateVisit(visit: VisitDomainModel) {
        self.output.updateVisit(visit)
        self.output.reloadTable()
    }
    
    @IBAction func phoneAction(sender: AnyObject)
    {
        PhoneService.call()
    }


    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        router.presentForm()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.output.reloadTable()
    }

    // MARK: View Input
 
    func update(withError error: String)
    {
        showError(error)
    }
    
    func fill(visit: VisitDomainModel)
    {
        form.fill(visit)
    }
    
}
//
//  SuccessViewController.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 24/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class SuccessViewController: BaseViewController
{

    weak var parent: BookingSummaryViewController!
    weak var parent1: DateTimeViewController!
    
    @IBAction func okayButtonAction(sender: AnyObject)
    {
        if let parent = self.parent {
        parent.output.successDoneButtonDidSelect()
        }
        else {
            self.parent1?.output.successDoneButtonDidSelect()
        }
    }
}

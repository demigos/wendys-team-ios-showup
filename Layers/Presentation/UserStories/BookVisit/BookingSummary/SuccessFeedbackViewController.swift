//
//  SuccessViewController.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 24/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class SuccessFeedbackViewController: BaseViewController {
    var titleText = "Thank you!"
    var summaryText = "Your payment was successful.\r\n\r\nSincerely,\r\nWendy’s Team"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleText
        summaryLabel.text = summaryText
    }

    weak var parent: PaidVisitViewController?
    weak var parent2: UnpaidVisitsViewController?

    @IBAction func okayButtonAction(sender: AnyObject) {
        if let parent = self.parent {
            if parent.nofeedback == true {
                parent.output.feedbackSendSuccess()
            }
            else {
                parent.output.successDoneButtonDidSelect()
            }
        }
        else {
            if let parent2 = self.parent2 {
                parent2.output.successDoneButtonDidSelect()
            }
        }
    }
}

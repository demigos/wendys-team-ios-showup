//
//  BookingSummaryInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol BookingSummaryInteractorInput: class
{
    func bookVisit(visit: VisitDomainModel)
}

//MARK: Output
protocol BookingSummaryInteractorOutput: class
{
    func visitDidCreate(visit: VisitDomainModel)
    func visitDidFailToCreate(error: WTError)
    func needsCard(visit: VisitDomainModel)
}

// MARK: - Interactor
final class BookingSummaryInteractor: BookingSummaryInteractorInput
{
    weak var output: BookingSummaryInteractorOutput!
    
    var visits : VisitsDSInterface
    var cards : CardsDSInterface
    
    // MARK: Setup
    
    init(visits: VisitsDSInterface, cards : CardsDSInterface)
    {
        self.visits = visits
        self.cards = cards
        self.setup()
    }
    
    func setup()
    {
        // setup observers
    }
    
    // MARK: Interface
    
    
    
    func bookVisit(visit: VisitDomainModel)
    {
        checkCards(visit)
    }
    
    func updateVisit(visit: VisitDomainModel)
    {
        visits.updateVisit(visit) { (result) in
            switch result {
            case .Success(let visit):
                self.output.visitDidCreate(visit)
            case .Failure(let error):
                self.output.visitDidFailToCreate(error)
            }
        }
    }
    
    
    // MARK: Private
    
    func checkCards(visit: VisitDomainModel)
    {
        cards.cardsListUpdate.observeResult { result in
            switch result {
            case .Success(let cards):
                if cards.count > 0 {
                    self.createOrUpdateVisit(visit)
                } else {
                    self.output.needsCard(visit)
                }
            case .Failure(let error):
                self.output?.visitDidFailToCreate(error)
            default: break
            }
        }
        
        cards.loadCards()
    }
    
    func createOrUpdateVisit(visit: VisitDomainModel)
    {
        if visit.id != 0 {
            updateVisit(visit)
        } else {
            visits.createVisit(visit, result: { (result) in
                switch result {
                    
                case .Success(let visit):
                    self.output?.visitDidCreate(visit)
                    
                case .Failure(let error):
                    self.output?.visitDidFailToCreate(error)
                }
            })
        }
    }
    
}
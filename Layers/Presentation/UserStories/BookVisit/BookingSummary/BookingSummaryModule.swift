//
//  BookingSummaryModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
// Used BookVisitStepModuleInput
// Used BookVisitStepModuleOutput

protocol BookingSummaryModuleOuput: class
{
    func bookingDidCancel()
    func servicesDidSelect(visit: VisitDomainModel)
    func dateTimeDidSelect(visit: VisitDomainModel)
    func contactInfoDidSelect(visit: VisitDomainModel)
    func visitDidCreate(visit: VisitDomainModel)
    func stepDidCancel(step: BookVisitFlowStep, visit: VisitDomainModel)
    func isFirstFlow() -> Bool
}

protocol BookingSummaryModuleInput: class
{
    func setup(output output: BookingSummaryModuleOuput, step: BookVisitFlowStep)
    func updateVisit(visit: VisitDomainModel)
}


// MARK: - Presenter
final class BookingSummaryPresenter: BookingSummaryModuleInput
    , BookingSummaryViewOutput
    , BookingSummaryInteractorOutput
    , AddCardModuleOutput
{
    weak var view: BookingSummaryViewInput!
    var interactor: BookingSummaryInteractorInput!
    weak var router: BookingSummaryRouter!
    weak var output: BookingSummaryModuleOuput?
    var resources: Dependencies!

    var step: BookVisitFlowStep!
    var visit: VisitDomainModel!

    func setup(output output: BookingSummaryModuleOuput, step: BookVisitFlowStep)
    {
        self.output = output
        self.step = step
    }

    func updateVisit(visit: VisitDomainModel)
    {
        self.visit = visit
    }


    // MARK: - Interactor Output
    func visitDidCreate(visit: VisitDomainModel)
    {
        self.visit = visit
        DispatchToMainQueue {
            self.router.presentSuccessDialog()
        }
    }

    func visitDidFailToCreate(error: WTError)
    {
        DispatchToMainQueue {
            self.view.waitMode(false)
            self.view.update(withError: error.errorDescription)
        }
    }

    func bookVisit(visit: VisitDomainModel) {
        self.interactor.bookVisit(visit)
    }

    func needsCard(visit: VisitDomainModel)
    {
        DispatchToMainQueue {
            self.router.viewController.waitMode(false)
            self.router.presentAddCardScreen()
        }
    }


    // MARK: - View Output
    func successDoneButtonDidSelect()
    {
        output?.visitDidCreate(visit)
    }

    func reloadTable() {
        view.fill(visit)
    }

    func viewDidLoad()
    {
        view.fill(visit)
    }

    func back()
    {
        //        output?.bookingDidCancel()
        output?.stepDidCancel(step, visit: visit)
    }

    func next(visit: VisitDomainModel)
    {
        DispatchToBackground {
            self.interactor.bookVisit(visit)
        }
    }

    func servicesDidSelect(visit: VisitDomainModel)
    {
        output?.servicesDidSelect(visit)
    }

    func dateTimeDidSelect(visit: VisitDomainModel)
    {
        output?.dateTimeDidSelect(visit)
    }

    func contactInfoDidSelect(visit: VisitDomainModel)
    {
        output?.contactInfoDidSelect(visit)
    }

    // MARK: Add card output
    func cardAddingDidCancel()
    {
        DispatchToMainQueue {
            self.router.dismissAddCardScreen()
        }
    }

    func cardDidAdd(card: PaymentCardDomainModel)
    {
        DispatchToMainQueue {
            if let output = self.output {
                if output.isFirstFlow() {
                    self.interactor.bookVisit(self.visit)
                    return
                }
            }
            DispatchToMainQueue {
            self.router.dismissAddCardScreen()
            }
        }
    }
}
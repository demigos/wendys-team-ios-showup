//
//  SelectServicesRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class SelectServicesRouter: VIPERRouter
{
    weak var view: SelectServicesViewController!
}
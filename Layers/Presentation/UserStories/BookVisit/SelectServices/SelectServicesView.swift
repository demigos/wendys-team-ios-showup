//
//  SelectServicesView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit


// MARK: - Interface

protocol SelectServicesViewInput: class {
    func fill(services: [CareServiceDomainModel])

}

protocol SelectServicesViewOutput: class {
    func viewDidLoad()

    func back()

    func next(services: [CareServiceDomainModel])
}

// MARK: - View Controller


final class SelectServicesViewController: BaseViewController
        , SelectServicesViewInput
        , Routable
        , ServiceGridDelegate {
    var output: SelectServicesViewOutput!
    typealias RouterType = SelectServicesRouter
    var router: RouterType!

    // MARK: Interface Builder
    @IBOutlet weak var serviceGrid: ServiceGrid!
    @IBOutlet weak var continueButton: UIButton!

    @IBAction func backButtonAction(sender: AnyObject) {
        output?.back()
    }

    override func viewDidAppear(animated: Bool) {
        self.serviceGrid.prepareView()
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Stopwatch"];
//        [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    }

    @IBAction func nextButtonAction(sender: AnyObject) {
        output.next(serviceGrid.selectedServices())
    }

    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }


    override func viewWillAppear(animated: Bool) {
//        self.serviceGrid.collectionView.hidden = true
    }


    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceGrid.delegate = self
        output.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        serviceGrid.updateLabelSizes()
    }

    // MARK: View Input

    func fill(services: [CareServiceDomainModel]) {
        continueButton.enabled = (services.count > 0)
        serviceGrid.setupServices(services)
    }

    //MARK: ServiceGridDelegate
    func servicesSetDidChanged(services: [CareServiceDomainModel]) {
        continueButton.enabled = (services.count > 0)
    }

}
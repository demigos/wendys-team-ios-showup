//
//  SelectServicesInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol SelectServicesInteractorInput: class
{

}

//MARK: Output
protocol SelectServicesInteractorOutput: class
{

}

// MARK: - Interactor
final class SelectServicesInteractor: SelectServicesInteractorInput
{
    weak var output: SelectServicesInteractorOutput!
}
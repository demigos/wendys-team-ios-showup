//
//  SelectServicesAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class SelectServicesAssembly
{
    class func createModule(configure: (module: BookVisitStepModuleInput) -> Void) -> SelectServicesViewController
    {
        let vc = R.storyboard.bookVisit.selectServicesViewController()!
        let interactor = SelectServicesInteractor()
        let presenter = SelectServicesPresenter()
        let router = SelectServicesRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc

        return vc
    }
}
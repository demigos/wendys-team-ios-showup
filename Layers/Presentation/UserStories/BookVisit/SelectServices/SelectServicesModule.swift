//
//  SelectServicesModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 23/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
// Used BookVisitStepModuleInput
// Used BookVisitStepModuleOutput

// MARK: - Presenter
final class SelectServicesPresenter: BookVisitStepModuleInput
    , SelectServicesViewOutput
    , SelectServicesInteractorOutput
{
    weak var view: SelectServicesViewInput!
    var interactor: SelectServicesInteractorInput!
	weak var router: SelectServicesRouter!
    weak var output: BookVisitStepModuleOutput?
    
    var step: BookVisitFlowStep!
    var visit: VisitDomainModel!

    func setup(output output: BookVisitStepModuleOutput, step: BookVisitFlowStep)
    {
        self.output = output
        self.step = step
    }
    
    func updateVisit(visit: VisitDomainModel)
    {
        self.visit = visit
    }

    // MARK: - Interactor Output

    // MARK: - View Output
    func viewDidLoad()
    {
        view.fill(visit.services)
    }
    
    func back()
    {
        output?.stepDidCancel(step, visit: visit)
    }
    
    func next(services: [CareServiceDomainModel])
    {
        visit.services = services
        output?.stepDidSuccess(step, visit: visit)
    }
}
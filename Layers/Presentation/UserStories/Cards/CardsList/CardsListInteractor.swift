//
//  CardsListInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol CardsListInteractorInput: class
{
    func loadCards()
    func deleteCard(card: PaymentCardDomainModel)
    func setCardPrimary(card: PaymentCardDomainModel)
}

//MARK: Output
protocol CardsListInteractorOutput: class
{
    func cardsDidLoad(cards: [PaymentCardDomainModel])
    func cardDidFailedToLoadWithError(error: String)

    func cardDidFailToDelete(card: PaymentCardDomainModel)
    func cardDidDelete(card: PaymentCardDomainModel)

    func cardDidSetPrimary(card: PaymentCardDomainModel?)
}

// MARK: - Interactor
final class CardsListInteractor: CardsListInteractorInput
{
    weak var output: CardsListInteractorOutput!
    private var cardsDS: CardsDSInterface!

    init(cardsDS: CardsDSInterface!)
    {
        self.cardsDS = cardsDS
        setup()
    }
    
    func reloadCards() {
        cardsDS.cardsListUpdate.observeResult { result in
            switch result {
            case .Success(let cards):
                let sorted = cards.sort({ (c1, c2) -> Bool in
                    c1.last4 < c2.last4
                })
                self.output?.cardsDidLoad(sorted)
            case .Failure(let error):
                self.output?.cardDidFailedToLoadWithError(error.errorDescription)
            }
        }
    }

    func setup()
    {
        reloadCards()

        cardsDS.primaryCardUpdate.observeResult { result in
            switch result {
            case .Success(let card):
                self.output?.cardDidSetPrimary(card)
            case .Failure(let error):
                print(error)
            }

        }
    }

    // MARK: Input
    func loadCards()
    {
        setup()
        cardsDS.loadCards()
    }

    func deleteCard(card: PaymentCardDomainModel)
    {
        setup()
        if let id = card.id {
            cardsDS.removeCard(id, result: {
                (result) in switch result {

                case .Success(let card):
                    self.output?.cardDidDelete(card)

                case .Failure(_):
                    self.output?.cardDidFailToDelete(card)

                }
            })
        }

    }

    func setCardPrimary(card: PaymentCardDomainModel)
    {
        setup()
        if let id = card.id {
            cardsDS.makeCardPrimary(id, result: {
                (result) in switch result {

                case .Success(let card):
                    self.output?.cardDidSetPrimary(card)

                case .Failure(_):
                    print("failure")
                default: break
                }
            })
        }
    }

}
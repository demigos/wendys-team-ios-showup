//
//  CardsListModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol CardsListModuleInput
{
    func setupDelegate(output: CardsListModuleOutput)
    func setupNavigation(nc: UINavigationController)
    func refreshList()
}

//MARK: Output
protocol CardsListModuleOutput: class
{
    func cardsFramePaddingDidChange(to padding:CGFloat)
}


// MARK: - Presenter
final class CardsListPresenter: CardsListModuleInput
    , CardsListViewOutput
    , CardsListInteractorOutput
{
    weak var view: CardsListViewInput!
    var interactor: CardsListInteractorInput!
    weak var router: CardsListRouter!
    weak var output: CardsListModuleOutput?

    var loadFailed = false

    init() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                selector: #selector(refreshFailedList),
                name: NetworkService.networkReachable,
                object: nil)
    }

    func mustUpdate() {
        self.interactor.loadCards()
    }

    func refreshList() {
        loadFailed = false
        DispatchToMainQueue {
            self.view.waitMode(true)
        }

        DispatchToBackground {
            self.interactor.loadCards()
        }
    }

    @objc func refreshFailedList() {
        if (loadFailed) {
            refreshList()
        }
    }


    func setupDelegate(output: CardsListModuleOutput)
    {
        self.output = output
    }

    func setupNavigation(nc: UINavigationController)
    {
        router.navigationController = nc
    }

    // MARK: - Interactor Output
    func cardsDidLoad(cards: [PaymentCardDomainModel])
    {
        loadFailed = false
        DispatchToMainQueue {
            self.view.waitMode(false)
            self.view.update(withCards: cards)
        }
    }

    func cardDidFailedToLoadWithError(error: String){
        loadFailed = true
    }

    func cardDidFailToDelete(card: PaymentCardDomainModel)
    {
        DispatchToBackground {
            self.interactor.loadCards()
        }
    }

    func cardDidDelete(card: PaymentCardDomainModel)
    {
        DispatchToBackground {
            self.interactor.loadCards()
        }
    }

    func cardDidSetPrimary(card: PaymentCardDomainModel?)
    {

        DispatchToMainQueue {
            self.view.waitMode(false)
            self.view.updatePrimaryCard(card)
        }
    }


    // MARK: - View Output
    func framePaddingDidChange(to padding: CGFloat)
    {
        output?.cardsFramePaddingDidChange(to: padding)
    }
    func cardDidSelect(model: PaymentCardDomainModel)
    {
        DispatchToBackground {
            self.interactor.setCardPrimary(model)
        }
    }

    func addCardButtonDidSelect()
    {
        DispatchToMainQueue {
            self.router?.presentAddCard()
        }
    }


    func userWantsToDeleteCard(card: PaymentCardDomainModel)
    {
        view.waitMode(true)
        DispatchToBackground {
            self.interactor.deleteCard(card)
        }
    }
}
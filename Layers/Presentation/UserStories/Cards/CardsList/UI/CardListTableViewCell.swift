//
//  CardListTableViewCell.swift
//  WendysTeamIOS
//
//  Created by Alexey on 21/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

protocol CardListCellDelegate: class
{
    func cardDeleteTouched(cell: CardListTableViewCell)
}

class CardListTableViewCell: UITableViewCell
{    
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardIsPrimaryCheckImage: TintedImageView!
    @IBOutlet weak var usedNowLabel: UILabel!
    @IBOutlet weak var validThruLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var deleteShadow: UIImageView!
    @IBOutlet weak var confirmDeleteButton: UIButton!
    @IBOutlet weak var cardView: UIView!
    
    weak var delegate: CardListCellDelegate?
    
    
    func fill(withModel model:PaymentCardDomainModel) {
        customerNameLabel.text = model.cardholderName?.uppercaseString
        
        if let last = model.last4 {
            cardNumber.text = "XXXX XXXX XXXX \(last)"
            if let cardEmiter = model.cardEmitter {
                if cardEmiter == "diners club" {
                    cardNumber.text = "XXXX XXXX XX\(last.substringToIndex(last.startIndex.advancedBy(2))) \(last.substringFromIndex(last.endIndex.advancedBy(-2)))"
                }
                if cardEmiter == "american express" {
                    cardNumber.text = "XXXX XXXXXX \(last)"
                }
            }
        } else {
            cardNumber.text = "-"
        }
        
        if let primary = model.isPrimary where primary == true {
            cardIsPrimaryCheckImage.hidden = false
            validThruLabel.text = ""
            usedNowLabel.text = "Used Now"
        } else {
            cardIsPrimaryCheckImage.hidden = true
            usedNowLabel.text = ""
            validThruLabel.text = ""
            if let year = model.expirationYear, let month = model.expirationMonth {
                validThruLabel.text = "VALID THRU: \(month)/\(year % 100)"
            }
            else {
                validThruLabel.text = ""
            }
            
        }
        
        if let emitter = model.cardEmitter, let image = UIImage(named: emitter)  {
            cardImage.image = image
            cardImage.hidden = false
        } else {
            cardImage.hidden = true
        }
        
        setNeedsDisplay()
    }
    
    @IBAction func deleteTouched(sender: UIButton)
    {
        UIView.animateWithDuration(0.5, animations: {
            self.cardView.frame.origin.x = -self.confirmDeleteButton.frame.width
            self.cardView.frame.size.width += self.confirmDeleteButton.frame.width
        })
    }
    
    @IBAction func confirmDeleteTouched(sender: AnyObject)
    {
        delegate?.cardDeleteTouched(self)
    }
    
    override func willTransitionToState(state: UITableViewCellStateMask)
    {
        super.willTransitionToState(state)
        
        if state.contains(.ShowingEditControlMask) {
            deleteButton.hidden = false
            deleteShadow.hidden = false
        } else {
            deleteButton.hidden = true
            deleteShadow.hidden = true
            
            if cardView.frame.origin.x != 0 {
                self.cardView.frame.origin.x = 0
                self.cardView.frame.size.width -= self.confirmDeleteButton.frame.width
            }
            
        }
    }
}

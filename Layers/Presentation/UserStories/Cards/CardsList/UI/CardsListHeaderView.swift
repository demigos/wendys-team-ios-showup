//
//  CardsListHeaderView.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 28.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

protocol CardsListHeaderViewDelegate: class {

    func cardsListViewEditModeChanged(editMode: Bool)

}

class CardsListHeaderView: UIView {

    var view : UIView!
    weak var tableView : UITableView? {
        didSet {
            updateButtonTitle()
        }
    }
    weak var delegate: CardsListHeaderViewDelegate?

    @IBOutlet weak var manageButton: UIButton!

    @IBAction func manageButtonAction() {
        if let tableView = tableView {
            tableView.setEditing(!tableView.editing, animated: false)
            delegate?.cardsListViewEditModeChanged(tableView.editing)
            updateButtonTitle()
        }
    }
    
    func updateManageButtonEnabled(itemsCount: Int = 0) {
        manageButton.enabled = itemsCount > 1
    }

    func updateButtonTitle() {
        if let tableView = tableView where tableView.editing {
            manageButton.setTitle("Done", forState: .Normal)
        } else {
            manageButton.setTitle("Manage", forState: .Normal)
        }
    }

    func xibSetup() {
        view = R.nib.cardsListHeaderView.firstView(owner: self)
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]

        addSubview(view)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }


}

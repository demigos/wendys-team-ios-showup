//
//  CardsListRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

protocol CardsListRouterDelegate: class {
    func cardsMustBeUpdated()
}


// MARK: - Router
final class CardsListRouter: VIPERRouter, CardsListRouterDelegate
{
    weak var view: CardsListViewController!
    var resources: Dependencies!

    func cardsMustBeUpdated() {
        view.mustUpdate()
    }

    func presentAddCard()
    {
        let vc = AddCardAssembly.createModule(resources) { (module) in
        }
        vc.cardsListDelegate = self
        vc.present(inNavigation: navigationController!)
    }
}
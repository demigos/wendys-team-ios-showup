//
//  CardsListAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class CardsListAssembly
{
    class func createModule(resources: Dependencies, configure: (module: CardsListModuleInput) -> Void) -> CardsListViewController
    {
        let vc = R.storyboard.cards.cardsListViewController()!
        let interactor = CardsListInteractor(cardsDS: resources.datasources.cards)
        let presenter = CardsListPresenter()
        let router = CardsListRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc
        router.resources = resources

        return vc
    }
}
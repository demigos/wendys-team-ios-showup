//
//  CardsListView.swift
//  wendysteam-ios
//
//  Created by Alexey on 21/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol CardsListViewInput: BaseViewControllerInput {
    func update(withCards cards: [PaymentCardDomainModel])
    
    func updatePrimaryCard(card: PaymentCardDomainModel?)
}

protocol CardsListViewOutput: class {

    func mustUpdate()

    func framePaddingDidChange(to padding: CGFloat)
    
    func cardDidSelect(model: PaymentCardDomainModel)
    
    func addCardButtonDidSelect()
    
    func userWantsToDeleteCard(card: PaymentCardDomainModel)
}

// MARK: - View Controller

final class CardsListViewController: BaseViewController
    , CardsListViewInput
    , Routable
    , UITableViewDataSource
    , UITableViewDelegate
    , CardListCellDelegate
, CardsListHeaderViewDelegate {
    var output: CardsListViewOutput!
    typealias RouterType = CardsListRouter
    var router: RouterType!
    
    
    var openedFromPaidView = false
    var cards: [PaymentCardDomainModel]?
    var primaryCard: PaymentCardDomainModel?
    
    var topOffset = CGFloat(0)
    
    var phHeight = CGFloat(0)
    var rowHeight = CGFloat(80)
    let dotLineHeight = CGFloat(10)
    
    enum State: Int {
        case Collapsed = 0
        case Extended = 1
    }
    
    var state: State = .Collapsed {
        didSet {
            applyState()
        }
    }
    
    // Header
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    
    // Bottom panel
    @IBOutlet weak var bottomPanel: UIView!
    @IBOutlet weak var bottomPanelToTableViewPadding: NSLayoutConstraint!
    @IBOutlet weak var bottomPanelToViewPadding: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCardButton: RoundedButton!
    
    // Primary card
    @IBOutlet weak var primaryCardHeight: NSLayoutConstraint!
    @IBOutlet weak var primaryCardImage: UIImageView!
    @IBOutlet weak var primaryCardNumber: UILabel!
    @IBOutlet weak var primaryCardName: UILabel!
    @IBOutlet weak var validThruLabel: UILabel!
    @IBOutlet weak var headerArrow: UIImageView!
    
    // Table admin panel
    @IBOutlet weak var tableAdminPanel: CardsListHeaderView!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()


        phHeight = headerHeight.constant
        rowHeight = primaryCardHeight.constant
        tableView.dataSource = self
        tableView.delegate = self
        clearPrimaryCard()
        tableAdminPanel.tableView = tableView
        tableAdminPanel.delegate = self
        
    }

    func mustUpdate() {
        self.output.mustUpdate()
    }
    
    override func viewDidLayoutSubviews() {
        applyState()
    }

    override func viewDidAppear(animated: Bool) {
        //must not call basecontroller viewdidappear now
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        topOffset = CGRectGetMinY(view.frame)
        roundCorners()
        applyState()
    }
    
    
    // MARK: View Input
    func update(withCards cards: [PaymentCardDomainModel]) {
        self.cards = cards
        applyState()
        tableView.reloadData()
        applyState()
        tableAdminPanel.updateManageButtonEnabled(cards.count)
    }
    
    func updatePrimaryCard(card: PaymentCardDomainModel?) {
        if let card = card {
            primaryCard = card
            
            primaryCardName.text = card.cardholderName?.uppercaseString
            if let last = card.last4 {
                primaryCardNumber.text = "XXXX XXXX XXXX \(last)"
                if let cardEmiter = card.cardEmitter {
                    if cardEmiter == "diners club" {
                        primaryCardNumber.text = "XXXX XXXX XX\(last.substringToIndex(last.startIndex.advancedBy(2))) \(last.substringFromIndex(last.endIndex.advancedBy(-2)))"
                    }
                    if cardEmiter == "american express" {
                        primaryCardNumber.text = "XXXX XXXXXX \(last)"
                    }
                }
            } else {
                primaryCardNumber.text = "-"
            }
            
            if let year = card.expirationYear, let month = card.expirationMonth {
                validThruLabel.text = "VALID THRU: \(month)/\(year % 100)"
            }
            else {
                validThruLabel.text = ""
            }
            
            if let emitter = card.cardEmitter, let image = UIImage(named: emitter) {
                primaryCardImage.image = image
                primaryCardImage.hidden = false
            } else {
                primaryCardImage.hidden = true
            }
            
            showPrimaryCard()
        } else {
            hidePrimaryCard()
        }
        
        view.needsUpdateConstraints()
        
    }
    
    
    // MARK: Actions
    @IBAction func panAction(sender: UIPanGestureRecognizer) {
        if self.hasData() && sender.translationInView(view).y < 0 && state == .Collapsed {
            state = .Extended
        }
        
        if sender.translationInView(view).y > 0 && state != .Collapsed {
            state = .Collapsed
        }
    }
    
    @IBAction func addCardAction(sender: AnyObject) {
        output.addCardButtonDidSelect()
    }
    
    // MARK: Private
    private func clearPrimaryCard() {
        primaryCardImage.image = nil
        primaryCardName.text = ""
        primaryCardNumber.text = ""
    }
    
    private func showPrimaryCard() {
        
    }
    
    private func hidePrimaryCard() {
    }
    
    private func applyState() {
        var offset = CGFloat(0)
        
        if state == .Collapsed {
            
            if hasData() {
                if !openedFromPaidView {
                    bottomPanelToTableViewPadding.priority = 999
                    bottomPanelToViewPadding.priority = 1
                }
//                if primaryCard != nil {
                    primaryCardHeight.constant = rowHeight
//                } else {
//                    primaryCardHeight.constant = 0
//                }
                tableAdminPanel.hidden = false
                headerHeight.constant = phHeight
                tableView.hidden = false
                view.setNeedsUpdateConstraints()
                tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                tableView.scrollEnabled = false
                offset = rowHeight + headerHeight.constant - dotLineHeight - 2
                headerArrow.image = R.image.arrow()
            } else {
                tableAdminPanel.hidden = true
                primaryCardHeight.constant = 0
                headerHeight.constant = 0
                tableView.hidden = true
                if !openedFromPaidView {
                    bottomPanelToViewPadding.priority = 999
                    bottomPanelToTableViewPadding.priority = 1
                }
                view.setNeedsUpdateConstraints()
                offset = CGRectGetHeight(bottomPanel.frame) + 8 // top guide
            }
            
        } else {
            if let pvcv = parentViewController?.view {
                offset = CGRectGetHeight(pvcv.bounds) - topOffset
                tableView.scrollEnabled = true
            }
            
            headerArrow.image = R.image.arrowReversed()
            
        }
        moveTop(offset)
    }
    
    private func moveTop(offset: CGFloat) {
        output.framePaddingDidChange(to: offset)
        var newFrame = CGRectZero
        if let pvcv = parentViewController?.view {
            
            var top = CGRectGetMaxY(pvcv.bounds) - offset
            if top < CGRectGetMinY(pvcv.bounds) {
                top = CGRectGetMinY(pvcv.bounds)
            }
            if (self.openedFromPaidView && top == 0) {
                top = 120
            }
            newFrame = CGRectMake(CGRectGetMinY(pvcv.bounds),
                                  top,
                                  CGRectGetWidth(pvcv.bounds),
                                  CGRectGetHeight(pvcv.bounds)
            )
            var animDuration = 0.2
            if self.openedFromPaidView && self.cards == nil {
                animDuration = 0.0
            }
            UIView.animateWithDuration(animDuration, delay: 0, options: .CurveEaseInOut, animations: {
                self.view.frame = newFrame
                self.view.needsUpdateConstraints()
                self.tableView.flashScrollIndicators()
                }, completion: {
                    (_) in
                    
            })
        }
    }
    
    
    private func roundCorners() {
        let f = view.frame
        let maskFrame = CGRectMake(0, 0, f.size.width, f.size.height)
        let path = UIBezierPath(roundedRect: maskFrame, byRoundingCorners: [.TopLeft, .TopRight], cornerRadii: CGSizeMake(16.0, 16.0)).CGPath
        let mask = CAShapeLayer()
        mask.frame = maskFrame
        mask.path = path
        view.layer.mask = mask
        
    }
    
    // MARK: Data Source
    
    func hasData() -> Bool {
        return (cards?.count > 0)
    }
    
    func tryToDeleteCell(indexPath: NSIndexPath) {
        
        if let card = cardForIndexpath(indexPath) {
            output?.userWantsToDeleteCard(card)
            cards?.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
            let count = cards?.count ?? 0
            tableAdminPanel.updateManageButtonEnabled(count)
            if count < 2 {
                tableAdminPanel.manageButtonAction()
            }
        }
    }
    
    
    func cardForIndexpath(indexPath: NSIndexPath) -> PaymentCardDomainModel? {
        let row = indexPath.row
        if let data = cards where row < data.count {
            let card = data[row]
            return card
        }
        
        return nil
    }
    
    // MARK: TableView Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let data = cards {
            return data.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.cardListTableCell.identifier) as! CardListTableViewCell
        
        cell.delegate = self
        
        let row = indexPath.row
        if let data = cards where row < data.count {
            let cellModel = data[row]
            cell.fill(withModel: cellModel)
            if (row == data.count - 1) {
                cell.separatorInset = UIEdgeInsetsMake(0.0, cell.bounds.size.width * 5, 0.0, 0.0);
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return clearView()
    }
    
    func clearView() -> UIView {
        let frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), CGFloat.min)
        let view = UIView(frame: frame)
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .None
    }
    
    func tableView(tableView: UITableView, shouldIndentWhileEditingRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    //
    // MARK: TableView Delegate
    //
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let targetOffset = targetContentOffset.memory
        if targetOffset.y == 0 && scrollView.contentOffset.y <= 0 {
            state = .Collapsed
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let row = indexPath.row
        if let data = cards where row < data.count {
            let cellModel = data[row]
            output?.cardDidSelect(cellModel)
        }
    }
    
    // MARK: CardListCellDelegate
    func cardDeleteTouched(cell: CardListTableViewCell) {
        if let indexPath = tableView.indexPathForCell(cell) {
            tryToDeleteCell(indexPath)
        }
    }
    
    func cardsListViewEditModeChanged(editMode: Bool) {
        bottomPanel.hidden = editMode
        bottomPanelToTableViewPadding.constant = editMode ? -bottomPanel.frame.height : 0
    }
    
}
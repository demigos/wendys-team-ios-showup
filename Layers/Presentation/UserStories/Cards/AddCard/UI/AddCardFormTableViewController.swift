//
//  AddCardFormTableViewController.swift
//  WendysTeamIOS
//
//  Created by Alexey on 22/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class AddCardFormTableViewController: UITableViewController
    , Routable
    , DateTextFieldOutput
{
    typealias RouterType = AddCardRouter
    var router: AddCardRouter!

    @IBOutlet weak var cardHolderNameField: UITextField!
    @IBOutlet weak var cardNumberField: MaskTextField!

    @IBOutlet weak var titleCell: UITableViewCell!
    @IBOutlet weak var expYear: DateTextField!
    var expDateYear: String?
    var expDateMonth: String?

    @IBOutlet weak var securityCodeField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        cardNumberField.setCreditCard()

        titleCell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);

        expYear.output = self
        securityCodeField.addTarget(self, action: #selector(securityCodeChanged), forControlEvents: .EditingChanged)
    }

    func securityCodeChanged(textField: UITextField)
    {
        if textField.text?.characters.count > 4 {
            textField.deleteBackward()
        }
    }

    func clear()
    {
        cardHolderNameField.text = ""
        cardNumberField.text = ""
        expYear.text = ""
        securityCodeField.text = ""
    }

    // MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 1))
        let colorView = UIView(frame: CGRectMake(tableView.separatorInset.left, 0, tableView.frame.size.width, 0.5))

        colorView.backgroundColor = Colors.tableSeparator
        footer.addSubview(colorView)

        return footer
    }

    // MARK: DateTextFieldOutput
    func didSelectDate(date: NSDate)
    {
        let formatterYear = NSDateFormatter()
        let formatterMonth = NSDateFormatter()

        formatterYear.dateFormat = "y"

        formatterMonth.dateFormat = "MM"
        expDateYear = formatterYear.stringFromDate(date)
        expDateMonth = formatterMonth.stringFromDate(date)
        expYear.text = formatterYear.stringFromDate(date) + "/" + formatterMonth.stringFromDate(date)
    }
}

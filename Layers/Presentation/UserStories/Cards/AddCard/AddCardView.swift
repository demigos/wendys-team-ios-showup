//
//  AddCardView.swift
//  wendysteam-ios
//
//  Created by Alexey on 22/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol AddCardViewInput: BaseViewControllerInput {
    func clearForm()
}

protocol AddCardViewOutput: class {
    func backButtonDidSelect()
    
    func phoneButtonDidSelect()
    
    func continueButtonDidSelectWithCard(card: PaymentCardDomainModel)
}

// MARK: - View Controller

final class AddCardViewController: BaseViewController
    , AddCardViewInput
, Routable, UITextFieldDelegate {
    var output: AddCardViewOutput!
    typealias RouterType = AddCardRouter
    var router: RouterType!
    var cardsListDelegate: CardsListRouterDelegate?
    
    var addCardForm: AddCardFormTableViewController!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!
    @IBOutlet weak var continueButton: UIButton!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        router.presentForm()
        continueButton.enabled = false
        addCardForm!.cardHolderNameField.addTarget(self, action: #selector(textFieldTextChanged), forControlEvents: UIControlEvents.EditingChanged)
        addCardForm!.cardNumberField.addTarget(self, action: #selector(textFieldTextChanged), forControlEvents: UIControlEvents.EditingChanged)
        addCardForm!.expYear.addTarget(self, action: #selector(textFieldTextChanged), forControlEvents: UIControlEvents.EditingChanged)
        addCardForm!.securityCodeField.addTarget(self, action: #selector(textFieldTextChanged), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    // MARK: View Input
    func clearForm() {
        addCardForm.clear()
    }
    
    
    // MARK: Actions
    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }
    
    @IBAction func backButtonAction(sender: AnyObject) {
        cardsListDelegate?.cardsMustBeUpdated()
        output.backButtonDidSelect()
        view.endEditing(true)
    }
    
    func textFieldTextChanged() {
        var fieldsEmpty = false
        if addCardForm!.cardHolderNameField.text?.characters.count < 1 {
            fieldsEmpty = true
        }
        if addCardForm!.cardNumberField.text?.characters.count < 1 {
            fieldsEmpty = true
        }
        if addCardForm!.expYear.text?.characters.count < 1 {
            fieldsEmpty = true
        }
        if addCardForm!.securityCodeField.text?.characters.count < 1 {
            fieldsEmpty = true
        }
        self.continueButton.enabled = !fieldsEmpty
    }
    
    @IBAction func continueAction(sender: AnyObject) {
        var card = PaymentCardDomainModel()
        
        card.cardholderName = addCardForm!.cardHolderNameField.text
        card.cvc = addCardForm.securityCodeField.text
        if let text = addCardForm.expDateYear, let year = UInt(text) {
            card.expirationYear = year
        }
        
        if let text = addCardForm.expDateMonth, let month = UInt(text) {
            card.expirationMonth = month
        }
        
        if let number = addCardForm.cardNumberField.text {
            card.number = number
        }
        
        output.continueButtonDidSelectWithCard(card)
        view.endEditing(true)
    }
    
    //MARK: - Keyboard handling
    //
    override func keyboardWillShow(kb: KeyboardParameters) {
        let height = CGRectGetHeight(kb.frameEnd)
        
        UIView.animateWithDuration(kb.animationDuration, delay: 0, options: kb.animationCurve, animations: {
            self.bottomPadding.constant = height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }) {
            (_) in }
    }
    
    override func keyboardWillHide(kb: KeyboardParameters) {
        UIView.animateWithDuration(kb.animationDuration, delay: 0, options: kb.animationCurve, animations: {
            self.bottomPadding.constant = 0
        }) {
            (_) in
            //
        }
    }
    
}
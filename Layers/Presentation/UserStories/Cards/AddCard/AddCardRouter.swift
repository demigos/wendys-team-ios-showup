//
//  AddCardRouter.swift
//  wendysteam-ios
//
//  Created by Alexey on 22/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class AddCardRouter: VIPERRouter
{
    weak var view: AddCardViewController!
    
    func presentForm()
    {
        view.addCardForm.presentChild(from: view, using: view.containerView)
    }
}
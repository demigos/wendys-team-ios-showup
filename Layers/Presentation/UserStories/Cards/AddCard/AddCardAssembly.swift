//
//  AddCardAssembly.swift
//  wendysteam-ios
//
//  Created by Alexey on 22/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class AddCardAssembly
{
    class func createModule(resources: Dependencies, configure: (module: AddCardModuleInput) -> Void) -> AddCardViewController
    {
        let vc = R.storyboard.cards.addCardViewController()!
        let interactor = AddCardInteractor(cardsDS: resources.datasources.cards)
        let presenter = AddCardPresenter()
        let router = AddCardRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.view = vc
        
        vc.addCardForm = R.storyboard.cards.addCardFormTableViewController()!
        vc.addCardForm.router = router

        return vc
    }
}
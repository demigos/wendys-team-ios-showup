//
//  AddCardInteractor.swift
//  wendysteam-ios
//
//  Created by Alexey on 22/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol AddCardInteractorInput: class
{
    func addCard(card: PaymentCardDomainModel)
}

//MARK: Output
protocol AddCardInteractorOutput: class
{
    func cardDidAdd(card: PaymentCardDomainModel)
    func errorDidHappend(error: WTError)
}

// MARK: - Interactor
final class AddCardInteractor: AddCardInteractorInput
{
    weak var output: AddCardInteractorOutput!
    private var cardsDS: CardsDSInterface!
    
    init(cardsDS: CardsDSInterface!)
    {
        self.cardsDS = cardsDS
    }
    
    func addCard(card: PaymentCardDomainModel)
    {
        cardsDS.createCard(card, result: { (result) in
            switch result {
                
            case .Success(let card):
                self.output.cardDidAdd(card)
            case .Failure(let error):
                self.output.errorDidHappend(error)
                break
                
            }
        })
    }
}
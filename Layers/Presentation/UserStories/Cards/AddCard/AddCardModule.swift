//
//  AddCardModule.swift
//  wendysteam-ios
//
//  Created by Alexey on 22/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol AddCardModuleInput
{
    func setupDelegate(output: AddCardModuleOutput)
}

//MARK: Output
protocol AddCardModuleOutput: class
{
    func cardDidAdd(card: PaymentCardDomainModel)
    func cardAddingDidCancel()
}


// MARK: - Presenter
final class AddCardPresenter: AddCardModuleInput
    , AddCardViewOutput
    , AddCardInteractorOutput
{
    weak var view: AddCardViewInput!
    var interactor: AddCardInteractorInput!
    weak var router: AddCardRouter!
    weak var output: AddCardModuleOutput?

    func setupDelegate(output: AddCardModuleOutput)
    {
        self.output = output
    }


    // MARK: - Interactor Output
    func cardDidAdd(card: PaymentCardDomainModel) {
        DispatchToMainQueue {
            self.view.waitMode(false)
            if self.output == nil {
                self.router.view.backButtonAction(self)
            }
            else {
                self.output?.cardDidAdd(card)
            }
        }
    }

    func errorDidHappend(error: WTError) {
        DispatchToMainQueue {
            self.view.waitMode(false)
            self.view.showError(error.errorDescription)
        }
    }

    // MARK: - View Output
    func backButtonDidSelect()
    {
        DispatchToMainQueue {
            self.view.waitMode(false)
//            self.output?.cardAddingDidCancel()
            self.router.popCurrentScreen()
        }
    }

    func phoneButtonDidSelect()
    {
        PhoneService.call()
    }

    func continueButtonDidSelectWithCard(card: PaymentCardDomainModel)
    {
        view.waitMode(true)
        DispatchToBackground {
            self.interactor.addCard(card)
        }
    }
}
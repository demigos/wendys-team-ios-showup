import Foundation

class TermsViewController : UIViewController, TTTAttributedLabelDelegate {
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var mainLabel: TTTAttributedLabel!
    @IBOutlet var scrollView: UIScrollView!
    var terms = true
    
    @IBAction func backButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        scrollView.contentSize = CGSizeMake(self.view.frame.width, scrollView.contentSize.height)
        if terms {
            navBar.topItem?.title = "TERMS OF SERVICE"
            
            let termsText = "Disclaimer, Terms of Use & Legal Statement\r\n\r\nPrivacy Policy\r\nFor the privacy of our clients, Wendy’s Team, Inc does not share or sell any information about the company’s clients and their families. To include visit details, personal and contact information, emails or other communications.\r\n\r\nSubmitting a Visit Request\r\nClients and their authorized representative may submit visit requests by calling (719)425-9572, placing an online request at www.wendys-team.com or through the iphone app. Submission and receipt of the visit request by Wendy’s Team is not a guarantee of service which is subject to Team Member availability.\r\n\r\nServices  \r\nWendy’s Team, Inc is a Homecare Placement Agency operating in accordance with the provisions of C.R.S. 25-27.5-103 set forth by the Colorado Department of Public Health and Environment, and thus provides only referrals of providers to home care consumers seeking services. Wendy’s Team Members provide non-medical care under the status of independent contractors.\r\n\r\nNot a Skilled or Medical Home Health Service\r\nOur Team Members are not licensed clinically trained medical professionals. We do not provide nursing, therapy or certified nursing aide services that require the supervision of a licensed or certified health care professional.\r\n\r\nCancellation\r\nThere is no penalty for cancellations up to one hour prior to the start time of the visit. Cancellations within one hour of the start time are subject to a charge of the one-hour minimum rate.\r\n\r\nWendy’s Team, Inc reserves the right to refuse or cancel any order.\r\nWe reserve the right, to accept or decline your visit request or limit the services delivered for any reason including delinquent payments. Payments may be subject to the approval of a financial institution and Wendy’s Team shall not be liable in any way if such financial institution refuses to accept the payment for any reason. We may require, at our dsicretion, that any visit request receive pre-approval. We may also require additional verification or information before accepting a visit request.\r\n\r\nPayments  \r\nPayments can be made by credit/debit card, personal check or cash. The Client Portal accessed through the Wendy’s Team website enables clients to book and pay for visits online with ease. We accept payments by VISA, MasterCard and American Express. Wendy’s Team is not a financial service provider and does not keep any information with respect to your credit card, financial situation, or your credit history.\r\n\r\nAgreement to Terms and Conditions\r\nIf you do not agree to these Terms and Conditions, do not place a visit request, or submit information to Wendy’s Team, Inc.\r\n\r\nAll questions concerning this Agreement should be directed to: info@wendys-team.com.\r\n\r\nWe may update these terms and conditions at any time and without notice. The latest version of the terms and conditions is available on our website -  http://www.wendys-team.com."
            let finalString = NSMutableAttributedString(string: termsText, attributes: nil)
            let overallRange: NSRange = (termsText as NSString).rangeOfString(termsText)
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica", size: 15.0)!, range: overallRange)
            var range1: NSRange = (termsText as NSString).rangeOfString("Disclaimer, Terms of Use & Legal Statement")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Privacy Policy")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Submitting a Visit Request")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Services  ")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Not a Skilled or Medical Home Health Service")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Cancellation")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Wendy’s Team, Inc reserves the right to refuse or cancel any order.")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Payments  ")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("Agreement to Terms and Conditions")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            range1 = (termsText as NSString).rangeOfString("www.wendys-team.com")
            finalString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 13 / 255, green: 122 / 255, blue: 255 / 255, alpha: 1.0), range: range1)
            
            mainLabel.attributedText = finalString
             mainLabel.delegate = self
             mainLabel.activeLinkAttributes = nil
             mainLabel.inactiveLinkAttributes = nil
             mainLabel.linkAttributes = nil
             mainLabel.addLinkToURL(NSURL(string: "termsURL"), withRange: range1)
            
        }
        else {
            navBar.topItem?.title = "DATA POLICY"
            
            let termsText = "WENDY’S TEAM, INC - DATA PRIVACY POLICY & STATEMENT\r\n\r\nLast updated: October 7th, 2016\r\n\r\nWendy’s Team operates http://www.wendys-team.com. This document serves to inform you of our policies regarding the collection, use and disclosure of personal information we receive from users of the website. We use your personal information only for providing our company’s services, namely coordinating and processing payments for clients to receive non-medical home services. By using the website, you agree to the collection and use of information in accordance with this policy.\r\n\r\nSECTION 1 - INFORMATION COLLECTION\r\n\r\nWhile using our website, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name, address, phone number and email contact. This information could be for yourself or another individual for whom you are paying. Information we may ask for could include payment information such as debit card numbers or bank account information. Provision of any of the requested, is at your voluntary discretion.\r\n\r\nEmail marketing: With your permission, we may send you emails about our services and other updates.\r\n\r\nSECTION 2 - CONSENT\r\n\r\nHow do you get my consent?\r\n\r\nWhen you provide us with personal information to complete a transaction, verify your payment method, or place a visit request, we imply that you have given consent to our collecting it and using it for that specific reason only.\r\n\r\nIf we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.\r\n\r\nHow do I withdraw my consent?\r\n\r\nIf after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at info@wendys-team.com or mailing us at: Wendy's Team, Inc 203 S Jackson St, Denver, CO, 80209, United States\r\n\r\nSECTION 3 - DISCLOSURE\r\n\r\nWe do not sell or rent your personal data to marketers or third parties. We may disclose your personal data to law enforcement, government officials, or other parties if required by law or we believe in good faith that the disclosure is necessary to prevent physical or financial harm or if you violate our Terms of Service. Any sharing or disclosure of your Personal Data will be in compliance with applicable data protection laws and regulations.\r\n\r\n\r\nSECTION 4 - DATA STORAGE\r\n\r\nYour payment information is stored through the Stripe data storage, databases and general application on a secure platform compliant with regulatory requirements. The Stripe privacy policy can be found at: https://stripe.com/gb/privacy/\r\n\r\nOther data not related to payments, is stored through Wendy’s Team, Inc data storage, databases and the general application. Your data is stored on a secure server behind a firewall.\r\n\r\nLinks\r\n\r\nWhen you click on links on our website, they may direct you away from our site. We are not responsible for the privacy practices of other sites and encourage you to read their privacy statements.\r\n\r\nSECTION 5 - SECURITY\r\n\r\nTo protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.\r\n\r\n\r\nSECTION 6 - AGE OF CONSENT\r\n\r\nBy using this site, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.\r\n\r\n\r\nSECTION 7 - CHANGES TO THIS PRIVACY POLICY\r\n\r\nWe reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website.\r\n\r\nIf Wendy’s Team, Inc is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to provide our services to you.\r\n\r\n\r\nQUESTIONS AND CONTACT INFORMATION\r\n\r\nIf you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at info@wendys-team.com or by mail to Wendy's Team, Inc, 203 S. Jackson St, Denver, CO 80209.\r\n\r\nRe: Privacy Compliance Officer\r\n\r\n203 S Jackson St, Denver, CO, 80209, United States"
            let finalString = NSMutableAttributedString(string: termsText, attributes: nil)
            let overallRange: NSRange = (termsText as NSString).rangeOfString(termsText)
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica", size: 15.0)!, range: overallRange)
            let range1: NSRange = (termsText as NSString).rangeOfString("WENDY’S TEAM, INC - DATA PRIVACY POLICY & STATEMENT")
            finalString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: 15.0)!, range: range1)
            mainLabel.attributedText = finalString
        }
    }
    
    func attributedLabel(label: TTTAttributedLabel!, didSelectLinkWithURL url: NSURL!) {
        if let url = NSURL(string: "http://www.wendys-team.com") {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
}
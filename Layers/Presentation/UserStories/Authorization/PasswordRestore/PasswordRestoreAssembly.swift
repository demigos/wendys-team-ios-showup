//
//  PasswordRestoreAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 27/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class PasswordRestoreAssembly
{
    class func createModule(resources: Dependencies, configure: (module: PasswordRestoreModuleInput) -> Void) -> PasswordRestoreViewController
    {
        let vc = R.storyboard.authorization.passwordRestoreViewController()!
        let interactor = PasswordRestoreInteractor(auth: resources.authService)
        let presenter = PasswordRestorePresenter()
        let router = PasswordRestoreRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
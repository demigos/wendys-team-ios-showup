//
//  PasswordRestoreInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 27/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface

protocol PasswordRestoreInteractorInput: class {
    func restorePassword(email: String)
}

//MARK: Output

protocol PasswordRestoreInteractorOutput: class {

    func restoreSuccess()

    func restoreFailed(error: WTError)
}

// MARK: - Interactor

final class PasswordRestoreInteractor: PasswordRestoreInteractorInput {
    weak var output: PasswordRestoreInteractorOutput!
    var auth: AuthorizationServiceInterface

    init(auth: AuthorizationServiceInterface) {
        self.auth = auth
    }

    func restorePassword(email: String) {
        auth.restorePassword(email, result: {
            result in
            switch result {
            case .Success(_):
                self.output.restoreSuccess()

            case .Failure(let error):
                self.output.restoreFailed(error)
            }
        })

    }
}
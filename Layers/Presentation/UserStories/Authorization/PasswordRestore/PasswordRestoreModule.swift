//
//  PasswordRestoreModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 27/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface

protocol PasswordRestoreModuleInput {
    func setupDelegate(output: PasswordRestoreModuleOutput)
}

//MARK: Output

protocol PasswordRestoreModuleOutput: class {

}


// MARK: - Presenter

final class PasswordRestorePresenter: PasswordRestoreModuleInput
        , PasswordRestoreViewOutput
        , PasswordRestoreInteractorOutput {
    weak var view: PasswordRestoreViewInput!
    var interactor: PasswordRestoreInteractorInput!
    weak var router: PasswordRestoreRouter!
    weak var output: PasswordRestoreModuleOutput?

    func setupDelegate(output: PasswordRestoreModuleOutput) {
        self.output = output
    }


    // MARK: - Interactor Output

    func restoreSuccess() {
        view.waitMode(false)
        DispatchToMainQueue {
            self.view.showSuccess()
        }
    }

    func restoreFailed(error: WTError) {
        view.waitMode(false)
        DispatchToMainQueue {
            self.view.showError(error.errorDescription)
        }
    }

    // MARK: - View Output
    func back() {
        router.popCurrentScreen()
    }

    func restorePassword(email: String) {
        view.waitMode(true)
        interactor.restorePassword(email)
    }

}
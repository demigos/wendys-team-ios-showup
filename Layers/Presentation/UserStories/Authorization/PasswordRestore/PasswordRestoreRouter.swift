//
//  PasswordRestoreRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 27/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class PasswordRestoreRouter: VIPERRouter
{
    weak var viewController: PasswordRestoreViewController!
	weak var presenter: PasswordRestorePresenter!
    
    override func popCurrentScreen()
    {
        if let nc = navigationController ?? viewController.navigationController {
            nc.popViewControllerAnimated(true)
        }
    }
}
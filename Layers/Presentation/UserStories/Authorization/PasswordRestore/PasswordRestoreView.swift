//
//  PasswordRestoreView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 27/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol PasswordRestoreViewInput: BaseViewControllerInput {
    func showSuccess()

    func showError(error: String)
}

protocol PasswordRestoreViewOutput: class {
    func back()

    func restorePassword(email: String)
}

// MARK: - View Controller

final class PasswordRestoreViewController: BaseViewController
        , PasswordRestoreViewInput
        , Routable, UITextFieldDelegate {
    var output: PasswordRestoreViewOutput!
    typealias RouterType = PasswordRestoreRouter
    var router: RouterType!

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var resetButton: RoundedButton!

    // MARK: Interface Builder
    @IBAction func backButtonDidSelect(sender: AnyObject) {
        output.back()
    }

    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }

    @IBAction func completeRestoreAction(sender: AnyObject) {
        output.restorePassword(emailTextField.text ?? "")
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        resetButton.enabled = false

        let tapper = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }

    func isFieldsValid() -> Bool {
        return (emailTextField.text ?? "").characters.count > 0
    }

    @IBAction func textFieldChanged(sender: AnyObject) {
        resetButton.enabled = isFieldsValid()
    }

    func optionDidSelect(index: Int, optionsSwitch: TwoOptonsSwitch) {
        resetButton.enabled = isFieldsValid()
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if isFieldsValid() {
            completeRestoreAction(self)
            return true
        }
        return false
    }

    // MARK: View Input
    func showSuccess() {
        let alertController = UIAlertController(title: "Wendys Team", message: "Please check your email to continue restore process",
                preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: {
            alert in
            self.output.back()
        }))
        presentViewController(alertController, animated: true, completion: nil)
    }
}
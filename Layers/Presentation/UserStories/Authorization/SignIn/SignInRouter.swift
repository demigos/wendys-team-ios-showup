//
//  SignInRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class SignInRouter: VIPERRouter
{
    weak var viewController: SignInViewController!
    weak var presenter: SignInPresenter!
    
    func presentPasswordRestoring()
    {
        if let nc = viewController?.navigationController {
            
            let screen = PasswordRestoreAssembly.createModule(presenter.resources) { module in
                //
            }
            present(screen, using: nc)
        }
    }
}
//
//  SignInInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol SignInInteractorInput: class
{
    func auth(email email: String, password: String)
}

//MARK: Output
protocol SignInInteractorOutput: class
{
    func authDidSuccess()
    func authDidFailWithError(error: WTError)
}

// MARK: - Interactor
final class SignInInteractor: SignInInteractorInput
{
    weak var output: SignInInteractorOutput!
    var auth: AuthorizationServiceInterface
    
    init(auth: AuthorizationServiceInterface)
    {
        self.auth = auth
    }
    
    func auth(email email: String, password: String)
    {
        auth.auth(email: email, password: password) { (result) in
            switch result {
            case .Success(_):
                if self.output != nil {
                    self.output.authDidSuccess()
                }
                
            case .Failure(let error):
                if self.output != nil {
                    self.output.authDidFailWithError(error)
                }
                
            }
        }
    }
    
}
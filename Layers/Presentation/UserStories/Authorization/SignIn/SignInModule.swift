//
//  SignInModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol SignInModuleInput
{
    func setupDelegate(output: SignInModuleOutput)
}

//MARK: Output
protocol SignInModuleOutput: class
{
    func userDidLogin()
}


// MARK: - Presenter
final class SignInPresenter: SignInModuleInput
    , SignInViewOutput
    , SignInInteractorOutput
{
    weak var view: SignInViewInput!
    var interactor: SignInInteractorInput!
	weak var router: SignInRouter!
    weak var output: SignInModuleOutput?
    var resources: Dependencies!

    func setupDelegate(output: SignInModuleOutput)
    {
        self.output = output
    }


    // MARK: - Interactor Output
    func authDidFailWithError(error: WTError)
    {
        view.waitMode(false)
        DispatchToMainQueue {
            self.view.update(withError: error.errorDescription)
        }
        
    }
    
    func authDidSuccess()
    {
        view.waitMode(false)
        output?.userDidLogin()
    }

    // MARK: - View Output
    
    func forgotPasword()
    {
        DispatchToMainQueue {
            self.router.presentPasswordRestoring()
        }
    }
    
    func back()
    {
        DispatchToMainQueue { 
            self.router.popCurrentScreen()
        }
    }
    
    func authDidSelect(email: String, password: String)
    {
        view.waitMode(true)
        DispatchToBackground { 
            self.interactor.auth(email: email, password: password)
        }
    }
}
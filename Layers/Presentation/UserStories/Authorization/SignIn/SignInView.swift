//
//  SignInView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol SignInViewInput: BaseViewControllerInput
{
    func update(withError error: String)
}

protocol SignInViewOutput: class
{
    func back()
    func forgotPasword()
    func authDidSelect(email: String, password: String)
}

// MARK: - View Controller
final class SignInViewController: BaseViewController
    , SignInViewInput
	, Routable
    , UITextFieldDelegate
{
    var output: SignInViewOutput!
    typealias RouterType = SignInRouter
    var router: RouterType!
    @IBOutlet weak var emailAddressField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signinButton: RoundedButton!
    @IBOutlet var containerView: UIView!
    
    //MARK: - Actions
    @IBAction func forgotPasswordAction(sender: AnyObject)
    {
        output.forgotPasword()
    }

    @IBAction func signInButtonTouchUpInside(sender: AnyObject) {
       signIn()
    }
    
    @IBAction func backButtonDidSelect(sender: AnyObject)
    {
        output.back()
    }
    
    @IBAction func phoneAction(sender: AnyObject)
    {
        PhoneService.call()
    }

    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        signinButton.enabled = false
        let tapper = UITapGestureRecognizer(target: self.view, action:#selector(UIView.endEditing))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }

    // MARK: View Input
    func update(withError error: String)
    {
        showError(error)
    }
    
    //MARK: - Private
    
    func showErrorInEmail() {
        emailAddressField.shake()
    }
    
    func showPasswordError() {
        passwordField.shake()
    }
    
    func signIn() {
        if let email = emailAddressField.text, let password = passwordField.text
            where email.isValidEmail == true
        {
            output.authDidSelect(email, password: password)
        } else {
            showErrorInEmail()
            showPasswordError()
        }
    }

    func isSignInOk() -> Bool {
        return (emailAddressField.text ?? "").characters.count > 0 && (passwordField.text ?? "").characters.count > 0
    }
    
    @IBAction func emailChanged(sender: AnyObject) {
        signinButton.enabled = isSignInOk()
    }

    @IBAction func passwordChanged(sender: AnyObject) {
        signinButton.enabled = isSignInOk()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case emailAddressField:
            passwordField.becomeFirstResponder()
            break
        case passwordField:
            passwordField.resignFirstResponder()
            if isSignInOk() {
                signIn()
                return true
            }
            return false
        default:
            break
        }
        
        return true
    }
}
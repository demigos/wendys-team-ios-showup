//
//  SignInAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class SignInAssembly
{
    class func createModule(resources: Dependencies, configure: (module: SignInModuleInput) -> Void) -> SignInViewController
    {
        let vc = R.storyboard.authorization.signInViewController()!
        let interactor = SignInInteractor(auth: resources.authService)
        let presenter = SignInPresenter()
        let router = SignInRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
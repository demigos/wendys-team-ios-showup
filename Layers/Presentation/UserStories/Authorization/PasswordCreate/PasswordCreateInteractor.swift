// MARK: - Interface

protocol PasswordCreateInteractorInput: class {
    func createPassword(code: String, password: String)
}

//MARK: Output

protocol PasswordCreateInteractorOutput: class {

    func createSuccess()

    func createFailed(error: WTError)
}

// MARK: - Interactor

final class PasswordCreateInteractor: PasswordCreateInteractorInput {
    weak var output: PasswordCreateInteractorOutput!
    var auth: AuthorizationServiceInterface

    init(auth: AuthorizationServiceInterface) {
        self.auth = auth
    }

    func createPassword(code: String, password: String) {
        auth.createPassword(code, password: password, result: {
            result in
            switch result {
            case .Success(_):
                self.output.createSuccess()

            case .Failure(let error):
                self.output.createFailed(error)
            }
        })
    }

}
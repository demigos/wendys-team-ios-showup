import UIKit

// MARK: - Router

final class PasswordCreateRouter: VIPERRouter {
    weak var viewController: PasswordCreateViewController!
    weak var presenter: PasswordCreatePresenter!

    override func popCurrentScreen() {
        self.viewController.navigationController?.popViewControllerAnimated(true)
    }
}
import UIKit

// MARK: - Interface

protocol PasswordCreateViewInput: class {
    func showSuccess()

    func showError(error: String)
}

protocol PasswordCreateViewOutput: class {
    func back()

    func createPassword(password: String)
}

// MARK: - View Controller

final class PasswordCreateViewController: BaseViewController
        , PasswordCreateViewInput
        , Routable, UITextFieldDelegate {
    var output: PasswordCreateViewOutput!
    typealias RouterType = PasswordCreateRouter
    var router: RouterType!

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var saveButton: RoundedButton!

    // MARK: Interface Builder
    @IBAction func backButtonDidSelect(sender: AnyObject) {
        output.back()
    }

    @IBAction func phoneAction(sender: AnyObject) {
        PhoneService.call()
    }

    @IBAction func saveAndSignInAction(sender: AnyObject) {
        if passwordTextField.text != confirmTextField.text {
            showError("Password and confirmation are different")
            return
        }
        output.createPassword(passwordTextField.text ?? "")
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.enabled = false

        let tapper = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }

    func isFieldsValid() -> Bool {
        return (passwordTextField.text ?? "").characters.count > 0
                && (confirmTextField.text ?? "").characters.count > 0
    }

    @IBAction func textFieldChanged(sender: AnyObject) {
        saveButton.enabled = isFieldsValid()
    }

    func optionDidSelect(index: Int, optionsSwitch: TwoOptonsSwitch) {
        saveButton.enabled = isFieldsValid()
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case passwordTextField:
            confirmTextField.becomeFirstResponder()
            return true
        case confirmTextField:
            if isFieldsValid() {
                saveAndSignInAction(self)
                return true
            }
            return false
        default:
            return false
        }
    }

    // MARK: View Input
    func showSuccess() {
        // todo open main screen
    }
}
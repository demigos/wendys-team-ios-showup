import UIKit

// MARK: - Module interface

protocol PasswordCreateModuleInput {
    func setupDelegate(output: PasswordCreateModuleOutput)

    func setCode(code: String)
}

//MARK: Output

protocol PasswordCreateModuleOutput: class {
    func userDidLogin()
}


// MARK: - Presenter

final class PasswordCreatePresenter: PasswordCreateModuleInput
        , PasswordCreateViewOutput
        , PasswordCreateInteractorOutput {
    weak var view: PasswordCreateViewInput!
    var interactor: PasswordCreateInteractorInput!
    weak var router: PasswordCreateRouter!
    weak var output: PasswordCreateModuleOutput?
    var code: String?

    func setupDelegate(output: PasswordCreateModuleOutput) {
        self.output = output
    }

    func setCode(code: String) {
        self.code = code
    }

    // MARK: - Interactor Output

    func createSuccess() {
        DispatchToMainQueue {
            self.output?.userDidLogin()
        }
    }

    func createFailed(error: WTError) {
        DispatchToMainQueue {
            self.view.showError(error.errorDescription)
        }
    }

    // MARK: - View Output
    func back() {
        router.popCurrentScreen()
    }

    func createPassword(password: String) {
        guard let code = code else {
            self.view.showError("Wrong reset password code")
            return
        }
        interactor.createPassword(code, password: password)

    }

}
import UIKit

final class PasswordCreateAssembly
{
    class func createModule(resources: Dependencies, configure: (module: PasswordCreateModuleInput) -> Void) -> PasswordCreateViewController
    {
        let vc = R.storyboard.authorization.passwordCreateViewController()!
        let interactor = PasswordCreateInteractor(auth: resources.authService)
        let presenter = PasswordCreatePresenter()
        let router = PasswordCreateRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router

        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
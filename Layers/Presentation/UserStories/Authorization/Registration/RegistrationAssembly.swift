//
//  RegistrationAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class RegistrationAssembly
{
    class func createModule(resources resources: Dependencies, configure: (module: RegistrationModuleInput) -> Void) -> RegistrationViewController
    {
        let vc = R.storyboard.authorization.registrationViewController()!
        let form = R.storyboard.authorization.createAccountForm()!
        vc.form = form
        form.parent = vc
        
        let interactor = RegistrationInteractor(auth: resources.authService)
        let presenter = RegistrationPresenter()
        let router = RegistrationRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
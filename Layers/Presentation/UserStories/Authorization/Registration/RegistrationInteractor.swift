//
//  RegistrationInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol RegistrationInteractorInput: class
{
    func signUp(user: UserDomainModel)
}

//MARK: Output
protocol RegistrationInteractorOutput: class
{
    func userDidSignUp()
    func userDidFailToSignUp(error: WTError)
    func authDidSuccess()
}

// MARK: - Interactor
final class RegistrationInteractor: RegistrationInteractorInput
{
    weak var output: RegistrationInteractorOutput!
    var auth: AuthorizationServiceInterface
    
    init(auth: AuthorizationServiceInterface)
    {
        self.auth = auth
    }
    
    func signUp(user: UserDomainModel)
    {
        auth.signUp(user) { result in
            switch result {
                
            case .Success(_):
                var tracker = GAI.sharedInstance().defaultTracker
                let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Registration Success", label: nil, value: nil).build() as [NSObject: AnyObject]
                tracker.send(event)
                self.output.authDidSuccess()
//                self.output.userDidSignUp()

            case .Failure(let error):
                self.output.userDidFailToSignUp(error)
            }
        }
    }
}
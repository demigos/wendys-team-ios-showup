//
//  RegistrationView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface

protocol RegistrationViewInput: class {
    func waitMode(on: Bool)
}

protocol RegistrationViewOutput: class {
    func viewDidLoad()

    func back()

    func signUp(user: UserDomainModel)
}

// MARK: - Form

class CreateAccountForm: UITableViewController, UITextFieldDelegate, TwoOptionsSwitchDelegate {

    @IBOutlet weak var genderSwitch: GenderSwitch!
    @IBOutlet weak var name: MaskTextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: MaskTextField!

    weak var parent: RegistrationViewController!

    override func viewDidLoad() {
        genderSwitch.delegate = self
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }

    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectMake(0, 0, 0, 0))
    }

    func isFieldsValid() -> Bool {
        return (name.text ?? "").characters.count > 0 && (password.text ?? "").characters.count > 0
                && (email.text ?? "").characters.count > 0 && genderSwitch.gender != nil
    }

    @IBAction func textFieldChanged(sender: AnyObject) {
        parent.enableButton(isFieldsValid())
    }

    func optionDidSelect(index: Int, optionsSwitch: TwoOptonsSwitch) {
        parent.enableButton(isFieldsValid())
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField {
        case name:
            email.becomeFirstResponder()
            break
        case email:
            password.becomeFirstResponder()
            break
        case password:
            if isFieldsValid() {
                parent.signUpAction(self)
                return true
            }
            return false
        default:
            break
        }

        return true
    }
}

// MARK: - View Controller

final class RegistrationViewController: BaseViewController
        , RegistrationViewInput
        , Routable {
    var output: RegistrationViewOutput!
    typealias RouterType = RegistrationRouter
    var router: RouterType!
    var form: CreateAccountForm!

    @IBOutlet weak var button: RoundedButton!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var bottomPadding: NSLayoutConstraint!


    // MARK: Actions
    @IBAction func backButtonAction(sender: AnyObject) {
        output.back()
    }

    @IBAction func phoneButtonAction(sender: AnyObject) {
        PhoneService.call()
    }
    
    @IBAction func termsLabelTapped(sender:AnyObject) {
        let termsViewController = R.storyboard.authorization.termsViewController()!
        termsViewController.title = "Terms"
        self.navigationController?.pushViewController(termsViewController, animated: true)
    }
    
    @IBAction func dataPolicyLabelTapped(sender:AnyObject) {
        let termsViewController = R.storyboard.authorization.termsViewController()!
        termsViewController.terms = false
        self.navigationController?.pushViewController(termsViewController, animated: true)
    }

    @IBAction func signUpAction(sender: AnyObject) {
        var user = UserDomainModel()
        user.email = form.email.text
        user.name = form.name.text
        user.password = form.password.text
        user.gender = form.genderSwitch.gender

        output.signUp(user)
    }


    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewDidLoad()
        button.enabled = false

        let tapper = UITapGestureRecognizer(target: self.view, action:#selector(UIView.endEditing))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }

    // MARK: View Input

    func enableButton(enabled: Bool) {
        button.enabled = enabled
    }
}
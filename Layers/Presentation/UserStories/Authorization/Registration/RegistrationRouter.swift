//
//  RegistrationRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Router
final class RegistrationRouter: VIPERRouter
{
    weak var viewController: RegistrationViewController!
	weak var presenter: RegistrationPresenter!
    
    func presentForm()
    {
        
        let parent = viewController
        let child = viewController.form
        
        parent.addChildViewController(child)
        child.view.frame = parent.container.bounds
        parent.container.addSubview(child.view)
        child.didMoveToParentViewController(parent)
    }
    
    func presentMessage(message: String)
    {
        viewController.showError(message)
    }
    
    
}

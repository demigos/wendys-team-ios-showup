//
//  RegistrationModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Module interface
protocol RegistrationModuleInput
{
    func setupDelegate(output: RegistrationModuleOutput)
}

//MARK: Output
protocol RegistrationModuleOutput: class
{
    func userDidSignup()
    func userDidFailToSignUpWithDecription(description : String)
    func userDidLogin()
}


// MARK: - Presenter
final class RegistrationPresenter: RegistrationModuleInput
    , RegistrationViewOutput
    , RegistrationInteractorOutput
{
    weak var view: RegistrationViewInput!
    var interactor: RegistrationInteractorInput!
    weak var router: RegistrationRouter!
    weak var output: RegistrationModuleOutput?
    
    func setupDelegate(output: RegistrationModuleOutput)
    {
        self.output = output
    }

    func authDidSuccess()
    {
        view.waitMode(false)
        output?.userDidLogin()
    }
    
    // MARK: - Interactor Output
    func userDidSignUp()
    {
        
        DispatchToMainQueue {
            self.view.waitMode(false)
            self.output?.userDidSignup()
        }
        
    }
    
    func userDidFailToSignUp(error: WTError)
    {
        DispatchToMainQueue {
            self.view.waitMode(false)
            self.output?.userDidFailToSignUpWithDecription(error.errorDescription)
        }
    }
    
    // MARK: - View Output
    func viewDidLoad()
    {
        router.presentForm()
    }
    
    func back()
    {
        router.popCurrentScreen()
    }
    
    func signUp(user: UserDomainModel)
    {
        view.waitMode(true)
        DispatchToBackground {
            self.interactor.signUp(user)
        }
    }
}
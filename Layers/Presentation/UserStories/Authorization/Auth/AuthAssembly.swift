//
//  AuthAssembly.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

final class AuthAssembly
{
    class func createModule(resources resources: Dependencies, configure: (module: AuthModuleInput) -> Void) -> AuthViewController
    {
        let vc = R.storyboard.authorization.authViewController()!
        let interactor = AuthInteractor(auth: resources.authService)
        let presenter = AuthPresenter()
        let router = AuthRouter()


        interactor.output = presenter

        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.resources = resources
		
        configure(module: presenter)

        vc.output = presenter
        vc.router = router

        router.viewController = vc
		router.presenter = presenter

        return vc
    }
}
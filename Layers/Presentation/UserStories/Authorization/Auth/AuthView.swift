//
//  AuthView.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: - Interface
protocol AuthViewInput: BaseViewControllerInput
{
    func update(withError error: String)
}

protocol AuthViewOutput: class
{
    func tokenDidReceived(token: String)
    func viewIsReady()
    func loginWithEmailDidSelect()
    func registrationButtonDidSelect()
    func okPressed()
}

// MARK: - View Controller
final class AuthViewController: BaseViewController
    , AuthViewInput
    , Routable
    , MethodFormViewDelegate
    , FBSDKLoginButtonDelegate
{
    var output: AuthViewOutput!
    typealias RouterType = AuthRouter
    var router: RouterType!
    
    var form : MethodFormView?
    
    // MARK: IB
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var facebookLoginButton: RoundedButton!
    @IBOutlet weak var emailLoginButton: UIButton!
    
    
    
    @IBAction func loginButtonTochUpInside(sender: AnyObject) {
        
        let login = FBSDKLoginManager()
        login.logOut()
        login.logInWithReadPermissions(["public_profile","email"], fromViewController: self) { (loginResult, error) in
            if error != nil {
                self.showError("Login error: \(error.localizedDescription)");
            } else if (loginResult.isCancelled ) {
                self.showError("Login cancelled");
            } else {
                self.output.tokenDidReceived(loginResult.token.tokenString)
            }
        }
        
    }
    
    
    @IBAction func loginEmailButtonTouchUpInside(sender: AnyObject) {
        
        form = MethodFormView(frame: view.bounds)
        form!.formDelegate = self
        form!.hidden = true
        view.addSubview(form!)
        view.bringSubviewToFront(form!)
        
        form!.show()
    }
    
    
    // MARK: - Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        // Email login button
        emailLoginButton.layer.cornerRadius = 4
        emailLoginButton.layer.borderWidth = 1
        emailLoginButton.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.bottomView.frame
        rectShape.position = self.bottomView.center
        rectShape.path = UIBezierPath(roundedRect: self.bottomView.bounds, byRoundingCorners: [.TopRight, .TopLeft], cornerRadii: CGSize(width: 20, height: 20)).CGPath
        self.bottomView.layer.mask = rectShape
    }
    
    func okPressed() {
        output.okPressed()
    }
    
    // MARK: View Input
    func update(withError error: String)
    {
        showError(error)
    }
    
    // MARK: Form delegate
    func loginDidSelect()
    {
        form?.hide()
        output?.loginWithEmailDidSelect()
    }
    
    func registrationDidSelect()
    {
        form?.hide()
        output?.registrationButtonDidSelect()
    }
    
    func cancelDidSelect()
    {
        self.form?.hide()
    }
    
    //MARK: Facebook Delegate Methods
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            if let token = result.token.tokenString {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if result.grantedPermissions.contains("email")
                {
                    self.output.tokenDidReceived(token)
                }
            }
            
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
}
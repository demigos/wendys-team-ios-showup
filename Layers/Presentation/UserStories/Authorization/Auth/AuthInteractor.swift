//
//  AuthInteractor.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

// MARK: - Interface
protocol AuthInteractorInput: class
{
    func authWithFacebookToken(token: String)
}

//MARK: Output
protocol AuthInteractorOutput: class
{
    func authDidSuccess()
    func authDidFailWithError(error: WTError)
}

// MARK: - Interactor
final class AuthInteractor: AuthInteractorInput
{
    weak var output: AuthInteractorOutput!
    var auth: AuthorizationServiceInterface
    
    init(auth: AuthorizationServiceInterface)
    {
        self.auth = auth
    }
    
    func authWithFacebookToken(token: String)
    {
        auth.auth(facebookToken: token, result: { result in
            switch result {
            case .Success(_):
                var tracker = GAI.sharedInstance().defaultTracker
                let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Authorization Success", label: nil, value: nil).build() as [NSObject: AnyObject]
                tracker.send(event)
                self.output.authDidSuccess()
                
            case .Failure(let error):
                self.output.authDidFailWithError(error)
            }
        })
        
    }

}
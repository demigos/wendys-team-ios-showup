//
//  AuthRouter.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit

// MARK: Router input
protocol AuthRouterInput: class
{
    func presentEmailAuthorization()
    func presentRegistration()
    func presentMessage(message: String)
    func presentSuccessAlert()
    func popToRoot()
}

// MARK: - Router
final class AuthRouter: VIPERRouter
    , AuthRouterInput
{
    weak var viewController: AuthViewController!
	weak var presenter: AuthPresenter!
    
    func presentEmailAuthorization()
    {
        guard let nc = viewController.navigationController else {
            assertionFailure("\(self) has no navigation controller")
            return
        }
        
        let signIn = SignInAssembly.createModule(presenter.resources) { module in
            module.setupDelegate(self.presenter)
        }
        
        signIn.present(inNavigation: nc)
    }
    
    func presentRegistration()
    {
        guard let nc = viewController.navigationController else {
            assertionFailure("\(self) has no navigation controller")
            return
        }
        
        let signUp = RegistrationAssembly.createModule(resources: presenter.resources) { module in
            module.setupDelegate(self.presenter)
        }
        
        signUp.present(inNavigation: nc)
    }
    
    func presentSuccessAlert() {
        
        let alertController = UIAlertController(title: "Wendys Team", message: "You successfully signed up, check your email for confirmation link and finish registration", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default,handler: { (action) in
            self.viewController?.okPressed()
            }))
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func presentMessage(message: String)
    {
        viewController.showError(message)
    }

}
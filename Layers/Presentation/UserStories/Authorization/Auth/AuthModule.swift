//
//  AuthModule.swift
//  wendysteam-ios
//
//  Created by Maksim Bazarov on 24/07/2016.
//  Copyright © 2016 Wendy's Team. All rights reserved.
//

import UIKit
import ThreadsKit

// MARK: - Module interface
protocol AuthModuleInput
{
    func setupDelegate(output: AuthModuleOutput)
}

//MARK: Output
protocol AuthModuleOutput: class
{
    func userDidLogin()
}


// MARK: - Presenter
final class AuthPresenter: AuthModuleInput
    , AuthViewOutput
    , AuthInteractorOutput
    , SignInModuleOutput
    , RegistrationModuleOutput
{
    weak var view: AuthViewInput!
    var interactor: AuthInteractorInput!
    weak var router: AuthRouterInput!
    weak var output: AuthModuleOutput?
    var resources: Dependencies!
    
    func setupDelegate(output: AuthModuleOutput)
    {
        self.output = output
    }
    
    
    // MARK: - Interactor Output
    func authDidFailWithError(error: WTError)
    {
        view.waitMode(false)
        DispatchToMainQueue {
            self.view.update(withError: error.errorDescription)
        }
        
    }
    
    func authDidSuccess()
    {
        var tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Authorization Success", label: nil, value: nil).build() as [NSObject: AnyObject]
        tracker.send(event)
        view.waitMode(false)
        output?.userDidLogin()
    }
    // MARK: - View Output
    func tokenDidReceived(token: String)
    {
        view.waitMode(true)
        DispatchToBackground {
            self.interactor.authWithFacebookToken(token)
        }
    }
    
    func viewIsReady(){}
    
    func loginWithEmailDidSelect()
    {
        router.presentEmailAuthorization()
    }
    
    func okPressed() {
        router.popToRoot()
    }
    
    func registrationButtonDidSelect()
    {
        router.presentRegistration()
    }
    
    
    // MARK: SignIn Output
    func userDidLogin()
    {
        var tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEventWithCategory("Flow", action: "Authorization Success", label: nil, value: nil).build() as [NSObject: AnyObject]
        tracker.send(event)
        output?.userDidLogin()
    }
    
    func userDidFailToSignUpWithDecription(description : String) {
        router.presentMessage(description)
    }
    
    
    
    // MARK: Registration output
    func userDidSignup()
    {
        //        router.popToRoot()
        router.presentSuccessAlert()
        //        router.presentMessage("You successfully signed up, check your email for confirmation link and finish registration")
        
    }
}
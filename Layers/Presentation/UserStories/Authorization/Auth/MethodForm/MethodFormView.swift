//
//  MethodFormView.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 29.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class MethodFormView: UIView {

    var view: UIView!
    weak var formDelegate : MethodFormViewDelegate?
    
    @IBOutlet weak var bottomPanel: UIView!
    @IBOutlet weak var registerButton: UIButton! 
    @IBOutlet weak var dimView: UIView!
    
    @IBAction func cancelButtonTouchUpInside(sender: AnyObject) {
        formDelegate?.cancelDidSelect()
    }
    
    @IBAction func registerButtonTouchUpInside(sender: AnyObject) {
        formDelegate?.registrationDidSelect()
    }
    
    @IBAction func alreadyMemberButtonTouchUpInside(sender: AnyObject) {
        formDelegate?.loginDidSelect()
    }
    
    
    func xibSetup() {

        view = loadViewFromNib()
        
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "MethodFormView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    } 
  
    override func layoutSubviews() {
        super.layoutSubviews()

    }
    
    func show() {
       
        dimView.alpha = 0
        bottomPanel.center = CGPointMake(bottomPanel.center.x , bottomPanel.center.y + CGRectGetHeight(bottomPanel.frame))
        hidden = false
        
        
        
        
        UIView.animateWithDuration(0.25, delay: 0, options: [.CurveEaseInOut], animations: { 
            self.dimView.alpha = 1
            self.bottomPanel.center = CGPointMake(self.bottomPanel.center.x, self.bottomPanel.center.y - CGRectGetHeight(self.bottomPanel.frame))
            
            }) { (finished) in
                let rectShape = CAShapeLayer()
                rectShape.bounds = self.bottomPanel.frame
                rectShape.position = self.bottomPanel.center
                rectShape.path = UIBezierPath(roundedRect: self.bottomPanel.bounds, byRoundingCorners: [.TopRight, .TopLeft], cornerRadii: CGSize(width: 20, height: 20)).CGPath
                self.bottomPanel.layer.mask = rectShape
        }
        
    }
    
    func hide() {
        
        UIView.animateWithDuration(0.25, delay: 0, options: [.CurveEaseInOut], animations: {
            self.dimView.alpha = 0
            self.bottomPanel.center = CGPointMake(self.bottomPanel.center.x, self.bottomPanel.center.y + CGRectGetHeight(self.bottomPanel.frame))
            
        }) { (finished) in
            self.hidden = true
        }
    }
    

}

//
//  MethodFormViewDelegate.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 29.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//


protocol MethodFormViewDelegate: class {

    func loginDidSelect()
    func registrationDidSelect()
    func cancelDidSelect()
    
}

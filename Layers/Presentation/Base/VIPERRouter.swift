//
//  ViperRouter.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 24.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

//
// MARK: - VIPER Router Interface
protocol VIPERRouterInterface
{
    weak var window : UIWindow? {get set}
    weak var navigationController : UINavigationController? {get set}

    func presentChild(vc: UIViewController, from parent: UIViewController)
    func presentChild(vc: UIViewController, from parent: UIViewController, using view: UIView)
    func presentModal(vc: UIViewController, from parent: UIViewController)
    func present(vc: UIViewController, using window:UIWindow)
    func present(vc: UIViewController, using nc: UINavigationController)
    func presentModalTransparent(vc: UIViewController, from parent: UIViewController)
    
    func popCurrentScreen()
    func popToRoot()

}

//
// MARK: - VIPER Router
class VIPERRouter:  VIPERRouterInterface
{
    weak var window : UIWindow?
    weak var navigationController : UINavigationController?
    
    func presentChild(vc: UIViewController, from parent: UIViewController)
    {
        parent.addChildViewController(vc)
        vc.view.frame = parent.view.bounds
        parent.view.addSubview(vc.view)
        vc.didMoveToParentViewController(parent)
    }
    
    func presentChild(vc: UIViewController, from parent: UIViewController, using view: UIView)
    {
        if !parent.view.subviews.contains(view) {
            fatalError("FATAL: presenting parent \(parent) doesn't contain that view \(view)")
        }
        
        parent.addChildViewController(vc)
        vc.view.frame = view.bounds
        view.addSubview(vc.view)
        vc.didMoveToParentViewController(parent)
    }

    func presentModal(vc: UIViewController, from parent: UIViewController)
    {
        parent.presentViewController(vc, animated: true, completion: { })
    }
    
    func presentModalTransparent(vc: UIViewController, from parent: UIViewController) {
        vc.modalPresentationStyle = .OverCurrentContext
        parent.presentViewController(vc, animated: true, completion: { })
    }

    func present(vc: UIViewController, using window:UIWindow)
    {
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBarHidden = true
        window.rootViewController = nc
        window.makeKeyAndVisible()
        navigationController = nc
        
        self.window = window
    }

    func present(vc: UIViewController, using nc: UINavigationController)
    {
        if nc.topViewController != vc {
            navigationController = nc
            nc.pushViewController(vc, animated: true)
        }
    }

    func popCurrentScreen()
    {
        if let nc = navigationController {
            nc.popViewControllerAnimated(true)
        }
    }
    
    func popToRoot()
    {
        if let nc = navigationController {
            nc.popToRootViewControllerAnimated(true)
        }
    }
}


//
// MARK: - Routable protocol

protocol Routable
{
    associatedtype RouterType
    
    var router: RouterType! {get set}
    
    func presentChild(from parent: UIViewController)
    func presentChild(from parent: UIViewController, using view: UIView)
    func presentModal(from parent: UIViewController)
    func present(fromWindow window:UIWindow)
    func present(inNavigation nc: UINavigationController)
    
    func showError(error: String)
}

extension Routable where RouterType : VIPERRouterInterface, Self: UIViewController
{
    /**
     Present as child of parent view controller
     
     - parameter vc: View controller for presenting
     - parameter parent: Parent view controller for presenting from
     */
    func presentChild(from parent: UIViewController)
    {
        router.presentChild(self, from: parent)
    }
    
    /**
     Present as child of parent view controller using specified view
     
     - parameter vc: View controller for presenting
     - parameter parent: Parent view controller for presenting from
     - parameter view: View on which controller should be presented
     */
    func presentChild(from parent: UIViewController, using view: UIView)
    {
        router.presentChild(self, from: parent, using: view)
    }
    
    
    /**
     Present as modal from parent view controller
     
     - parameter vc: View controller for presenting
     - parameter parent: Parent view controller for presenting from
     */
    func presentModal(from parent: UIViewController)
    {
        router.presentModal(self, from: parent)
    }
    
    func presentModalTransparent(from parent: UIViewController)
    {
        router.presentModalTransparent(self, from: parent)
    }
    
    
    /**
     Present as *window's* root view controller
     
     - parameter vc: View controller for presenting
     - parameter window: Window for presenting from
     */
    func present(fromWindow window:UIWindow)
    {
        router.present(self, using: window)
    }
    
    /**
     Present in navigation controller
     
     - parameter vc: View controller for presenting
     - parameter nc: Navigation controller for pushing
     */
    func present(inNavigation nc: UINavigationController)
    {
        router.present(self, using: nc)
    }
    
    func showError(error: String)
    {
        let alertController = UIAlertController(title: "Wendys Team", message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default,handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }

}


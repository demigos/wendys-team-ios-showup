//
//  ViperViewController.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 26.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit
import ThreadsKit

protocol BaseViewControllerInput: class
{
    func waitMode(on: Bool)
    func showError(error: String)
}

class BaseViewController: GAITrackedViewController
{
    var waitView = UIView()
    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    var alertController : UIAlertController?

    deinit {
        print("[D] \(self) destroyed")
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }


    override func viewDidLoad()
    {
        super.viewDidLoad()
        waitView.addSubview(spinner)
        waitView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
        view.addSubview(waitView)
        waitView.hidden = true

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(_keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(_keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)

        let tapper = UITapGestureRecognizer(target: self.view, action:#selector(UIView.endEditing))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }


    // MARK: Abstract
    func keyboardWillShow(kb: KeyboardParameters)
    {
    }

    func keyboardWillHide(kb: KeyboardParameters)
    {
    }

    // MARK: Private
    func _keyboardWillShow(notification: NSNotification)
    {
        let n = KeyboardParameters(notification: notification)
        keyboardWillShow(n)
    }

    func _keyboardWillHide(notification: NSNotification)
    {
        let n = KeyboardParameters(notification: notification)
        keyboardWillHide(n)
    }


    // MARK : - Wait mode


    func waitMode(on: Bool)
    {

        DispatchToMainQueue {
            if on {
                self.setupControllerToWaitMode()
            } else {
                self.setupControllerToRegularMode()
            }
        }

    }

    override func viewDidLayoutSubviews() {
        waitView.frame = view.frame
        spinner.center = waitView.center

    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        var tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "baseView")
        tracker.send(GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject])
    }

    func setupControllerToWaitMode()
    {
        waitView.frame = view.frame
        spinner.center = waitView.center
        spinner.startAnimating()
        waitView.hidden = false

    }

    func setupControllerToRegularMode()
    {
        spinner.stopAnimating()
        waitView.hidden = true

    }
}

//
//  PaymentServiceInterface.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 07.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation
import Result

protocol PaymentServiceInterface: class {
    
   
    /**
     Method registers payment card

     - parameter card:          card as PaymentCardDomainModel
     - parameter result:        Result of registration, returns Result<String, NSError> structure where String is token or NSError is error
     */
    func registerCard(card:PaymentCardDomainModel, result: (Result<String, NSError>) ->())


}

//
// Created by Maksim Bazarov on 09.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Result
import Stripe

class StripeService : PaymentServiceInterface {

    static let instance = StripeService()

    class func auth()
    {
        DispatchToBackground {
            Stripe.setDefaultPublishableKey("pk_test_wiGHbN0AXu2NVgw3wGOpTk1E")
//            Stripe.setDefaultPublishableKey("pk_live_FZMM8j26pFTGe8eAHSttXPa4")
        }

    }

    func registerCard(card:PaymentCardDomainModel, result: (Result<String, NSError>) ->())
    {
        if let stripeCard = card.stripeCardParams() {
            STPAPIClient.sharedClient().createTokenWithCard(stripeCard) { (token, error) -> Void in

                if let error = error
                {
                    result(.Failure(self.makeError(error.localizedDescription)))
                }

                else if let token = token {
                    let stripeToken = token.tokenId
                    result(.Success(stripeToken))
                }
                else
                {
                    result(.Failure(self.makeError("Undefined error")))
                }
            }
        } else {
            result(.Failure(self.makeError("Please fill all fields")))
        }

    }


    func makeError(errorDescription: String) -> NSError {
        let userInfo = [
            NSLocalizedDescriptionKey: NSLocalizedString(errorDescription, comment: "Error description string")
        ]
        let error = NSError(domain: "card.error", code: 0, userInfo: userInfo)
        return error
    }
}

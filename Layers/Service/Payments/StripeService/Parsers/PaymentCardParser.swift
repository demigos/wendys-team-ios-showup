//
//  PaymentCardParser.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 25.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//


import Stripe

extension PaymentCardDomainModel
{

    func stripeCardParams() -> STPCardParams?
    {
        var params : STPCardParams?
        
        if  let name = cardholderName where !name.isEmpty,
            let cvc = cvc where !cvc.isEmpty,
            let expirationYear = expirationYear,
            let expirationMonth = expirationMonth,
            let number = number where !number.isEmpty
        {
            params = STPCardParams()
            params!.number = number
            params!.expMonth = expirationMonth
            params!.expYear = expirationYear
            params!.name = name
            params!.cvc = cvc
        } 
        
        return params
    }
}

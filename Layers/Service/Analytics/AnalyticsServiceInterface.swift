//
// Created by Maksim Bazarov on 25.06.16.
// Copyright (c) 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

protocol AnalyticsServiceInterface
{
    
    /**
     Registers the user registration event and set user id for rest analytics
     
     - parameter userID: User name|emeayl|any user identificator
     */
    func userDidRegistred(userID: String)
    
    /**
     Registers visit creation event
     
     - parameter visitID: visit identificator
     */
    func visitDidBook(visitID: String)
    
    /**
     Registers payment
     
     - parameter paymentID: payment ID or "mass" if mass payments happend
     */
    func paymentDidSuccess(paymentID: String)
    
}

//
// Created by Maksim Bazarov on 25.06.16.
// Copyright (c) 2016 Maksim Bazarov. All rights reserved.
//

import Foundation


class GoogleAnalyticsService : AnalyticsServiceInterface
{

    var gai = GAI.sharedInstance()
    var tracker : GAITracker
    
    init()
    {
        gai.trackUncaughtExceptions = true
        gai.logger.logLevel = .Verbose
        gai.dispatchInterval = 10
        
        self.tracker = gai.trackerWithTrackingId("UA-79646562-1")
    }
    
    func userDidRegistred(userID: String)
    {
        let event = createEvent(withCategory: "Flow", action: "User registred", label: userID)
        tracker.send(event)
    }

    func visitDidBook(visitID: String)
    {
        let event = createEvent(withCategory: "Flow", action: "Visit booked", label: visitID)
        tracker.send(event)
    }

    func paymentDidSuccess(paymentID: String)
    {
        let event = createEvent(withCategory: "Flow", action: "Payment success", label: paymentID)
        tracker.send(event)
    }
    
    
    private func createEvent(withCategory category: String, action: String, label: String) -> [NSObject: AnyObject]
    {
        return  GAIDictionaryBuilder.createEventWithCategory(category, action: action, label: label, value: nil).build() as [NSObject: AnyObject]
    }

}

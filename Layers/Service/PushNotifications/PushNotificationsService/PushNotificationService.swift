//
// Created by Maksim Bazarov on 25.02.16.
// Copyright (c) 2016 Wendy'sTeam. All rights reserved.
//

import UIKit


class PushNotificationService : PushNotificationsServiceInterface {
    
    var token : String?
    weak var delegate : PushNotificationsServiceDelegate?
    weak var server: ServerAPIInterface!
    var notHandledNotificationID : Int?
    
    
    func setupServer(server: ServerAPIInterface)
    {
        self.server = server
    }
    
    func registerWithDelegate(delegate: PushNotificationsServiceDelegate?) {
        let application = UIApplication.sharedApplication()
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        self.delegate = delegate
    }
    
    func checkNotification() {
        if let notID = self.notHandledNotificationID {
            self.delegate?.visitDidPaid(notID)
            self.notHandledNotificationID = nil
        }
    }
    
    func setupToken(deviceToken: NSData) {
        
        var deviceTokenString = NSString(string:deviceToken.description)
        deviceTokenString = deviceTokenString.stringByReplacingOccurrencesOfString("<", withString: "")
        deviceTokenString = deviceTokenString.stringByReplacingOccurrencesOfString(" ", withString: "")
        deviceTokenString = deviceTokenString.stringByReplacingOccurrencesOfString(">", withString: "")
        token = String(deviceTokenString)
        
        print("APNS token did received: \(String(deviceTokenString))")
        
        sendTokenToServer(String(deviceTokenString))
    }
    
    func handleLaunchOptions(launchOptions: [NSObject: AnyObject]?)
    {
        if let options = launchOptions, let data = options[UIApplicationLaunchOptionsRemoteNotificationKey] as? [NSObject:AnyObject]  {
            handlePushNotification(data)
        }
    }
    
    func handlePushNotificationFromBackground(data: [NSObject:AnyObject]) {
        if let notification = NotificationDomainModel.initFromResponse(data) {
            let message = "[PUSH N]: TM:\(notification.teamMemberID) VID: \(notification.visitID) TYPE: \(notification.type)"
            print(message)
            routeNotification(notification)
        } else {
            let message = "[PUSH N]: handlePushNotification: Can't parse data: \(data)"
            print(message)
            if let type_raw = data["type"] as? Int,
                let failureMessage = data["message"] as? String
                //                ,visitID = data["visit_id"] as? Int
            {
                if type_raw == 9 {
                    if let visitId = data["visit_id"] as? Int {
                        self.delegate?.visitDidDone(visitId)
                    }
                    else {
                        self.delegate?.showUnpaidVisits()
                    }
                }
                //                self.delegate?.visitDidDone(notification.visitID)
            }
            //            else {
            //                let message = "[PUSH N]: handlePushNotification: Can't parse data: \(data)"
            //                print(message)
            //            }
        }
    }
    
    
    func handlePushNotification(data: [NSObject:AnyObject]) {
        if let notification = NotificationDomainModel.initFromResponse(data) {
            let message = "[PUSH N]: TM:\(notification.teamMemberID) VID: \(notification.visitID) TYPE: \(notification.type)"
            print(message)
            routeNotification(notification)
        } else {
            let message = "[PUSH N]: handlePushNotification: Can't parse data: \(data)"
            print(message)
            if let type_raw = data["type"] as? Int,
            let failureMessage = data["message"] as? String
                //                ,visitID = data["visit_id"] as? Int
            {
                if type_raw == 9 {
                    delegate?.paidWasFailedWithMessage(failureMessage)
                }
                //                self.delegate?.visitDidDone(notification.visitID)
            }
            //            else {
            //                let message = "[PUSH N]: handlePushNotification: Can't parse data: \(data)"
            //                print(message)
            //            }
        }
        
        
    }
    
    func sendTokenToServer(token: String)
    {
        server.registerPushToken(token, result: { result in
            switch result {
            case .Success(_):
                print("Push token did registred")
            case .Failure(let error):
                print("Push token did fail to register with error \(error)")
            }
        })
    }
    
    
    private func routeNotification(notification: NotificationDomainModel)
    {
        switch notification.type {
        case .Assigned:
            self.delegate?.visitDidAssignCaregiver(notification.visitID)
            
        case .Paid:
            if let delegate = self.delegate {
                self.notHandledNotificationID = nil
                delegate.visitDidPaid(notification.visitID)
            }
            else {
                self.notHandledNotificationID = notification.visitID
            }
        case .Done:
            self.delegate?.visitDidDone(notification.visitID)
        default:
            break
        }
    }
}

//
//  NotificationParser.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 30.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

extension  NotificationDomainModel  {
    
    class func initFromResponse(data: [NSObject:AnyObject]) -> NotificationDomainModel? {
        
        if let tmID = data["team_member_id"] as? Int
            ,let visitID = data["visit_id"] as? Int
            ,let type_raw = data["type"] as? Int
            ,let type = VisitStatus(rawValue: UInt(type_raw))
        {
            return NotificationDomainModel(teamMemberID: tmID, visitID: visitID, type: type)
        }
    
        
        return nil
        
    }

}

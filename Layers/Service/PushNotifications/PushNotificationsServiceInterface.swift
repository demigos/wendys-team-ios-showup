//
// Created by Maksim Bazarov on 25.02.16.
// Copyright (c) 2016 Wendy'sTeam. All rights reserved.
//

import Foundation

protocol PushNotificationsServiceInterface: class
{
    
    func setupServer(server: ServerAPIInterface)
    func registerWithDelegate(delegate: PushNotificationsServiceDelegate?)
    func handleLaunchOptions(launchOptions: [NSObject: AnyObject]?)
    func handlePushNotification(data: [NSObject:AnyObject])
    func checkNotification()
}

protocol PushNotificationsServiceDelegate: class
{
    func paidWasFailedWithMessage(message : String?)
    func visitDidAssignCaregiver(visitID: Int)
    func visitDidPaid(visitID: Int)
    func visitDidDone(visitID: Int)
    func visitDidDoneWithFailureMessage(failureMessage : String?, visitID: Int)
    func showUnpaidVisits()
}
//
// Created by Maksim Bazarov on 25.02.16.
// Copyright (c) 2016 Wendy'sTeam. All rights reserved.
//

import Foundation
import Result
import ReactiveCocoa

protocol ServerAPIDelegate: class
{
    func authErrorDidHappened(error: WTError)
}

protocol ServerAPIInterface : class
{

    weak var delegate: ServerAPIDelegate? {get set}
    // MARK: - Authorization

    func signUp(user: UserDomainModel, result: (Result<AuthToken, WTError>) -> ())
    func checkCode(code: String, result: (Result<AuthToken, WTError>) -> ())
    func createPassword(code: String, password: String, result: (Result<AuthToken, WTError>) -> ())
    func auth(email email: String, password: String, result: (Result<AuthToken, WTError>) -> ())
    func auth(facebook token: String, result: (Result<AuthToken, WTError>) -> ())
    func restorePassword(email: String, result: (Result<NoData, WTError>) -> ())
    func logout(result: (Result<NoData, WTError>) -> ())


    //MARK: - Service

    func registerPushToken(token: String, result: (Result<NoData, WTError>) -> ())
    func checkZIPCode(zipcode: String, result: (Result<NoData, WTError>) -> ())


    // MARK: - Customers
    func loadCustomers(result: (Result<[CustomerDomainModel], WTError>) -> ())
    func updateCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    func createCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    func deleteCustomer(customer: CustomerDomainModel, result: (Result<NoData, WTError>) -> ())


    //MARK: - Visits
    func loadVisits(forCustomerID customer: String, result: (Result<[VisitDomainModel], WTError>) -> ())
    func loadVisits(result: (Result<[VisitDomainModel], WTError>) -> ())
    func loadUnpaidVisits(result: (Result<[VisitDomainModel], WTError>) -> ())
    func loadUnpaidAndNoFeedbackVisits(result: (Result<UnpaidNoFeedbackVisitsDomainModel, WTError>) -> ())
    func createVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    func feedbackForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    func payForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    func payForVisits(visits: [VisitDomainModel], result: (Result<NoData, WTError>) -> ())
    func loadVisit(visitID: UInt, result: (Result<VisitDomainModel, WTError>) -> ())
    func updateVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    func cancelVisit(visit: VisitDomainModel, charge: Bool?, result: (Result<CancellationResultDomainModel, WTError>) -> ())


    //MARK: - Cards
    func loadCards(result: (Result<[PaymentCardDomainModel], WTError>) -> ())
    func createCard(card: PaymentCardDomainModel, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    func removeCard(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    func makeCardPrimary(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())

}



//
// Created by Maksim Bazarov on 17.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Alamofire



protocol HTTPTransportDelegate: class
{
    func authErrorDidHappened(error: WTError)
}

class HTTPTransport
{
    weak var delegate: HTTPTransportDelegate?
    static let manager: Alamofire.Manager = {
        var manager : Alamofire.Manager
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPShouldSetCookies = false
        configuration.timeoutIntervalForRequest = 15
        configuration.timeoutIntervalForResource = 15
        manager = Alamofire.Manager(configuration: configuration)
        return manager
    }()

    func runRequest(URLRequest: WSURLRequest, result: (Result<NSData, WTError>) -> ())
    {
        let apiRequest = HTTPTransport.manager.request(URLRequest)

        let requestDescription = makePrintableRequest(URLRequest)
        print("\n>>> " + requestDescription)

        apiRequest.validate()
        apiRequest.response { (request, response, data, error) in
            if let body = request?.HTTPBody, bodyString = NSString(data: body, encoding: NSUTF8StringEncoding) {
                print(bodyString)
            }
            let code = response?.statusCode ?? 0
            print("Status code: \(code)")
            print("<<< " + self.makeSmallPrintableRequest(URLRequest))
            self.log(data: data)

            if let _ = error {
                let serverError = self.parseError(data, error: error)
                result(.Failure(serverError))
                return
            }

            if let data = data where error == nil {
                result(.Success(data))
            } else {
                result(.Failure(self.errorNoData()))
            }

        }
    }



    func parseError(data: NSData?, error: NSError? = nil) -> WTError
    {
        if data == nil || data?.length == 0 {
            if let e = error {
                return WTError(error: e)
            }
        }
        guard let data = data else {
            return errorNoData()
        }
        if let errors = WTError(data: data) {
            checkAuthError(errors)
            return errors
        } else {
            return errorNoData()
        }

    }

    func checkAuthError(error: WTError)
    {
        let authErrors = error.errors.filter({$0.code == ServerAPIError.authError.info.code})
        if authErrors.count > 0 {
            delegate?.authErrorDidHappened(error)
        }
    }



    // MARK: Private

    func errorNoData() -> WTError
    {
        return WTError(domain: "Wendys Team", code: 83839, userInfo: [NSLocalizedDescriptionKey:"Server returned no data"])
    }

    func log(data data: NSData?)
    {
        guard let safeData = data else {
            return
        }
        let string = safeData.prettyJson() ?? NSString(data: safeData, encoding: NSUTF8StringEncoding) ?? ""
        print (string)
    }

    func log(response response: AnyObject?, request: WSURLRequest)
    {
        let request = makePrintableRequest(request)
        let message = "\nSuccess responce --- \n\nRequest : \(request) \n\nResponse: \(response)"

        print (message)

    }

    func log(error error: WTError, request: WSURLRequest )
    {
        let request = makePrintableRequest(request)
        let message = "\n! ERROR :\n\nRequest: \(request) \n\nDescription: \(error.errorDescription) \n ---"
        print (message)

    }

    func makeSmallPrintableRequest(request: WSURLRequest) -> String {
        var str = "\(request.method.rawValue) "

        if let url = request.URLRequest.URL {
            str += " \(url)"
        } else {
            str += "URL WAS NOT SET!"
        }
        return str
    }

    func makePrintableRequest(request: WSURLRequest) -> String
    {

        var str = "\(request.method.rawValue) "

        if let url = request.URLRequest.URL {
            str += " \(url)"
        } else {
            str += "URL WAS NOT SET!"
        }

        if let headers = request.URLRequest.allHTTPHeaderFields where headers.count > 0
        {
            headers.forEach {
                (key, value) in
                str += "\n\(key): \(value)"
            }
        }

        if let params = request.parameters
        {
            str += "\n PARAMETERS: \(params)"
        }

        return str

    }

}

extension NSData {

    func prettyJson() -> String? {
        do {
            let jsonData = try NSJSONSerialization
            .JSONObjectWithData(self, options: NSJSONReadingOptions())
            let data = try NSJSONSerialization.dataWithJSONObject(jsonData, options: .PrettyPrinted)
            let string = String(data: data, encoding: NSUTF8StringEncoding)
            return string
        } catch {
            return ""
        }
    }

}
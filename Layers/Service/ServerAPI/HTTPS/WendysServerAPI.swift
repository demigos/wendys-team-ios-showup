//
// Created by Maksim Bazarov on 25.02.16.
// Copyright (c) 2016 Wendy'sTeam. All rights reserved.
//

import Foundation
import Result
import ReactiveCocoa

/// Configuration
let unexpectedError = ServerAPIError.wrongJSON.error

class WendysServerAPI :
    ServerAPIInterface
    , HTTPTransportDelegate
{
    let auth: AuthorizationServiceInterface!
    private let transport : HTTPTransport

    init(auth: AuthorizationServiceInterface)
    {
        self.auth = auth
        self.transport = HTTPTransport()
        self.transport.delegate = self
        auth.server = self
    }

    // MARK: Interface
    weak var delegate: ServerAPIDelegate?
    let stripe: PaymentServiceInterface = StripeService()

    // MARK: - Authorization

    func signUp(user: UserDomainModel, result: (Result<AuthToken, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.signUp(user: user)
        request(URLRequest, result: result)
    }

    func restorePassword(email: String, result: (Result<NoData, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.resetPassword(email: email)
        request(URLRequest, result: result)
    }

    func checkCode(code: String, result: (Result<AuthToken, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.checkCode(code: code)
        request(URLRequest, result: result)
    }
    
    func createPassword(code: String, password: String, result: (Result<AuthToken, WTError>) -> ()) {
        let URLRequest = WSURLRequest.createPassword(code: code, password: password)
        request(URLRequest, result: result)
    }
    
    func auth(email email: String, password: String, result: (Result<AuthToken, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.authEmail(email: email, password: password)
        request(URLRequest, result: result)
    }

    func auth(facebook token: String, result: (Result<AuthToken, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.authFacebook(token: token)
        request(URLRequest, result: result)
    }

    func logout(result: (Result<NoData, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.Logout()
        request(URLRequest, result: result)
    }

    //MARK: - Service

    func registerPushToken(token: String, result: (Result<NoData, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.registerAPNSToken(token: token)

        request(URLRequest, result: result)
    }

    func checkZIPCode(zipcode: String, result: (Result<NoData, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.CheckZipCode(zipcode: zipcode)
        request(URLRequest, result: result)
    }


    // MARK: - Customers
    func loadCustomers(result: (Result<[CustomerDomainModel], WTError>) -> ())
    {
        let URLRequest = WSURLRequest.CustomersList()
        request(URLRequest, result: result)
    }

    func updateCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.UpdateCustomer(customer: customer, image: image)
        request(URLRequest, result: result)
    }

    func createCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.CreateCustomer(customer: customer, image: image)
        request(URLRequest, result: result)
    }

    func deleteCustomer(customer: CustomerDomainModel, result: (Result<NoData, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.DeleteCustomer(customer: customer)
        request(URLRequest, result: result)
    }


    //MARK: - Visits
    func loadVisits(forCustomerID customer: String, result: (Result<[VisitDomainModel], WTError>) -> ())
    {
        let URLRequest = WSURLRequest.VisitsList(forCusomerID: customer)
        request(URLRequest, result: result)
    }

    func loadVisits(result: (Result<[VisitDomainModel], WTError>) -> ())
    {
        let URLRequest = WSURLRequest.VisitsListAll()
        request(URLRequest, result: result)
    }

    func loadUnpaidVisits(result: (Result<[VisitDomainModel], WTError>) -> ())
    {
        let URLRequest = WSURLRequest.UnpaidVisitsList()
        request(URLRequest, result: result)
    }
    
    func loadUnpaidAndNoFeedbackVisits(result: (Result<UnpaidNoFeedbackVisitsDomainModel, WTError>) -> ()) {
        let URLRequest = WSURLRequest.UnpaidNoFeedbackVisitsList()
        request(URLRequest, result: result)
    }

    func createVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.CreateVisit(visit: visit)
        request(URLRequest, result: result)
    }


    func feedbackForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    {

        let f = visit.feedback ?? ""
        let r = visit.rating ?? 0

        let URLRequest = WSURLRequest.FeedbackForVisit(visitID: UInt(visit.id), feedback: f, rating: r)
        request(URLRequest, result: result)

    }


    func payForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    {
        let f = visit.feedback ?? ""
        let r = visit.rating ?? 0
        let URLRequest = WSURLRequest.PayForVisit(visitID: UInt(visit.id), feedback: f, rating: r)
        request(URLRequest, result: result)
    }

    func payForVisits(visits: [VisitDomainModel], result: (Result<NoData, WTError>) -> ())
    {
        var visits_arr = [UInt]()
        for visit in visits {
            visits_arr.append(UInt(visit.id))
        }
        let URLRequest = WSURLRequest.PayForVisits(visits: visits_arr)
        request(URLRequest, result: result)
    }


    func loadVisit(visitID: UInt, result: (Result<VisitDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.Visit(visitID: visitID)
        request(URLRequest, result: result)
    }

    func updateVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.UpdateVisit(visit: visit)
        request(URLRequest, result: result)
    }

    func cancelVisit(visit: VisitDomainModel, charge: Bool?, result: (Result<CancellationResultDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.CancelVisit(visitID: UInt(visit.id), charge: charge)
        request(URLRequest, result: result)
    }


    //MARK: - Cards
    func loadCards(result: (Result<[PaymentCardDomainModel], WTError>) -> ())
    {
        let URLRequest = WSURLRequest.CardsList()
        request(URLRequest, result: result)
    }

    func createCard(card: PaymentCardDomainModel, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    {
        var card = card

        stripe.registerCard(card) { stripeResult in
            switch stripeResult {
            case .Success(let token):
                card.stripeToken = token
                let URLRequest = WSURLRequest.CreateCard(card: card)
                self.request(URLRequest, result: result)
            case .Failure(let error):
                result(.Failure(WTError(error: error)))
            }
        }
    }

    func removeCard(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.RemoveCard(cardID: cardID)
        request(URLRequest, result: result)
    }

    func makeCardPrimary(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    {
        let URLRequest = WSURLRequest.MakeCardPrimary(cardID: cardID)
        request(URLRequest, result: result)
    }


    // MARK: Transport delegate

    func authErrorDidHappened(error: WTError) {
        delegate?.authErrorDidHappened(error)

    }

    // MARK: Helpers
    private func request<T : Parsable> (request: WSURLRequest, result: (Result<T, WTError>) -> ())
    {
        transport.runRequest(request, result: { requestResult in
            switch requestResult {

            case .Success(let data):
                print(T)
                if let value = T.init(data: data) {
                    result(.Success(value))
                } else {
                    result(.Failure(unexpectedError))
                }
            case .Failure(let error):
                result(.Failure(error))
            }
        })
    }


}

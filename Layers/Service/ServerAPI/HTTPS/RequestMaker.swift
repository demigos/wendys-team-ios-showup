//
// Created by Maksim Bazarov on 17.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Alamofire

enum WSURLRequest: URLRequestConvertible {

    //MARK: - 1) API Methods list -
    // Add new method here first

    //MARK: Authorization
    case signUp(user:UserDomainModel)
    case checkCode(code:String)
    case createPassword(code: String, password: String)
    case authEmail(email:String, password:String)
    case resetPassword(email:String)
    case authFacebook(token:String)
    case registerAPNSToken(token:String)
    case Logout()

    //MARK: Customers
    case CustomersList()
    case PrimaryCustomer()
    case CreateCustomer(customer:CustomerDomainModel, image:NSData?)
    case UpdateCustomer(customer:CustomerDomainModel, image:NSData?)
    case DeleteCustomer(customer:CustomerDomainModel)

    //MARK: Visits
    case CreateVisit(visit:VisitDomainModel)
    case VisitsList(forCusomerID:String)
    case VisitsListAll()
    case UnpaidVisitsList()
    case UnpaidNoFeedbackVisitsList()
    case FeedbackForVisit(visitID:UInt, feedback:String, rating:Int)
    case Visit(visitID:UInt)
    case PayForVisit(visitID:UInt, feedback:String, rating:Int)
    case PayForVisits(visits:[UInt])
    case CancelVisit(visitID:UInt, charge:Bool?)
    case UpdateVisit(visit:VisitDomainModel)

    //MARK: Payments
    case CardsList()
    case MakeCardPrimary(cardID:Int)
    case CreateCard(card:PaymentCardDomainModel)
    case RemoveCard(cardID:Int)

    // MARK: Misc.
    case CheckZipCode(zipcode:String)


    //MARK: - URLRequest - computed -
    var URLRequest: NSMutableURLRequest {
        let request = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = method.rawValue
        request.setValue(AuthService.token(), forHTTPHeaderField: header_authorization)
        let encoding = Alamofire.ParameterEncoding.JSON
        return encoding.encode(request, parameters: parameters).0
    }

    //MARK: - 2) API Methods type (post/get..etc.) -
    // Define api method type
    var method: Alamofire.Method {
        switch self {


                //MARK: POST
        case .signUp(_), .resetPassword(_), .checkCode(_), .createPassword(_, _), .authEmail(_, _), .authFacebook(_), .registerAPNSToken(_), .Logout(),
             .CreateCustomer(_),
             .CreateVisit(_), .CancelVisit(_, _), .PayForVisit(_), .PayForVisits(_),
             .CreateCard, .MakeCardPrimary(_),
             .CheckZipCode(_):
            return .POST

                //MARK: PUT
        case .UpdateCustomer(_), .UpdateVisit(_), .FeedbackForVisit(_, _, _):
            return .PUT

                //MARK: DELETE
        case .RemoveCard(_),
             .DeleteCustomer(_):
            return .DELETE

                //MARK: GET
        default: return .GET
        }
    }


    //MARK: - 3) URL of method
    // Define api method URL

    var URL: NSURL {
        switch self {

                //MARK: Authorization
        case signUp(_): return NSURL(string: API_base_path + "/auth/registration/")!
        case checkCode(_): return NSURL(string: API_base_path + "/auth/email_confirm/")!
        case createPassword(_, _): return NSURL(string: API_base_path + "/auth/password/reset/confirm/")!
        case registerAPNSToken(_): return NSURL(string: API_base_path + "/device/apns/")!
        case authEmail(_, _): return NSURL(string: API_base_path + "/auth/login/")!
        case resetPassword(_): return NSURL(string: API_base_path + "/auth/password/reset/")!
        case authFacebook(_): return NSURL(string: API_base_path + "/auth/facebook/")!
        case Logout(): return NSURL(string: API_base_path + "/auth/logout/")!

                //MARK: Customers
        case CustomersList(): return NSURL(string: API_base_path + "/clients/")!
        case PrimaryCustomer():  return NSURL(string: API_base_path + "/client-profile/")!
        case CreateCustomer(_): return NSURL(string: API_base_path + "/clients/")!
        case UpdateCustomer(let customer, _): return NSURL(string: API_base_path + "/clients/\(customer.id)/")!
        case DeleteCustomer(let customer): return NSURL(string: API_base_path + "/clients/\(customer.id)/")!
                //MARK: Visits

        case VisitsList(let customerID): return NSURL(string: API_base_path + "/visits/client/\(customerID)/")!
        case VisitsListAll(): return NSURL(string: API_base_path + "/visits/")!
        case UnpaidVisitsList(): return NSURL(string: API_base_path + "/visits/unpaid/")!
        case UnpaidNoFeedbackVisitsList(): return NSURL(string: API_base_path + "/visits/unpaidnofeedback/")!
        case CreateVisit(let visit): return NSURL(string: API_base_path + "/visits/client/\(visit.customerID!)/")!
        case FeedbackForVisit(let visitID, _, _): return NSURL(string: API_base_path + "/visits/\(visitID)/feedback/")!
        case PayForVisit(let visitID, _, _): return NSURL(string: API_base_path + "/visits/\(visitID)/pay/")!
        case PayForVisits(_): return NSURL(string: API_base_path + "/visits/pay/")!
        case CancelVisit(let visitID, _): return NSURL(string: API_base_path + "/visits/\(visitID)/cancel/")!
        case Visit(let visitID): return NSURL(string: API_base_path + "/visits/\(visitID)/")!
        case UpdateVisit(let visit): return NSURL(string: API_base_path + "/visits/\(visit.id)/")!

                //MARK: Payments
        case CreateCard(_): return NSURL(string: API_base_path + "/paymethods/")!
        case MakeCardPrimary(let cardID): return NSURL(string: API_base_path + "/paymethods/\(cardID)/primary/")!
        case RemoveCard(let cardID): return NSURL(string: API_base_path + "/paymethods/\(cardID)/")!
        case CardsList(): return NSURL(string: API_base_path + "/paymethods/")!
                //MARK: Misc.
        case CheckZipCode(_): return NSURL(string: API_base_path + "/zipcode_check/")!

        }
    }

    //MARK: - PARAMETERS -
    var parameters: [String:AnyObject]? {
        var parameters = [String: AnyObject]()
        switch self {

                //MARK: Authorization
                //TODO: Refactor
        case .signUp(let user):
            parameters[request_name] = user.name;
            parameters[request_email] = user.email;
            parameters[request_password1] = user.password;
            parameters[request_gender] = user.gender?.rawValue

        case .checkCode(let code):
            parameters[request_code] = code
        case .createPassword(let code, let password):
            parameters[request_code] = code
            parameters[request_password] = password

        case .authEmail(let email, let password):
            parameters[request_email] = email
            parameters[request_password] = password

        case .resetPassword(let email):
            parameters[request_email] = email

        case .authFacebook(let token):
            parameters[request_fb_token] = token

        case .registerAPNSToken(let token):
            parameters[request_apns_token] = token

        case .Logout():return nil

                //MARK: Customers
        case .CustomersList():return nil
        case .PrimaryCustomer():return nil
        case .CreateCustomer(let customer, let image): return customer.createCustomerParams(image)
        case .UpdateCustomer(let customer, let image): return customer.updateCustomerParams(image)
        case .DeleteCustomer(_):return nil
                //MARK: Visits
        case .CreateVisit(let visit): return visit.createVisitParams()
        case .VisitsList(_): return nil
        case .VisitsListAll(): return nil
        case .UnpaidVisitsList(): return nil
        case .UnpaidNoFeedbackVisitsList(): return nil
        case .FeedbackForVisit(_, let feedback, let rating): return VisitDomainModel.feedbackParams(feedback, rating: rating)
        case .PayForVisit(_, let feedback, let rating): return VisitDomainModel.feedbackParams(feedback, rating: rating)
        case .PayForVisits(let visits): parameters["visits"] = visits
        case .CancelVisit(_, let charge):
            if let charge = charge {
                parameters["charge"] = charge
            } else {
                return nil
            }

        case .Visit(_): return nil
        case .UpdateVisit(let visit): return visit.updateVisitParams()

                //MARK: Payments
        case CardsList(): return nil
        case MakeCardPrimary(_): return nil
        case CreateCard(let card): return card.createCardParams()
        case RemoveCard(_): return nil
                //MARK: Misc.
        case .CheckZipCode(let zipcode): parameters["zipcode"] = zipcode

        }
        return parameters
    }
}
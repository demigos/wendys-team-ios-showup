//
// Created by Maksim Bazarov on 09.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

enum ServerAPIError
{
    case authError
    case wrongJSON
    
    var domain : String {
        return "WendysServer"
    }
    
    var info : (code: Int ,description:  String) {
        switch self {
        
        case authError: return (900, "Authorization error")
        case wrongJSON: return (87001 , "Unexpected json")
            
        }
    }
    
    var userInfo : [NSObject : AnyObject]? {
        return nil
    }
    
    var error : WTError {
        return WTError(domain: domain, code: info.code, userInfo: [NSLocalizedDescriptionKey:info.description])
    }
}
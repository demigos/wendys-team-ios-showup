//
//  PhoneService.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 25.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class PhoneService
{
    class func call()
    {
        UIApplication.sharedApplication().openURL(NSURL(string: "tel://7194259572")!)
    }

}

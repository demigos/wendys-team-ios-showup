//
// Created by Maksim Bazarov on 14.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

extension CancellationResultDomainModel : Parsable
{

    init?(json: JSON)
    {
        guard let cancelled = json["cancelled"].bool else { return nil }
        
        self.init(canceled: cancelled)
    
        if let amount = json["amount_to_pay"].int {
            self.amount = amount
        }

        if let message = json["message"].string {
            self.message = message
        }

    }

}
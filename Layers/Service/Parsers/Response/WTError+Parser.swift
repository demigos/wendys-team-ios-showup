//
//  WTError+Parser.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 15/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit


extension WTError {
    
    convenience init?(data: NSData)
    {
        
        let json = JSON(data: data)
        
        if json.count <= 0 { return nil }
        
        var data : [JSON]
        
        if let array = json.array {
            data = array
        } else if let array = json["errors"].array {
            data = array
        } else {
            return nil
        }
        
        let errors = data.flatMap { (json) -> NSError? in
            return (NSError(json: json))
        }
        
        self.init()
        self.errors = errors
    }
    

}


extension NSError {
    
    convenience init?(json: JSON) {
        let descriptionText = json["desc"].string ?? json["desc"]["message"].string
        if let domain = json["domain"].string, code = json["code"].int, text = descriptionText {
            self.init(domain: domain, code: code, userInfo: [NSLocalizedDescriptionKey: text])
        } else {
            return nil
        }
        
    }
}

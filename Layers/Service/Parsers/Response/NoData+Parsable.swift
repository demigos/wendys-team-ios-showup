//
//  NoData+Parsable.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 15/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//


extension NoData : Parsable
{
    init?(json: JSON)
    {
        self.init(0) // always not nil
    }
}

//
//  CardParser.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 25.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

extension PaymentCardDomainModel : Parsable
{
    
    init?(json: JSON)
    {
        self.init()
        
        if let id = json[response_id].int
        {
            self.id = id
        }
        
        if let customerID = json[response_account].int  {
            self.customerID = customerID
        }
        
        if let isPrimary = json[response_is_primary].bool {
            self.isPrimary = isPrimary
        }
        
        if let brand = json[response_brand].string  {
            self.cardEmitter = brand
        }

        
        if let name = json[response_name].string  {
            self.cardholderName = name
        }
       
        if let last4 = json[response_last4].string  {
            self.last4 = last4
        }
        
        if let exp_month = json[response_exp_month].int  {
            self.expirationMonth = UInt(exp_month)
        }
        
        if let exp_year = json[response_exp_year].int  {
            self.expirationYear = UInt(exp_year)
        }
        
        if let token = json[response_token].string  {
            self.stripeToken = token
        }
    }
}
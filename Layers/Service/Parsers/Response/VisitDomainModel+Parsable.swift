//
// Created by Maksim Bazarov on 14.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

extension VisitDomainModel : Parsable
{

    init?(json: JSON)
    {


        if let id = json["id"] .int
        {
            self.init()
            self.id = id
        } else {
            return nil
        }


        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let dateString = json["date"] .string,
            let date = dateFormatter.dateFromString(dateString)
        {
            self.date = date

        }


        if let actual_duration = json["actual_duration"] .string
        {
            dateFormatter.dateFormat = "HH:mm:ss"
            let time = dateFormatter.dateFromString(actual_duration)
            self.actualDuration = time
        }



        if let feedback = json["feedback"] .string
        {
            self.feedback = feedback
        }

        if let caregiverID = json["team_member_id"] .int
        {
            self.caregiverID = caregiverID
        }


        if let customerID = json["client"] .string
        {
            self.customerID = customerID
        }
        
        if let errorMessage = json["exception"] .string
        {
            self.failureMessage = errorMessage
        }

        if let status = json["status"] .int
        {
            self.status = VisitStatus(rawValue: UInt(status))
        }

        if let address = json["address"] .string
        {
            self.address = address
        }

        if let phone = json["phone"] .string
        {
            self.phone = phone
        }

        if let name = json["name"] .string
        {
            self.customerName = name
        }
        
//        if let failMsg = json[""]

        if let cost = json["amount_to_pay"].float
        {
            self.cost = cost
        }

        if let zipcode = json["zipcode"] .string
        {
            self.zipCode = String(zipcode)
        }

        if let rating = json["rating"] .int
        {
            self.rating = rating
        }

        if let services = json["services"].array
        {
            for s in services where s.int != nil{
                if let service = CareServiceDomainModel(rawValue: s.int!) {
                    self.services.append(service)
                }
            }
        }

        if let duration = json["duration"] .int
        {
            self.duration = duration
        }


        if let team_member_name = json["team_member_name"] .string
        {
            self.teamMemberName = team_member_name
        }


        if let team_member_thumb = json["team_member_thumb"] .string where team_member_thumb.isEmpty == false
        {
            self.teamMemberAvatar = media_base_path + team_member_thumb
        }

        if let customer_thumb = json["client_thumb"] .string where customer_thumb.isEmpty == false
        {
            self.customerAvatar = media_base_path + customer_thumb
        }

    }
}
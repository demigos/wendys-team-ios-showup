import Foundation

extension UnpaidNoFeedbackVisitsDomainModel : Parsable {
    
    init?(json: JSON) {
        self.init()
        if let unpaid = json["unpaid"].array
        {
            let unpaid1 = unpaid.flatMap { (json) -> VisitDomainModel? in
                return (VisitDomainModel(json: json))
            }
            self.unpaid = unpaid1
            

        }
        if let nofeedback = json["nofeedback"].array
        {
            let nofeed1 = nofeedback.flatMap { (json) -> VisitDomainModel? in
                return (VisitDomainModel(json: json))
            }
            self.nofeedback = nofeed1
            
            
        }
    }
    
}

//
// Created by Maksim Bazarov on 14.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

extension CustomerDomainModel: Parsable {
    init?(json: JSON) {
        if let id = json["id"].int, name = json["name"].string {
            self.init()
            self.id = id
            self.name = name

        } else {
            return nil
        }

        if let email = json["email"].string {
            self.email = email
        }

        if let thumb = json["thumb"].string where thumb.isEmpty == false {
            self.thumb = media_base_path + thumb
        }

        if let zipcode = json["zipcode"].string {
            self.zipcode = zipcode
        }
        if let address = json["address"].string {
            self.address = address
        }
        if let phone = json["phone"].string {
            self.phone = phone
        }
        if let visits_passed = json["visits_passed"].int {
            self.visits_passed = visits_passed
        }
        if let visits_upcoming = json["visits_upcoming"].int {
            self.visits_upcoming = visits_upcoming
        }

        if let isOwner = json["is_owner"].bool {
            self.isOwner = isOwner
        }

        if let email = json["email"].string {
            self.email = email
        }

        if let gender = json["gender"].int {
            self.gender = gender
        }

    }
}



//
//  AuthToken+Parsable.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 15/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

extension AuthToken: Parsable
{
    init?(json: JSON)
    {
        if let token = json[response_token].string {
            self.init(token)    
        } else if let token = json[response_key].string {
            self.init(token)
        } else {
            return nil
        }
        
    }
    
}

//
//  FeedbackResponse.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 28.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

struct FeedbackResponse : Parsable
{
    var feedback : String?
    var rating : Int?

    init?(json: JSON)
    {
        if let feedback = json["feedback"].string {
            self.feedback = feedback
        }
        
        if let rating = json["rating"].int {
            self.rating = rating
        }
        
    }
    
}

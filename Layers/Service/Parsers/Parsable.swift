//
//  Parsable.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 15/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

protocol Parsable {
    init?(data: NSData)
    init?(json: JSON)
}

extension Parsable {
    init?(data: NSData) {
        self.init(json: JSON(data: data))
    }
}

extension Array: Parsable  {
    
    init?(json: JSON) {
        guard let type = Element.self as? Parsable.Type else { return nil }
        
        guard let collection = json.array else { return nil }
        
        self.init()
        
        for json in collection {
            if let element = type.init(json: json) {
                append(element as! Element)
            }
        }
    }
}

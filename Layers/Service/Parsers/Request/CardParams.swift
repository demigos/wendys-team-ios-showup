//
//  CardParams.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 25.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

import Foundation

extension PaymentCardDomainModel {
    
    func createCardParams() -> [String:AnyObject]? {
        var params = [String:AnyObject]()
        
        if let primary = isPrimary {
            params[request_isPrimary] = primary
        } 
        
        if let last4 = makeLast4() {
            params[request_last4] = last4
        }
        
        if let token = stripeToken {
            params[request_token] = token
        }
        
        if let name = cardholderName {
            params[request_name] = name
        }
        
        if let exp_month = expirationMonth {
            params[request_exp_month] = exp_month
        }
        
        if let exp_year = expirationYear {
            params[request_exp_year] = exp_year
        }
        
        return params
    }
    
}

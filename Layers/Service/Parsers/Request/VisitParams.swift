//
//  selfParams.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 21.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

extension VisitDomainModel {
    
    func createVisitParams() -> [String:AnyObject]? {
        var params = [String:AnyObject]()
        var services: [String] = []
        
        for service in self.services {
            services.append((service.rawValue).description)
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = date {
            params[request_date] = dateFormatter.stringFromDate(date)
        }
        
        
        params[request_services] = services.joinWithSeparator(",")

        
        params[request_duration] = self.duration
        params[request_zipcode] = self.zipCode
        params[request_address] = self.address
        params[request_phone] = self.phone
        return params
    }
    
    func updateVisitParams() -> [String:AnyObject]? {
         return createVisitParams()
    }
    

    static func feedbackParams(feedback: String, rating: Int) -> [String:AnyObject]?
    {
        var params = [String:AnyObject]()
        params[request_feedback] = feedback
        params[request_rating] = rating
        return params
        
    }
    
    
}
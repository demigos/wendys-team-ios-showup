//
//  CustomerParams.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 21.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

extension CustomerDomainModel {
    
    func createCustomerParams(image: NSData?) -> [String:AnyObject]? {
        var params = [String:AnyObject]()
        if let name = self.name {
            params[request_name] = name;
        }
        
        if let zipcode = self.zipcode {
            params[request_zipcode] = zipcode;
        }
        
        if let address = self.address {
            params[request_address] = address;
        }
        
        if let phone = self.phone {
            params[request_phone] = phone;
        }
        
        if let image = image {
            let strBase64 = image.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            params[request_image] = strBase64;
        }
        
        if let gender = self.gender {
            params[request_gender] = gender
        }
        
        return params
    }
    
    func updateCustomerParams(image: NSData?) -> [String:AnyObject]? {
        return createCustomerParams(image)
    }
}
import Foundation
import Reachability

class NetworkService {
    static let networkReachable = "networkReachable"
    static let networkNotReachable = "networkNotReachable"

    static let sharedInstance = NetworkService()

    var reachability: Reachability?

    func setupReachability() {
        do {
            let reachability = try Reachability.reachabilityForInternetConnection()

            reachability.whenReachable = {
                reachability in
                dispatch_async(dispatch_get_main_queue()) {
                    self.updateNetworkState()
                }
            }
            reachability.whenUnreachable = {
                reachability in
                self.updateNetworkState()
            }

            self.reachability = reachability
            try reachability.startNotifier()
        } catch {
            print("Unable to start reachability")
        }
    }

    func updateNetworkState() {
        if let reachable = self.reachability?.isReachable() {
            if !reachable {
                NSNotificationCenter.defaultCenter().postNotificationName(NetworkService.networkNotReachable, object: nil)
                return
            }
            NSNotificationCenter.defaultCenter().postNotificationName(NetworkService.networkReachable, object: nil)
        }
    }

    func reachabilityChanged(note: NSNotification) {
        updateNetworkState()
    }

}

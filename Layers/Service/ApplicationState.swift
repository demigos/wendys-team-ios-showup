import Foundation

class ApplicationState {

    class func saveCustomer(customer: CustomerDomainModel) {
        NSUserDefaults.standardUserDefaults().setObject(customer.id, forKey: "owner.id")
        NSUserDefaults.standardUserDefaults().setObject(customer.name, forKey: "owner.name")
        NSUserDefaults.standardUserDefaults().setObject(customer.address, forKey: "owner.address")
        NSUserDefaults.standardUserDefaults().setObject(customer.phone, forKey: "owner.phone")
        NSUserDefaults.standardUserDefaults().setObject(customer.thumb, forKey: "owner.thumb")
        NSUserDefaults.standardUserDefaults().setObject(customer.zipcode, forKey: "owner.zipcode")
        NSUserDefaults.standardUserDefaults().setObject(customer.email, forKey: "owner.email")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    class func loadCustomer() -> CustomerDomainModel? {
        let id = (NSUserDefaults.standardUserDefaults().objectForKey("owner.id") as? Int) ?? 0
        let name = NSUserDefaults.standardUserDefaults().objectForKey("owner.name") as? String
        let address = NSUserDefaults.standardUserDefaults().objectForKey("owner.address") as? String
        let phone = NSUserDefaults.standardUserDefaults().objectForKey("owner.phone") as? String
        let thumb = NSUserDefaults.standardUserDefaults().objectForKey("owner.thumb") as? String
        let zipcode = NSUserDefaults.standardUserDefaults().objectForKey("owner.zipcode") as? String
        let email = NSUserDefaults.standardUserDefaults().objectForKey("owner.email") as? String

        if name == nil || address == nil || phone == nil || zipcode == nil {
            return nil
        }

        return CustomerDomainModel(id: id, name: name, gender: nil, address: address, phone: phone, thumb: thumb, zipcode: zipcode, email: email, visits_passed: nil, visits_upcoming: nil, isOwner: true)
    }

    class func removeCustomer() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.id")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.name")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.address")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.phone")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.thumb")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.zipcode")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("owner.email")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

}

//
//  WendysServerAuthService.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 29.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Result

let NSUDTokenKey = "authtoken"

class AuthService: AuthorizationServiceInterface
        , ServerAPIDelegate
        , UserActivityDelegate {

    weak var delegate: AuthorizationServiceDelegate?
    weak var server: ServerAPIInterface!


    static func token() -> String? {
        var token: String?
        if let nsudValue = NSUserDefaults.standardUserDefaults().valueForKey(NSUDTokenKey) as? String {
            token = nsudValue
        }
//        WARNING: TEST!!!
//        return "Token e98e95c33aa4cfe40ebff30a162a0fe73da5ca20"
        return token
    }

    func authentificated() -> Bool {
        let token = AuthService.token()
        return (token != nil && token?.characters.count > 0)
    }

    func signUp(user: UserDomainModel, result: (Result<AuthToken, WTError>) -> ()) {
        server.signUp(user) {
            (signUpResult) in
            switch signUpResult {

            case .Success(let data):
                self.saveToken(data)
                result(.Success(data))

            case .Failure(let error):
                print("WendysServerAuthService: auth error: \(error)")
                result(.Failure(error))

            }
        }
    }

    func checkCode(code: String, result: (Result<AuthToken, WTError>) -> ()) {
        server.checkCode(code) {
            (checkCodeResult) in
            switch checkCodeResult {
            case .Success(let token):
                self.saveToken(token)
                result(.Success(token))

            case .Failure(let error):
                print("WendysServerAuthService: auth error: \(error)")
                result(.Failure(error))

            }
        }
    }

    func createPassword(code: String, password: String, result: (Result<AuthToken, WTError>) -> ()) {
        server.createPassword(code, password: password) {
            (createPasswordResult) in
            switch createPasswordResult {
            case .Success(let token):
                self.saveToken(token)
                result(.Success(token))
            case .Failure(let error):
                print("WendysServerAuthService: auth error: \(error)")
                result(.Failure(error))

            }
        }
    }

    func auth(email email: String, password: String, result: (Result<AuthToken, WTError>) -> ()) {
        server.auth(email: email, password: password) {
            (authResult) in switch authResult {

            case .Success(let token):
                self.saveToken(token)
                result(.Success(token))
            case .Failure(let error):
                print("WendysServerAuthService: auth error: \(error)")
                result(.Failure(error))

            }
        }
    }

    func auth(facebookToken token: String, result: (Result<AuthToken, WTError>) -> ()) {
        server.auth(facebook: token) {
            (authResult) in switch authResult {

            case .Success(let token):
                self.saveToken(token)
                result(.Success(token))
            case .Failure(let error):
                result(.Failure(error))

            }
        }

    }

    func restorePassword(email: String, result: (Result<NoData, WTError>) -> ()) {
        server.restorePassword(email) {
            (restoreResult) in switch restoreResult {

            case .Success(_):
                result(.Success(NoData()))
            case .Failure(let error):
                result(.Failure(error))
            }
        }

    }

    func logout(result: (Result<NoData, WTError>) -> ()) {
        server.logout {
            _ in
            result(.Success(NoData()))
            ApplicationState.removeCustomer()
        }

        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: NSUDTokenKey)

    }

    // MARK: Server API Delegate
    func authErrorDidHappened(error: WTError) {
        delegate?.authErrorDidHappened(error)
    }

    //MARK: - Private -

    private func saveToken(token: String?) {
        if let token = token {
            let tokenToSave = "Token \(token)"
            NSUserDefaults.standardUserDefaults().setValue(tokenToSave, forKey: NSUDTokenKey)
        }
    }

    // MARK: Activity delegate
    func confirmationCodeDidReceive(code: String) {
        if authentificated() {
            return
        }

        server.checkCode(code) {
            (checkCodeResult) in
            switch checkCodeResult {
            case .Success(let token):
                self.saveToken(token)
                self.delegate?.userDidLogin()
            case .Failure(let error):
                self.delegate?.authErrorDidHappened(error)
            }
        }
    }

    func resetCodeDidReceive(code: String) {
        if authentificated() {
            return
        }

        self.delegate?.resetCodeDidReceive(code)
    }

}

//
// Created by Maksim Bazarov on 29.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Result

protocol AuthorizationServiceDelegate: class {
    func userDidLogin()
    func authErrorDidHappened(error: WTError)
    func resetCodeDidReceive(code: String)
}

protocol AuthorizationServiceInterface : class
{

    weak var delegate: AuthorizationServiceDelegate? {get set}
    weak var server: ServerAPIInterface! {get set}

    static func token() -> String?
    func authentificated() -> Bool
    func signUp(user: UserDomainModel, result: (Result<AuthToken, WTError>) -> ())
    func checkCode(code: String, result: (Result<AuthToken, WTError>) -> ())
    func auth(email email: String, password: String, result: (Result<AuthToken, WTError>) -> ())
    func auth(facebookToken token: String, result: (Result<AuthToken, WTError>) -> ())
    func restorePassword(email: String, result: (Result<NoData, WTError>) -> ())
    func createPassword(code: String, password: String, result: (Result<AuthToken, WTError>) -> ())

    func logout(result: (Result<NoData, WTError>) -> ())
}

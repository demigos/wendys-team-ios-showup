//
//  Datasources.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 01/07/2016.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class Datasources: DatasourcesInterface
{
    var customers : CustomersDSInterface
    var visits : VisitsDSInterface
    var cards : CardsDSInterface
    
    init (server: ServerAPIInterface)
    {
        customers = CustomersDS(server: server)
        visits = VisitsDS(server: server)
        cards = CardsDS(server: server)
    }
}

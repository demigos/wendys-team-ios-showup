//
// Created by Maksim Bazarov on 11.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Result

class CustomersDS : CustomersDSInterface
{
    // MARK: Init
    private var cachedOwnerID : Int

    var server : ServerAPIInterface

    init(server: ServerAPIInterface)
    {
        self.server = server
        cachedOwnerID = 0
    }

    var  (customersListSignal, customersListObservers) = Signal<[CustomerDomainModel] , WTError>.pipe()
    var  (ownerSignal, ownerObservers) = Signal<CustomerDomainModel , WTError>.pipe()

    var customersListObserversList: [ReactiveCocoa.Observer<[CustomerDomainModel], WTError>] = []
    var ownerObserversList: [ReactiveCocoa.Observer<CustomerDomainModel, WTError>] = []

    //
    // MARK: - Interface

    var ownerID : Int {
        return cachedOwnerID
    }

    var customersListUpdate: Signal<[CustomerDomainModel], WTError> {
        (customersListSignal, customersListObservers) = Signal<[CustomerDomainModel] , WTError>.pipe()
        customersListObserversList.append(customersListObservers)
        return customersListSignal
    }

    var ownerUpdate: Signal<CustomerDomainModel, WTError> {
        (ownerSignal, ownerObservers) = Signal<CustomerDomainModel , WTError>.pipe()
        ownerObserversList.append(ownerObservers)
        return ownerSignal
    }

    func loadCustomers()
    {
        server.loadCustomers { (result) in
            switch result {
            case .Success(let customers):
                for item in self.customersListObserversList {
                    item.sendNext(customers)
                }
                if let owner = customers.filter({$0.isOwner}).first {
                    self.ownerObserversList.forEach {
                        $0.sendNext(owner)
                    }
                    self.cachedOwnerID = owner.id
                    ApplicationState.saveCustomer(owner)
                }

            case .Failure(let error):
                self.customersListObservers.sendFailed(error)
            }
        }
    }

    func updateCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    {
        server.updateCustomer(customer, image: image) { (_result) in
            switch _result {
            case .Success(let customer):
                result(.Success(customer))
                self.loadCustomers()
            case .Failure(let error):
                result(.Failure(error))
            }
        }
    }

    func createCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    {
        server.createCustomer(customer, image: image) { (_result) in
            switch _result {
            case .Success(let customer):
                result(.Success(customer))
                self.loadCustomers()
            case .Failure(let error):
                result(.Failure(error))
            }
        }
    }

    func deleteCustomer(customer: CustomerDomainModel, result: (Result<NoData, WTError>) -> ())
    {
        server.deleteCustomer(customer) { (_result) in
            switch _result {
            case .Success(let data):
                result(.Success(data))
                self.loadCustomers()
                break

            case .Failure(let error):
                result(.Failure(error))
                self.loadCustomers()
                break
            }
        }
    }

}

//
// Created by Maksim Bazarov on 11.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Result
import ReactiveCocoa

protocol CustomersDSInterface
{
    var ownerID : Int { get }
    var customersListUpdate : Signal<[CustomerDomainModel] , WTError>  { get }
    var ownerUpdate : Signal<CustomerDomainModel , WTError>  { get }

    func loadCustomers()
    func updateCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    func createCustomer(customer: CustomerDomainModel, image: NSData?, result: (Result<CustomerDomainModel, WTError>) -> ())
    func deleteCustomer(customer: CustomerDomainModel, result: (Result<NoData, WTError>) -> ())
}

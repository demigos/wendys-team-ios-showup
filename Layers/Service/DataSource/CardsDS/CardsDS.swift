 //
// Created by Maksim Bazarov on 11.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Result
import ReactiveCocoa

class CardsDS : CardsDSInterface
{
    
    // MARK: Init
    var server : ServerAPIInterface
    init(server: ServerAPIInterface)
    {
        self.server = server
    }
    
    var  (cardsListSignal, cardsListObservers) = Signal<[PaymentCardDomainModel] , WTError>.pipe()
    
    
    var  (primaryCardSignal, primaryCardObservers) = Signal<PaymentCardDomainModel , WTError>.pipe()
    
    var primaryCardObserversList: [ReactiveCocoa.Observer<PaymentCardDomainModel , WTError>] = []
    
    var cardsListObserversList: [ReactiveCocoa.Observer<[PaymentCardDomainModel] , WTError>] = []
    
    //
    // MARK: Interface
    
    var primaryCardUpdate : Signal<PaymentCardDomainModel , WTError> {
        (primaryCardSignal, primaryCardObservers) = Signal<PaymentCardDomainModel , WTError>.pipe()
        primaryCardObserversList.append(primaryCardObservers)
        return primaryCardSignal
    }
    
    var cardsListUpdate: Signal<[PaymentCardDomainModel], WTError> {
        (cardsListSignal, cardsListObservers) = Signal<[PaymentCardDomainModel] , WTError>.pipe()
        cardsListObserversList.append(cardsListObservers)
        return cardsListSignal
    }
    
    func loadCards()
    {
        server.loadCards { (result) in
            switch result {
            case .Success(let cards):
                self.cardsListObservers.sendNext(cards)
                self.cardsListObservers.sendCompleted()
                self.detectPrimaryCard(cards)
            case .Failure(let error):
                self.cardsListObservers.sendFailed(error)
                self.cardsListObservers.sendCompleted()

            }
        }
    }
    
    func createCard(card: PaymentCardDomainModel, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    {
        server.createCard(card) { (_result) in
            switch _result {
                
            case .Success(let card):
                self.loadCards()
                result(.Success(card))
                
            case .Failure(let error):
                self.cardsListObservers.sendFailed(error)
                result(.Failure(error))
            }
        }
    }
    
    func removeCard(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    {
        server.removeCard(cardID) { (_result) in
            switch _result {
                
            case .Success(let card):
                self.loadCards()
                result(.Success(card))
                
            case .Failure(let error):
                self.cardsListObservers.sendFailed(error)
                result(.Failure(error))
            }
        }
    }
    
    func makeCardPrimary(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    {
        server.makeCardPrimary(cardID) { (_result) in
            switch _result {
                
            case .Success(let card):
                self.loadCards()
                result(.Success(card))
                
            case .Failure(let error):
                self.cardsListObservers.sendFailed(error)
                result(.Failure(error))
            }
            
        }
    }
    
    //
    // MARK: Private
    func detectPrimaryCard(cards: [PaymentCardDomainModel])
    {
        var primaryCard: PaymentCardDomainModel?
        for card in cards {
            if let primary = card.isPrimary where primary == true {
                primaryCard = card
            }
        }
        
        if let primaryCard = primaryCard {
            primaryCardObservers.sendNext(primaryCard)
        }
    }
    
}

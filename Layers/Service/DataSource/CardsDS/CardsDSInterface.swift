//
// Created by Maksim Bazarov on 11.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import Result
import ReactiveCocoa

protocol CardsDSInterface
{
    var cardsListUpdate : Signal<[PaymentCardDomainModel] , WTError> { get }
    var primaryCardUpdate : Signal<PaymentCardDomainModel , WTError> { get }
    
    func loadCards()
    func createCard(card: PaymentCardDomainModel, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    func removeCard(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())
    func makeCardPrimary(cardID: Int, result: (Result<PaymentCardDomainModel, WTError>) -> ())

}

//
// Created by Maksim Bazarov on 09.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Result

protocol VisitsDSInterface
{
    
    var visitsListUpdate : Signal<[VisitDomainModel] , WTError>  { get }
    var unpaidCountUpdate : Signal<UInt , WTError> { get }
    var unpaidListUpdate : Signal<[VisitDomainModel] , WTError> { get }
    var unpaidNoFeedbackUpdate : Signal<UnpaidNoFeedbackVisitsDomainModel , WTError> { get }
    
    func loadVisits()
    func loadUnpaidVisits()
    func loadUnpaidAndNoFeedbackVisits()
    
    func cancelVisit(visit: VisitDomainModel, charge: Bool?, result: (Result<CancellationResultDomainModel, WTError>) -> ())
    func loadVisit(visitID: UInt, result: (Result<VisitDomainModel, WTError>) -> ())
    func createVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    func updateVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    func feedbackForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    func payForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    func payForVisits(visits: [VisitDomainModel], result: (Result<NoData, WTError>) -> ())
}

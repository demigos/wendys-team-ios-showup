//
// Created by Maksim Bazarov on 09.06.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Result

class VisitsDS : VisitsDSInterface
{
    // MARK: Init
    init(server: ServerAPIInterface)
    {
        self.server = server
    }

    var server : ServerAPIInterface

    var (visitsSignal, visitsObservers) = Signal<[VisitDomainModel] , WTError>.pipe()
    let  (unpaidCountSignal, unpaidCountObservers) = Signal<UInt , WTError>.pipe()
    var  (unpaidNoFeedbackSignal, unpaidNoFeedbackObservers) = Signal<UnpaidNoFeedbackVisitsDomainModel , WTError>.pipe()
    let  (unpaidListSignal, unpaidListObservers) = Signal<[VisitDomainModel] , WTError>.pipe()
    var visitsListObserversList: [ReactiveCocoa.Observer<[VisitDomainModel], WTError>] = []
    var unpaidNoFeedbackObserversList: [ReactiveCocoa.Observer<UnpaidNoFeedbackVisitsDomainModel, WTError>] = []

    // MARK: Interface
    var visitsListUpdate : Signal<[VisitDomainModel] , WTError> {
        (visitsSignal, visitsObservers) = Signal<[VisitDomainModel] , WTError>.pipe()
        visitsListObserversList.append(visitsObservers)
        return visitsSignal
    }

    var unpaidListUpdate : Signal<[VisitDomainModel] , WTError> {
        return unpaidListSignal
    }
    
    var unpaidCountUpdate : Signal<UInt , WTError> {
        return unpaidCountSignal
    }

    var unpaidNoFeedbackUpdate : Signal<UnpaidNoFeedbackVisitsDomainModel , WTError> {
        (unpaidNoFeedbackSignal, unpaidNoFeedbackObservers) = Signal<UnpaidNoFeedbackVisitsDomainModel , WTError>.pipe()
        unpaidNoFeedbackObserversList.append(unpaidNoFeedbackObservers)
        return unpaidNoFeedbackSignal
    }

    func loadVisits()
    {
        server.loadVisits { result in
            switch result {
            case .Success(let visits):
                self.visitsListObserversList.forEach {
                    $0.sendNext(visits)
                }
            case .Failure(let error):
                self.visitsObservers.sendFailed(error)
            }
        }
    }
    
    func loadUnpaidAndNoFeedbackVisits()
    {
        server.loadUnpaidAndNoFeedbackVisits { (loadingResult) in
            switch loadingResult {
            case .Success(let visits):
                print("loaded")
                self.unpaidCountObservers.sendNext(UInt(visits.unpaid.count))
                self.unpaidNoFeedbackObservers.sendNext(visits)
            case .Failure(let error):
                self.unpaidNoFeedbackObservers.sendFailed(error)
            }
        }
    }

    func loadUnpaidVisits()
    {
        server.loadUnpaidVisits { (loadingResult) in
            switch loadingResult {
            case .Success(let visits):
                self.unpaidCountObservers.sendNext(UInt(visits.count))
                self.unpaidListObservers.sendNext(visits)
            case .Failure(let error):
                self.unpaidListObservers.sendFailed(error)
            }
        }
    }

    // MARK: Just proxing for future
    func cancelVisit(visit: VisitDomainModel, charge: Bool?, result: (Result<CancellationResultDomainModel, WTError>) -> ())
    {
        server.cancelVisit(visit, charge: charge) { (_result) in
            switch _result {
            case .Success(let cr):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Success(cr))
            case .Failure(let error):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Failure(error))
            }
        }
    }

    func loadVisit(visitID: UInt, result: (Result<VisitDomainModel, WTError>) -> ())
    {
        server.loadVisit(visitID, result: result)
    }

    func createVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    {
        server.createVisit(visit) { (_result) in
            switch _result {
            case .Success(let cr):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Success(cr))
            case .Failure(let error):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Failure(error))
            }

        }
    }

    func updateVisit(visit: VisitDomainModel, result: (Result<VisitDomainModel, WTError>) -> ())
    {
        server.updateVisit(visit) { (_result) in
            switch _result {
            case .Success(let cr):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Success(cr))
            case .Failure(let error):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Failure(error))
            }
        }
    }

    func feedbackForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    {
        server.feedbackForVisit(visit, result: result)
    }

    func payForVisit(visit: VisitDomainModel, result: (Result<NoData, WTError>) -> ())
    {
        server.payForVisit(visit) { (_result) in
            switch _result {
            case .Success(let cr):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Success(cr))
            case .Failure(let error):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Failure(error))
            }

        }
    }

    func payForVisits(visits: [VisitDomainModel], result: (Result<NoData, WTError>) -> ())
    {
        server.payForVisits(visits) { (_result) in
            switch _result {
            case .Success(let cr):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Success(cr))
            case .Failure(let error):
                self.loadVisits()
                self.loadUnpaidVisits()
                result(.Failure(error))
            }

        }
    }

}

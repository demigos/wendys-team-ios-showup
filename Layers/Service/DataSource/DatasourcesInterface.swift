//
//  DatasourcesInterface.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 01/07/2016.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

protocol DatasourcesInterface
{
    var visits : VisitsDSInterface { get }
    var customers : CustomersDSInterface { get }
    var cards : CardsDSInterface { get }
}

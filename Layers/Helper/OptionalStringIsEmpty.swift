//
//  String+NotEmpty.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 23/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

protocol PossiblyEmpty {
    var isEmpty: Bool { get }
}

extension String: PossiblyEmpty {}

extension Optional where Wrapped: PossiblyEmpty {
    var isEmpty: Bool {
        switch self {
        case .None: return true
        case .Some(let value): return value.isEmpty
        }
    }
}

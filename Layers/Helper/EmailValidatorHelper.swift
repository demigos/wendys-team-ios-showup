//
//  EmailValidatorHelper.swift
//  WendysTeam
//
//  Created by Sergey Sivak on 18/02/16.
//  Copyright © 2016 Wendy'sTeam. All rights reserved.
//

import Foundation

extension String {
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }
}
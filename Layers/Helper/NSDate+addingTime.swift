//
//  NSDate+addingTime.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 01.04.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

extension NSDate {
    
    /**
     Add time only
     */
    func separateHoursMinutes(time: NSDate) -> (hours: UInt, minutes: UInt)
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH"
        let hoursStr = dateFormatter.stringFromDate(time)
        dateFormatter.dateFormat = "mm"
        let minutesStr = dateFormatter.stringFromDate(time)
        var hours = UInt(0)
        var minutes = UInt(0)
        
        if let h = UInt(hoursStr) {
            hours = h
        }
        
        if let m = UInt(minutesStr) {
            minutes = m
        }
    
        return (hours, minutes)
    }
    
    func separateHoursMinutes() -> (hours: UInt, minutes: UInt)
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH"
        let hoursStr = dateFormatter.stringFromDate(self)
        dateFormatter.dateFormat = "mm"
        let minutesStr = dateFormatter.stringFromDate(self)
        var hours = UInt(0)
        var minutes = UInt(0)
        
        if let h = UInt(hoursStr) {
            hours = h
        }
        
        if let m = UInt(minutesStr) {
            minutes = m
        }
        
        return (hours, minutes)
    }

}
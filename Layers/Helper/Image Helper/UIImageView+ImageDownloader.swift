//
//  UIImage+ImageDownloader.swift
//  DaTestApp
//
//  Created by Maksim Bazarov on 08/02/2016.
//  Copyright © 2016 Maksim Bazarov (bazaroffma@gmail.com). All rights reserved.
//

import UIKit

extension UIImageView {

    func setupImage(fromURL url: String) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade;
        
        layer.addAnimation(transition, forKey: nil)
        ImageDownloaderHelper.download(image: url) { (image) -> () in
            self.image = image
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }

}

//
//  WendysServerConst.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 19.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit


let media_base_path = "https://test.wendys-team.com"
//let media_base_path = "https://wendys-team.com"
let API_base_path = media_base_path + "/api"

// MARK: Parameters
let header_authorization = "Authorization"

let request_email = "email"
let request_password = "password"
let request_password1 = "password1"
let request_code = "code"
let request_name = "name"
let request_zipcode = "zipcode"
let request_address = "address"
let request_phone = "phone"
let request_image = "image"
let request_services = "services"
let request_date = "date"
let request_duration = "duration"
let request_feedback = "feedback"
let request_rating = "rating"
let request_isPrimary = "is_primary"
let request_last4 = "last4"
let request_exp_month = "exp_month"
let request_exp_year = "exp_year"
let request_token = "token"
let request_fb_token = "access_token"
let request_apns_token = "apns_token"
let request_gender = "gender"


// MARK: Response

let response_key = "key"
let response_id = "id"
let response_account = "account"
let response_is_primary = "is_primary"
let response_brand = "brand"
let response_name = "name"
let response_last4 = "last4"
let response_exp_month = "exp_month"
let response_exp_year = "exp_year"
let response_token = "token"

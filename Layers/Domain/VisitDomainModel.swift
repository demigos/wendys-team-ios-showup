//
//  VisitDomainModel.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 11.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

enum VisitStatus: UInt {
    case Draft = 0
    case New = 1
    case Assigned = 2
    case Done = 3
    case Paid = 4
    case Cancelled = 5
    case Other = 6
}

struct VisitDomainModel {
    
    var id: Int = 0
    var caregiverID: Int?
    var services: [CareServiceDomainModel] = []
    var zipCode: String?
    var address: String?
    var phone: String?
    var date: NSDate?
    var duration: Int?
    var cost: Float?
    var status: VisitStatus?
    var rating: Int?
    var feedback: String?
    var failureMessage: String?
    
    var customerName: String?
    var customerID: String?
    var customerAvatar: String?
    
    var teamMemberName: String?
    var teamMemberAvatar: String?
    
    var actualDuration: NSDate?
    
    var isOwner: Bool = false
    
    
    var priceDurationString: String {
        if let cost = cost {
            let costString = String(format: "%.0f", cost)
            if let actualDuration = actualDuration {
                let (hours, minutes) = actualDuration.separateHoursMinutes()
                var resultString = "$" + costString + " for "
                if hours > 0 {
                    resultString = resultString + "\(hours) hours"
                }
                if minutes > 0 {
                    resultString = resultString + " \(minutes) minutes"
                }
                return resultString
            }
            else {
                var resultString = "$" + costString + " for 24 hours"
                return resultString
            }
        }
        return ""
    }
    
    var actualDurationString: String {
        if let actualDuration = actualDuration {
            let (hours, minutes) = actualDuration.separateHoursMinutes()
            var resultString = ""
            if hours > 0 {
                resultString = resultString + "\(hours) h"
            }
            if minutes > 0 {
                resultString = resultString + " \(minutes) m"
            }
            return resultString
        }
        if status == .Cancelled {
            return "0 h"
        }
        return "24 h"
    }
    
    
    var isUpcoming: Bool {
        if let status = self.status {
            return [VisitStatus.New, VisitStatus.Assigned].contains(status)
        }
        
        return false
    }
    
    var isPast: Bool {
        if let status = self.status {
            return [VisitStatus.Done, VisitStatus.Paid, VisitStatus.Cancelled].contains(status)
        }
        
        return false
    }
    
}

//
// Created by Maksim Bazarov on 07.03.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

struct PaymentCardDomainModel
{
    var id : Int?
    var isPrimary : Bool?
    var number: String?
    var customerID: Int?
    
    var cardEmitter: String?
    var expirationMonth: UInt?
    var expirationYear: UInt?
    var cvc: String?
    var cardholderName: String?
    var stripeToken : String?
    var last4 : String?
    
    
    init()
    {
        
    }
    
    func makeLast4() -> String? {
        if let number = number?.stringByReplacingOccurrencesOfString("\\s", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil) {
            let last4 = String(number.characters.suffix(4))
            return last4
        }
        return nil
    }
}

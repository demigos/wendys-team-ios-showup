//
// Created by Maksim Bazarov on 17.04.16.
// Copyright (c) 2016 WendysTeam. All rights reserved.
//

import Foundation

struct CancellationResultDomainModel
{
    var canceled: Bool
    var message : String?
    var amount : Int?

    init (canceled: Bool)
    {
        self.canceled = canceled
    }
}

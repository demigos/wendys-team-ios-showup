//
//  WTError.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 11/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class WTError: ErrorType
{
    var errors : [NSError] = []

    convenience init(error: NSError) {
        self.init(domain: error.domain, code: error.code, userInfo: error.userInfo)
    }

    convenience init(domain: String, code: Int, userInfo dict: [NSObject : AnyObject]?)
    {
        self.init()
        let e = NSError(domain: domain, code: code, userInfo: dict)
        self.errors = [e]
    }

    var errorDescription : String {
        var str = String()
        for e in errors {
            str = str + e.localizedDescription + "\n"
        }

        return str

    }

}

//
//  NotificationDomainModel.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 30.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import Foundation

class NotificationDomainModel {

    var teamMemberID : Int
    var type : VisitStatus
    var visitID : Int
    
    init(teamMemberID: Int, visitID: Int, type: VisitStatus) {
        self.teamMemberID = teamMemberID
        self.type = type
        self.visitID = visitID
    }
}

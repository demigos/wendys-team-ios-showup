import Foundation

struct UnpaidNoFeedbackVisitsDomainModel {
    
    var unpaid: [VisitDomainModel] = []
    var nofeedback: [VisitDomainModel] = []

}

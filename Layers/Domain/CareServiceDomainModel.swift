//
//  ServiceDomainModel.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 21.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

enum CareServiceDomainModel: Int, CustomStringConvertible {
    case Other          = 0
    case CheckIn        = 1
    case Companionship  = 2
    case Errands        = 3
    case Groceries      = 4
    case Housekeeping   = 5
    case PetCare        = 6
    case Reminders      = 7
    case Transportation = 8
    case HouseSitting   = 9
    case Computer       = 10
    case Handyman       = 11
    case Assistant      = 12

    var description: String {
        switch self {
        case .CheckIn:
            return "Check-In"
        case .Companionship:
            return "Companionship"
        case .Errands:
            return "Shopping"
        case .Groceries:
            return "Meals"
        case .Housekeeping:
            return "Housekeeping"
        case .PetCare:
            return "Pet Care"
        case .Reminders:
            return "Reminders"
        case .Transportation:
            return "Transportation"
        case .HouseSitting:
            return "House Sitting"
        case .Computer:
            return "Computer Basics"
        case .Handyman:
            return "Handyman"
        case .Assistant:
            return "Personal Assistant"
        case .Other:
            return "Other"
        }
    }

    var imageName: String {
        switch self {
        case .CheckIn:
            return "check-in"
        case .Companionship:
            return "companionship"
        case .Errands:
            return "shopping"
        case .Groceries:
            return "meals"
        case .Housekeeping:
            return "housekeeping"
        case .PetCare:
            return "pet-care"
        case .Reminders:
            return "reminders"
        case .Transportation:
            return "transportation"
        case .HouseSitting:
            return "homesitting"
        case .Computer:
            return "computer"
        case .Handyman:
            return "handyman"
        case .Assistant:
            return "assistant"
        case .Other:
            return "other"
        }
    }

    var bigImageName: String {
        switch self {
        case .CheckIn:
            return "big_check-in"
        case .Companionship:
            return "big_companionship"
        case .Errands:
            return "big_shopping"
        case .Groceries:
            return "big_meals"
        case .Housekeeping:
            return "big_housekeeping"
        case .PetCare:
            return "big_pet-care"
        case .Reminders:
            return "big_reminders"
        case .Transportation:
            return "big_transportation"
        case .HouseSitting:
            return "big_homesitting"
        case .Computer:
            return "big_computer"
        case .Handyman:
            return "big_handyman"
        case .Assistant:
            return "big_assistant"
        case .Other:
            return "big_other"
        }
    }


}

//
//  ProfileDomainModel.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 11.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

class ProfileDomainModel: NSObject {
    var id : Int
    var name : String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }

}

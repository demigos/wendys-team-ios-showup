//
//  CustomerDomainModel.swift
//  WendysTeam
//
//  Created by Maksim Bazarov on 11.03.16.
//  Copyright © 2016 WendysTeam. All rights reserved.
//

import UIKit

struct CustomerDomainModel {
    var id: Int = 0
    var name: String?

    var gender: Int?
    var address: String?
    var phone: String?
    var thumb: String?
    var zipcode: String?
    var email: String?
    var visits_passed: Int? = nil
    var visits_upcoming: Int? = nil
    var isOwner = false
}

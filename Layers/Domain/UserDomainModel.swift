//
//  UserDomainModel.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 26.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

struct UserDomainModel
{
    var name: String?
    var email: String?
    var password: String?
    var confirmationCode: String?
    var gender: Gender?
}

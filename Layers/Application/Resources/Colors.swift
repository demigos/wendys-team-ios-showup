//
//  Colors.swift
//  WendysTeamIOS
//
//  Created by Alexey on 23/07/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import Foundation

struct Colors
{
    static let tableSeparator = UIColor(red: 0.784, green: 0.780, blue: 0.801, alpha: 1)
    static let defaultAvatar = UIColor(red: 0.651, green: 0.839, blue: 0.714, alpha: 1)
}
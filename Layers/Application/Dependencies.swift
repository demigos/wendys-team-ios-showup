//
//  Dependencies.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 19.06.16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

class Dependencies
{
    weak var window : UIWindow!
    let serverAPI : WendysServerAPI
    let authService: AuthorizationServiceInterface
    let datasources : DatasourcesInterface
    let analytics : GoogleAnalyticsService
    weak var notifications: PushNotificationsServiceInterface!
    let activity =  UserActivityHandler()
    
    init (window: UIWindow, notifications: PushNotificationsServiceInterface)
    {
        self.window = window
        let auth = AuthService()
        let server = WendysServerAPI(auth: auth)
        server.delegate = auth
        
        self.authService = auth
        self.activity.delegate = auth
        
        self.serverAPI = server
        self.notifications = notifications
        self.notifications.setupServer(server)
        self.datasources = Datasources(server: server)
        self.analytics = GoogleAnalyticsService()
    }
    
    

}

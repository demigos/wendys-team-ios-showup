//
// Created by Maksim Bazarov on 25.02.16.
// Copyright (c) 2016 Wendy'sTeam. All rights reserved.
//

import UIKit

class Router:
    MainModuleOutput
    , AuthModuleOutput
    , PasswordCreateModuleOutput
    , AuthorizationServiceDelegate
{

    weak var resources: Dependencies!
    weak var auth: AuthorizationServiceInterface!
    var authViewController: AuthViewController?
    var mainScreen: MainViewController?
    var userLogged = true

    init(resources: Dependencies)
    {
        self.resources = resources
        self.auth = resources.authService
    }

    func start() {
        resources.authService.delegate = self
        if auth.authentificated() {
            presentMainScreen()
            self.resources.notifications.checkNotification()
        } else {
            presentAuthScreen()
        }
    }

    func presentMainScreen()
    {
        self.mainScreen = MainAssembly.createModule(resources) { module in
            module.setupDelegate(self)
            self.resources.notifications.registerWithDelegate(module.notificationDelegate)

        }
        self.userLogged = true
        self.mainScreen!.present(fromWindow: resources.window)
    }

    func presentResetPasswordScreen(code: String)
    {
        guard let navigationController = authViewController?.navigationController else {
            return
        }
        PasswordCreateAssembly.createModule(resources) { module in
            module.setupDelegate(self)
            module.setCode(code)
            }.present(inNavigation: navigationController)
    }

    func presentAuthScreen()
    {
        if self.userLogged {
            resources.authService.delegate = self
            authViewController = AuthAssembly.createModule(resources: resources){ module in
                module.setupDelegate(self)
            }
            self.userLogged = false
            //        resources.window.rootViewController = authViewController
            authViewController?.present(fromWindow: resources.window)
        }
    }


    func rePresentAuthScreen()
    {
        if self.userLogged {
            resources.authService.delegate = self
            authViewController = AuthAssembly.createModule(resources: resources){ module in
                module.setupDelegate(self)
            }
            //        resources.window.rootViewController = nil
            //        authViewController?.present(fromWindow: UIApplication.window)
            //        if let wind = UIApplication.sharedApplication().keyWindow? {
            //
            //        }
            //        self.mainScreen?.view.window?.removeFromSuperview()
            //        self.mainScreen!.dismissViewControllerAnimated(false, completion: nil)
            //        self.mainScreen!.dismissViewControllerAnimated(false, completion: nil)
            //        self.mainScreen!.dismissViewControllerAnimated(false, completion: nil)
            //        self.mainScreen!.dismissViewControllerAnimated(false, completion: nil)
//            self.mainScreen!.pageController.dismissViewControllerAnimated(false, completion: nil)
//            self.mainScreen!.view.removeFromSuperview()
            self.userLogged = false
            authViewController?.present(fromWindow: resources.window)
        }
    }

    //MARK: Main module output

    func userDidLogout()
    {
        resources.authService.logout { _ in }
        //        resources = Dependencies(window: resources.window, notifications: resources.notifications)
        self.rePresentAuthScreen()
    }

    // MARK: Auth screen output

    func userDidLogin()
    {
        presentMainScreen()
    }

    func resetCodeDidReceive(code: String) {
        presentResetPasswordScreen(code)
    }

    // MARK: AuthDelegate
    
    func authErrorDidHappened(error: WTError)
    {
        presentAuthScreen()
    }
    
}

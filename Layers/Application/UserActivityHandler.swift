//
//  UserActivityHandler.swift
//  WendysTeamIOS
//
//  Created by Maksim Bazarov on 01/08/16.
//  Copyright © 2016 Maksim Bazarov. All rights reserved.
//

import UIKit

protocol UserActivityDelegate: class {
    func confirmationCodeDidReceive(code: String)
    func resetCodeDidReceive(code: String)
}

class UserActivityHandler {
    weak var delegate: UserActivityDelegate?

    func handle(userActivity: NSUserActivity) {
        guard let webpage = userActivity.webpageURL else {
            return
        }

        guard let code = webpage.lastPathComponent else {
            return
        }
        if webpage.pathComponents?.contains("account-confirm-email") ?? false {
            delegate?.confirmationCodeDidReceive(code)
        } else {
            delegate?.resetCodeDidReceive(code)
        }
    }
}

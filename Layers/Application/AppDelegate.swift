//
//  AppDelegate.swift
//  WendysTeam
//
//  Created by Sergey Sivak on 12/21/15.
//  Copyright © 2015 Wendy'sTeam. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Stripe


@UIApplicationMain
class AppDelegate: UIResponder {

    var window: UIWindow?
    let notifications = PushNotificationService()
    var router : Router!
    var resources: Dependencies!

}

extension AppDelegate: UIApplicationDelegate {

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {
        
        notifications.handleLaunchOptions(launchOptions)
        application.applicationIconBadgeNumber = 0

        // Fabric integration
        Fabric.with([Crashlytics.self])

        // Facebook integration
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        // Stripe SDK
        StripeService.auth()



        // Navigation bar configuration
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: .Default)
        UINavigationBar.appearance().backgroundColor = UIColor(red:0.96, green:0.96, blue:0.97, alpha:1.0)

        NetworkService.sharedInstance.setupReachability()

        // Dependencies
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        resources = Dependencies(window: window!, notifications: notifications)
        router = Router(resources: resources)
        router.start()

        GAI.sharedInstance().trackUncaughtExceptions = true
        GAI.sharedInstance().logger.logLevel = .Verbose
        GAI.sharedInstance().dispatchInterval = 20
        var id = GAI.sharedInstance().trackerWithTrackingId("UA-79646562-1")
        var tracker = GAI.sharedInstance().defaultTracker

        NSTimeZone.setDefaultTimeZone(NSTimeZone(name: "America/Denver")!)

        return true
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        notifications.setupToken(deviceToken)
    }

//    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject:AnyObject]) {
//        notifications.handlePushNotificationFromBackground(userInfo)
//    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        if (application.applicationState == .Inactive) {
             notifications.handlePushNotificationFromBackground(userInfo)
        }
        else {
            notifications.handlePushNotification(userInfo)
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
        NetworkService.sharedInstance.updateNetworkState()
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {

        resources.activity.handle(userActivity )
        return true
    }


}
